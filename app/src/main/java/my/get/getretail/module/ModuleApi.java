package my.get.getretail.module;

import android.content.Context;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;

import my.get.getretail.network.IRestApiRetail;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 20/10/15.
 */
public class ModuleApi {

	public static final String		GET_API_URL = "http://private-ac317-getretail.apiary-mock.com";
	private static final String		CACHE_DIR = "http";
	private static final long		CACHE_MAX_SIZE = 1024 * 1024;

	private IRestApiRetail			mService;
	private Context					mContext;

	public ModuleApi(Context context) {
		OkHttpClient	okHttpClient;

		mContext = context;
		okHttpClient = buildOkHttpClient();
		mService = buildRetrofitService(okHttpClient);
	}

	private OkHttpClient		buildOkHttpClient() {
		OkHttpClient okHttpClient = new OkHttpClient();
		File cacheDir = mContext.getCacheDir();
		if (cacheDir != null) {
			File cacheDirectory = new File(cacheDir.getAbsolutePath(), CACHE_DIR);
			Cache cache = new Cache(cacheDirectory, CACHE_MAX_SIZE);
			okHttpClient.setCache(cache);
		}
		return okHttpClient;
	}

	private IRestApiRetail		buildRetrofitService(OkHttpClient okHttpClient) {
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(GET_API_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.client(okHttpClient)
				.build();

		return retrofit.create(IRestApiRetail.class);
	}

	public IRestApiRetail		getService() {
		return mService;
	}
}