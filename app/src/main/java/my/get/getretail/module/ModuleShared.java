package my.get.getretail.module;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.List;

import my.get.getretail.model.pojo.DeliveryInfo;
import my.get.getretail.model.pojo.Store;
import my.get.getretail.model.pojo.Theme;

/**
 * Created by qlitzler on 29/10/15.
 */
public class ModuleShared {

	private static final String							SUFFIX_SHARED_THEME = "_shared_theme";

	private HashMap<String, SharedPreferences>			mShared;

	private Context										mContext;
	private SharedPreferences.Editor					mEditor;

	public ModuleShared(Context context) {
		mContext = context;
		mShared = new HashMap<>(1);
	}

	public void			writeStoreTheme(List<Store> stores) {
		mShared = new HashMap<>(stores.size());
		for (Store store: stores) {
			writeTheme(store.getLabel(), store.getTheme());
		}
	}

	public void			writeTheme(String storeLabel, Theme theme) {
		SharedPreferences		sharedFile;

		sharedFile = mContext.getSharedPreferences(
				storeLabel + SUFFIX_SHARED_THEME,
				Context.MODE_PRIVATE
		);
		mShared.put(storeLabel, sharedFile);
		mEditor = sharedFile.edit();
		mEditor.putBoolean(Theme.PERSONAL_MESSAGE, theme.isPersonalMessage());
		mEditor.putBoolean(Theme.CATEGORY_PARALLAX, theme.isCategoryParallax());
		mEditor.putInt(Theme.BACKGROUND_COLOR, theme.getBackgroundColor());
		mEditor.putInt(Theme.PRICE_TEXT_COLOR, theme.getPriceTextColor());
		mEditor.putInt(Theme.PRODUCT_NAME_TEXT_COLOR, theme.getProductNameTextColor());
		mEditor.putInt(Theme.BUY_PANEL_TEXT_COLOR, theme.getBuyPanelTextColor());
		mEditor.putInt(Theme.BUY_PANEL_BACKGROUND_COLOR, theme.getBuyPanelBackgroundColor());
		mEditor.putInt(Theme.BUY_OPTION_TEXT_COLOR, theme.getBuyOptionTextColor());
		mEditor.putInt(Theme.BUY_BUTTON_BACKGROUND_COLOR, theme.getBuyButtonBackgroundColor());
		mEditor.putInt(Theme.BUY_BUTTON_TEXT_COLOR, theme.getBuyButtonTextColor());
		mEditor.putString(Theme.BUY_BUTTON_TEXT, theme.getBuyButtonText());
		mEditor.putInt(Theme.BUY_BUTTON_RADIUS, theme.getBuyButtonRadius());
		mEditor.putString(Theme.BUY_BUTTON_BACKGROUND_IMAGE, theme.getBuyButtonBackgroundImage());
		mEditor.putInt(Theme.BUY_BASKET_TEXT_COLOR, theme.getBuyBasketTextColor());
		mEditor.putInt(Theme.SHARE_BUTTON_COLOR, theme.getShareButtonColor());
		mEditor.putString(Theme.SHARE_MESSAGE_TEXT, theme.getShareMessageText());
		mEditor.putString(Theme.SHARE_MAIL_TEXT, theme.getShareMailText());
		mEditor.putInt(Theme.INFO_TEXT_COLOR, theme.getInfoTextColor());
		mEditor.putInt(Theme.INFO_BUTTON_COLOR, theme.getInfoButtonColor());
		mEditor.putInt(Theme.INFO_BACKGROUND_COLOR, theme.getInfoBackgroundColor());
		mEditor.putString(Theme.SHARE_FACEBOOK_URL, theme.getShareFacebookUrl());
		mEditor.putString(Theme.SHARE_TWITTER_URL, theme.getShareTwitterUrl());
		mEditor.putString(Theme.MENU_BACKGROUND_IMAGE, theme.getMenuBackgroundImage());
		mEditor.putInt(Theme.MENU_TEXT_COLOR, theme.getMenuTextColor());
		mEditor.putInt(Theme.MENU_BACKGROUND_COLOR, theme.getMenuBackgroundColor());
		mEditor.putInt(Theme.MENU_ICON_COLOR, theme.getMenuIconColor());
		mEditor.putInt(Theme.NAVIGATION_BAR_BACKGROUND_COLOR, theme.getNavigationBarBackgroundColor());
		mEditor.putInt(Theme.NAVIGATION_BAR_TEXT_COLOR, theme.getNavigationBarTextColor());
		mEditor.putInt(Theme.NAVIGATION_BAR_BASKET_COLOR, theme.getNavigationBarBasketColor());
		mEditor.putString(Theme.CATEGORY_TITLE_POSITION, theme.getCategoryTitlePosition());
		mEditor.putBoolean(Theme.TAKEAWAY + DeliveryInfo.ACTIVE, theme.getTakeAwayInfo().isActive());
		mEditor.putBoolean(Theme.DELIVERY + DeliveryInfo.ACTIVE, theme.getDeliveryInfo().isActive());
		mEditor.putBoolean(Theme.EATIN + DeliveryInfo.ACTIVE, theme.getEatInInfo().isActive());
		mEditor.putString(Theme.TAKEAWAY + DeliveryInfo.SUCCESS_MESSAGE, theme.getTakeAwayInfo().getSuccessMessage());
		mEditor.putString(Theme.DELIVERY + DeliveryInfo.SUCCESS_MESSAGE, theme.getDeliveryInfo().getSuccessMessage());
		mEditor.putString(Theme.EATIN + DeliveryInfo.SUCCESS_MESSAGE, theme.getEatInInfo().getSuccessMessage());
		mEditor.apply();
		mEditor.clear();
	}

	public int			getIntValue(final String storeLabel, final String key) {
		return mShared.get(storeLabel).getInt(key, 0);
	}

	public String		getStringValue(final String storeLabel, final String key) {
		return mShared.get(storeLabel).getString(key, "null");
	}

	public boolean		getBooleanValue(final String storeLabel, final String key) {
		return mShared.get(storeLabel).getBoolean(key, false);
	}
}
