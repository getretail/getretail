package my.get.getretail.module;

import android.content.Context;

import my.get.getretail.model.pojo.Customer;

/**
 * Created by qlitzler on 25/11/15.
 */
public class ModuleCustomer {

	private Customer		mCustomer;
	private Context			mContext;

	public ModuleCustomer(Context context) {
		mContext = context;
	}

	public void				setCustomer(Customer customer) { mCustomer = customer; }

	Customer				getService() { return mCustomer; }
}
