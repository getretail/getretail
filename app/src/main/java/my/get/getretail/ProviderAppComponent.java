package my.get.getretail;

import android.support.annotation.NonNull;

import my.get.getretail.module.ModuleApi;
import my.get.getretail.module.ModuleCustomer;
import my.get.getretail.module.ModuleShared;

/**
 * Created by qlitzler on 20/10/15.
 */
public class ProviderAppComponent {

	ModuleApi					mModuleApi;
	ModuleShared				mModuleShared;
	ModuleCustomer				mModuleCustomer;

	ProviderAppComponent(ModuleApi moduleApi, ModuleShared moduleShared, ModuleCustomer moduleCustomer) {
		mModuleApi = moduleApi;
		mModuleShared = moduleShared;
		mModuleCustomer = moduleCustomer;
	}

	Object		getService(@NonNull String name) {
		if (name.equals(ModuleApi.class.getName())) {
			return mModuleApi;
		} else if (name.equals(ModuleShared.class.getName())) {
			return mModuleShared;
		} else if (name.equals(ModuleCustomer.class.getName())) {
			return mModuleCustomer;
		}
		return null;
	}
}
