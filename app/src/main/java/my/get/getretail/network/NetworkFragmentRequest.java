package my.get.getretail.network;

import java.lang.ref.WeakReference;

import my.get.getretail.ui.base.getcorp.AGetCorpFragment;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 03/11/15.
 */
public abstract class NetworkFragmentRequest<T> implements Callback<T> {

	final protected WeakReference<AGetCorpFragment> mFragment;

	public NetworkFragmentRequest(AGetCorpFragment fragment) {
		mFragment = new WeakReference<>(fragment);
	}

	@Override
	abstract public void		onResponse(Response<T> response, Retrofit retrofit);

	@Override
	public void					onFailure(Throwable t) {
		t.printStackTrace();
	}
}
