package my.get.getretail.network.request;

import my.get.getretail.model.pojo.Success;
import my.get.getretail.network.IRestApiRetail;
import retrofit.Call;

/**
 * Created by qlitzler on 26/10/15.
 */
public class RequestAuth {

	public static Call<Success>	logout(IRestApiRetail restApiRetail) {
		return restApiRetail.logout("token_user");
	}

	public static Call<Success>	forgotPassword(IRestApiRetail restApiRetail) {
		return restApiRetail.forgotPassword("token_user", "ql@get.my");
	}

	public static Call<Success>	changePassword(IRestApiRetail restApiRetail) {
		return restApiRetail.changePassword("token_user", "1234");
	}
}
