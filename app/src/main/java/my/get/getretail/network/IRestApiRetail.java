package my.get.getretail.network;

import java.util.List;

import my.get.getretail.model.pojo.Address;
import my.get.getretail.model.pojo.Basket;
import my.get.getretail.model.pojo.Card;
import my.get.getretail.model.pojo.ConfigGetCorp;
import my.get.getretail.model.pojo.Customer;
import my.get.getretail.model.pojo.GroupBasketCheckout;
import my.get.getretail.model.pojo.GroupOrderHistory;
import my.get.getretail.model.pojo.Product;
import my.get.getretail.model.pojo.Schedule;
import my.get.getretail.model.pojo.Shop;
import my.get.getretail.model.pojo.Success;
import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by qlitzler on 20/10/15.
 */
public interface IRestApiRetail {

	String					AUTHORIZATION = "Authorization: ";
	String					AUTHORIZATION_BEARER = AUTHORIZATION + "Bearer ";
	String					CACHE_CONTROL = "Cache-Control: public, max-stale=0";

	// Configurations
	@Headers(CACHE_CONTROL)
	@GET("app/config/corp")
	Call<ConfigGetCorp>			getConfigGetCorp(
			@Query("app_key") String appKey
	);

	@Headers(CACHE_CONTROL)
	@GET("app/config/corp")
	Call<ConfigGetCorp>			getConfigGetCorp(
			@Query("app_key") String appKey,
			@Query("latitude") double latitude,
			@Query("longitude") double longitude
	);

	// Store
	@Headers(CACHE_CONTROL)
	@GET("app/category/{category_id}/products")
	Call<List<Product>>			getProducts(
			@Path("category_id") int category_id,
			@Query("app_key") String appKey
	);

	@Headers(CACHE_CONTROL)
	@GET("app/store/{id}/shops")
	Call<List<Shop>>			getShops(
			@Path("id") int id,
			@Query("app_key") String appKey
	);

	// Authentication
	@GET("auth/logout")
	Call<Success>				logout(
			@Header(AUTHORIZATION_BEARER) String auth
	);

	@GET("auth/password/reset")
	Call<Success>				forgotPassword(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Query("email") String email
	);

	@PUT("auth/password/change")
	Call<Success>				changePassword(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Query("new_password") String newPassword
	);

	// Customer
	@Headers(CACHE_CONTROL)
	@GET("me")
	Call<Customer>				login(
			@Header(AUTHORIZATION) String auth
	);

	@FormUrlEncoded
	@POST("customer")
	Call<Customer>				createCustomer(
			@Field("customer") Customer customer,
			@Query("password") String password
	);

	@FormUrlEncoded
	@PUT("me")
	Call<Customer>				updateCustomer(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Field("customer") Customer customer
	);

	//Address
	@FormUrlEncoded
	@POST("me/address")
	Call<Address>				createAddress(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Field("address") Address address
	);

	@FormUrlEncoded
	@PUT("me/address")
	Call<Address>				updateAddress(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Field("address") Address address
	);

	@DELETE("me/address")
	Call<Success>				deleteAddress(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Query("id") int id
	);

	//Card
	@FormUrlEncoded
	@POST("me/card")
	Call<Card>			createPaymentCard(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Field("payment_card") Card card
	);

	@DELETE("me/card")
	Call<Success>				deletePaymentCard(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Query("id") int id
	);

	//Basket
	@FormUrlEncoded
	@POST("me/basket")
	Call<Basket>				createBasket(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Field("basket") Basket basket,
			@Query("id") int id
	);

	@FormUrlEncoded
	@POST("me/basket")
	Call<Basket>				createBasket(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Field("basket") Basket basket
	);

	@FormUrlEncoded
	@PUT("me/basket")
	Call<Basket>				updateBasket(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Field("basket") Basket basket,
			@Query("id") int id
	);

	@FormUrlEncoded
	@PUT("me/basket")
	Call<Basket>				updateBasket(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Field("basket") Basket basket
	);

	@DELETE("me/basket")
	Call<Success>				deleteBasket(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Query("id") int id
	);

	//Orders
	@Headers(CACHE_CONTROL)
	@GET("me/orders")
	Call<List<GroupOrderHistory>>getOrders(
		@Header(AUTHORIZATION_BEARER) String auth,
		@Query("status") String status
	);

	@FormUrlEncoded
	@POST("me/order")
	Call<GroupBasketCheckout>	createOrder(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Field("order") GroupBasketCheckout groupBasketCheckout
	);

	@FormUrlEncoded
	@PUT("me/order")
	Call<Success>				cancelOrder(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Query("id") int id
	);

	//Delivery
	@GET("me/delivery")
	Call<Schedule>				getDelivery(
			@Header(AUTHORIZATION_BEARER) String auth,
			@Query("address") int address,
			@Query("store") int store
	);
}
