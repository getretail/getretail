package my.get.getretail.network.request;

import my.get.getretail.model.pojo.Card;
import my.get.getretail.model.pojo.Success;
import my.get.getretail.network.IRestApiRetail;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class RequestPaymentCard {

	static public Call<Card>	createPaymentCard(IRestApiRetail restApiRetail, Card card) {
		return restApiRetail.createPaymentCard("token_user", card);
	}

	static public Call<Success>		deletePaymentCard(IRestApiRetail restApiRetail, int paymentCard) {
		return restApiRetail.deletePaymentCard("token_user", paymentCard);
	}
}
