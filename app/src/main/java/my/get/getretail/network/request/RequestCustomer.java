package my.get.getretail.network.request;

import my.get.getretail.model.pojo.Customer;
import my.get.getretail.network.IRestApiRetail;
import retrofit.Call;

/**
 * Created by qlitzler on 26/10/15.
 */
public class RequestCustomer {

	public static Call<Customer>	login(IRestApiRetail restApiRetail) {
		return restApiRetail.login("token_user");
	}

	public static Call<Customer>	createCustomer(
			IRestApiRetail restApiRetail,
			Customer customer,
			String password
			) {
		return restApiRetail.createCustomer(customer, password);
	}

	public static Call<Customer>	updateCustomer(IRestApiRetail restApiRetail, Customer customer) {
		return restApiRetail.updateCustomer("token_user", customer);
	}
}
