package my.get.getretail.network.request;

import my.get.getretail.model.pojo.Address;
import my.get.getretail.model.pojo.Success;
import my.get.getretail.network.IRestApiRetail;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class RequestAddress {

	static public Call<Address>	createAddress(IRestApiRetail restApiRetail, Address address) {
		return restApiRetail.createAddress("token_user", address);
	}

	static public Call<Address>	updateAddress(IRestApiRetail restApiRetail, Address address) {
		return restApiRetail.updateAddress("token_user", address);
	}

	static public Call<Success>	deleteAddress(IRestApiRetail restApiRetail, int id) {
		return restApiRetail.deleteAddress("token_user", id);
	}
}
