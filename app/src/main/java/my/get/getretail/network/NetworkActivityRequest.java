package my.get.getretail.network;

import android.app.Activity;

import java.lang.ref.WeakReference;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 05/11/15.
 */
public abstract class NetworkActivityRequest<T> implements Callback<T> {

	final protected WeakReference<Activity> mActivity;

	public NetworkActivityRequest(Activity activity) {
		mActivity = new WeakReference<>(activity);
	}

	@Override
	abstract public void	onResponse(Response<T> response, Retrofit retrofit);

	@Override
	public void				onFailure(Throwable t) {
		t.printStackTrace();
	}
}
