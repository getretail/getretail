package my.get.getretail.network.request;

import java.util.List;

import my.get.getretail.model.pojo.Product;
import my.get.getretail.model.pojo.Shop;
import my.get.getretail.network.IRestApiRetail;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class RequestStore {

	public static Call<List<Product>>	getProducts(IRestApiRetail restApiRetail, int categoryId) {
		return restApiRetail.getProducts(categoryId, "app_key");
	}

	public static Call<List<Shop>>		getShops(IRestApiRetail restApiRetail, int storeId) {
		return restApiRetail.getShops(storeId, "app_key");
	}
}
