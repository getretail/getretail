package my.get.getretail.network.request;

import my.get.getretail.model.pojo.Schedule;
import my.get.getretail.network.IRestApiRetail;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class RequestDelivery {

	public static Call<Schedule>	getSchedule(IRestApiRetail restApiRetail, int address, int store) {
		return restApiRetail.getDelivery("token_user", address, store);
	}
}
