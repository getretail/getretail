package my.get.getretail.network.request;

import java.util.List;

import my.get.getretail.model.pojo.GroupBasketCheckout;
import my.get.getretail.model.pojo.GroupOrderHistory;
import my.get.getretail.model.pojo.Success;
import my.get.getretail.network.IRestApiRetail;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class RequestOrder {

	public static Call<List<GroupOrderHistory>>	getOrders(IRestApiRetail restApiRetail, String status) {
		return restApiRetail.getOrders("token_user", status);
	}

	public static Call<GroupBasketCheckout>		createOrder(IRestApiRetail restApiRetail, GroupBasketCheckout groupBasketCheckout) {
		return restApiRetail.createOrder("token_user", groupBasketCheckout);
	}

	public static Call<Success>				updateOrder(IRestApiRetail restApiRetail, int id) {
		return restApiRetail.cancelOrder("token_user", id);
	}
}
