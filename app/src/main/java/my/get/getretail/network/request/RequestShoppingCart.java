package my.get.getretail.network.request;

import my.get.getretail.model.pojo.Basket;
import my.get.getretail.model.pojo.Success;
import my.get.getretail.network.IRestApiRetail;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class RequestShoppingCart {

	static public Call<Basket>	createShoppingCart(IRestApiRetail restApiRetail, Basket basket, int id) {
		return restApiRetail.createBasket("token_user", basket, id);
	}

	static public Call<Basket>	createShoppingCart(IRestApiRetail restApiRetail, Basket basket) {
		return restApiRetail.createBasket("token_user", basket);
	}

	static public Call<Basket>	updateShoppingCart(IRestApiRetail restApiRetail, Basket basket, int id) {
		return restApiRetail.updateBasket("token_user", basket, id);
	}

	static public Call<Basket>	updateShoppingCart(IRestApiRetail restApiRetail, Basket basket) {
		return restApiRetail.updateBasket("token_user", basket);
	}

	static public Call<Success>			deleteShoppingCart(IRestApiRetail restApiRetail, int id) {
		return restApiRetail.deleteBasket("token_user", id);
	}
}
