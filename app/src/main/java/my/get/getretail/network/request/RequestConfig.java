package my.get.getretail.network.request;

import my.get.getretail.model.pojo.ConfigGetCorp;
import my.get.getretail.network.IRestApiRetail;
import retrofit.Call;

/**
 * Created by qlitzler on 20/10/15.
 */
public class RequestConfig {

	public static Call<ConfigGetCorp>		getConfigGetCorp(IRestApiRetail restApiRetail) {
		return restApiRetail.getConfigGetCorp("app_key");
	}

	public static Call<ConfigGetCorp>		getConfigGetCorp(IRestApiRetail restApiRetail,
															  double latitude,
															  double longitude) {
		return restApiRetail.getConfigGetCorp("app_key", latitude, longitude);
	}
}
