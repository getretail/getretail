package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 23/10/15.
 */
public class Schedule {

	private static final String			PRICE = "price";
	private static final String			PLANNING = "planning";

	@SerializedName(PRICE)
	private Price						mPrice;
	@SerializedName(PLANNING)
	private List<Day>					mPlanning;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mPrice)
				.append(mPlanning)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Schedule rhs = (Schedule) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mPrice, rhs.mPrice)
				.append(mPlanning, rhs.mPlanning)
				.isEquals();
	}

	@Override
	public String		toString() {
		return new ToStringBuilder(this)
				.appendSuper(super.toString())
				.append(PRICE, mPrice)
				.append(PLANNING, mPlanning)
				.toString();
	}

	public Price		getPrice() { return mPrice; }
	public List<Day>	getPlanning() { return mPlanning; }

	public void			setPrice(Price price) { mPrice = price; }
	public void			setPlanning(List<Day> planning) { mPlanning = planning; }
}
