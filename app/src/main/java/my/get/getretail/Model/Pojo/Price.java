package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 23/10/15.
 */
public class Price {

	private static final String			PRICE = "price";
	private static final String			PRICE_VAT = "price_vat";

	@SerializedName(PRICE)
	private int							mPrice;
	@SerializedName(PRICE_VAT)
	private int							mPriceVat;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mPrice)
				.append(mPriceVat)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Price rhs = (Price) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mPrice, rhs.mPrice)
				.append(mPriceVat, rhs.mPriceVat)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(PRICE, mPrice)
				.append(PRICE_VAT, mPriceVat)
				.toString();
	}

	public int			getPrice() { return mPrice; }
	public int			getPriceVat() { return mPriceVat; }

	public void			setPrice(int price) { mPrice = price; }
	public void			setPriceVat(int priceVat) { mPriceVat = priceVat; }
}
