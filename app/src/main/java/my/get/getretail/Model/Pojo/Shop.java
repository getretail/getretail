package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 23/10/15.
 */
public class Shop {

	private static final String			ID = "id";
	private static final String			LABEL = "label";
	private static final String			OPENING_HOURS = "opening_hours";
	private static final String			ADDRESS = "address";

	@SerializedName(ID)
	private int							mId;
	@SerializedName(LABEL)
	private String						mLabel;
	@SerializedName(OPENING_HOURS)
	private List<Day>					mOpeningHours;
	@SerializedName(ADDRESS)
	private Address						mAddress;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mLabel)
				.append(mOpeningHours)
				.append(mAddress)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Shop rhs = (Shop) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mLabel, rhs.mLabel)
				.append(mOpeningHours, rhs.mOpeningHours)
				.append(mAddress, rhs.mAddress)
				.isEquals();
	}

	@Override
	public String		toString() {
		return new ToStringBuilder(this)
				.appendSuper(super.toString())
				.append(ID, mId)
				.append(LABEL, mLabel)
				.append(OPENING_HOURS, mOpeningHours)
				.append(ADDRESS, mAddress)
				.toString();
	}

	public int			getId() { return mId; }
	public String		getLabel() { return mLabel; }
	public List<Day>	getOpeningHours() { return mOpeningHours; }
	public Address		getAddress() { return mAddress; }

	public void			setId(int id) { mId = id; }
	public void			setLabel(String label) { mLabel = label; }
	public void			setOpeningHours(List<Day> openingHours) { mOpeningHours = openingHours; }
	public void			setAddress(Address address) { mAddress = address; }
}
