package my.get.getretail.model.pojo;

import android.support.annotation.StringDef;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 23/10/15.
 */
public class BasketCheckout {

	protected static final String		ID = "id";
	protected static final String		ID_BASKET = "id_basket";
	protected static final String		ID_SHOP = "id_shop";
	protected static final String		ID_ADDRESS = "id_address";
	protected static final String		ID_PROMO = "id_promo";
	protected static final String		MESSAGE = "message";
	protected static final String		DELIVERY_DATE = "delivery_date";
	protected static final String		DELIVERY_TYPE = "delivery_type";

	@StringDef({
			DeliveryType.DELIVERY,
			DeliveryType.TAKEAWAY,
			DeliveryType.EAT_IN
	})
	public @interface DeliveryType {
		String DELIVERY = "delivery";
		String TAKEAWAY = "takeaway";
		String EAT_IN = "eat_in";
	}

	@SerializedName(ID)
	protected int						mId;
	@SerializedName(ID_BASKET)
	protected int						mIdBasket;
	@SerializedName(ID_SHOP)
	protected int						mIdShop;
	@SerializedName(ID_ADDRESS)
	protected int						mIdAddress;
	@SerializedName(ID_PROMO)
	protected int						mIdPromoCode;
	@SerializedName(MESSAGE)
	protected String					mMessage;
	@SerializedName(DELIVERY_DATE)
	protected String					mDeliveryDate;
	@SerializedName(DELIVERY_TYPE)
	protected String					mDeliveryType;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mIdBasket)
				.append(mIdShop)
				.append(mIdAddress)
				.append(mIdPromoCode)
				.append(mMessage)
				.append(mDeliveryDate)
				.append(mDeliveryType)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		BasketCheckout rhs = (BasketCheckout) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mIdBasket, rhs.mIdBasket)
				.append(mIdShop, rhs.mIdShop)
				.append(mIdAddress, rhs.mIdAddress)
				.append(mIdPromoCode, rhs.mIdPromoCode)
				.append(mMessage, rhs.mMessage)
				.append(mDeliveryDate, rhs.mDeliveryDate)
				.append(mDeliveryType, rhs.mDeliveryType)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(ID, mId)
				.append(ID_BASKET, mIdBasket)
				.append(ID_SHOP, mIdShop)
				.append(ID_ADDRESS, mIdAddress)
				.append(ID_PROMO, mIdPromoCode)
				.append(MESSAGE, mMessage)
				.append(DELIVERY_DATE, mDeliveryDate)
				.append(DELIVERY_TYPE, mDeliveryType)
				.toString();
	}

	public int			getId() { return mId; }
	public int			getIdCart() { return mIdBasket; }
	public int			getIdShop() { return mIdShop; }
	public int			getIdAddress() { return mIdAddress; }
	public int			getIdPromoCode() { return mIdPromoCode; }
	public String		getMessage() { return mMessage; }
	public String		getDeliveryDate() { return mDeliveryDate; }
	public String		getDeliveryType() { return mDeliveryType; }

	public void			setId(int id) { mId = id; }
	public void			setIdCart(int idCart) { mIdBasket = idCart; }
	public void			setIdShop(int idShop) { mIdShop = idShop; }
	public void			setIdAddress(int idAddress) { mIdAddress = idAddress; }
	public void			setIdPromoCode(int idPromoCode) { mIdPromoCode = idPromoCode; }
	public void			setMessage(String message) { mMessage = message; }
	public void			setDeliveryDate(String deliveryDate) { mDeliveryDate = deliveryDate; }
	public void			setDeliveryType(String deliveryType) { mDeliveryType = deliveryType; }
}
