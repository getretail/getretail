package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 26/10/15.
 */
public class Success {

	private static final String			SUCCESS = "success";

	@SerializedName(SUCCESS)
	private boolean						mSuccess;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mSuccess)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Success rhs = (Success) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mSuccess, rhs.mSuccess)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(SUCCESS, mSuccess)
				.toString();
	}

	public boolean			getSuccess() { return mSuccess; }

	public void				setSuccess(boolean success) { mSuccess = success; }
}
