package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 20/10/15.
 */
public class Store {

	public static final String			ID = "id";
	public static final String			LABEL = "label";
	public static final String			TYPE = "type";
	public static final String			DESCRIPTION = "description";
	public static final String			OPENING_DATE = "opening_date";
	public static final String			IS_NEW = "is_new";
	public static final String			PICTURE = "picture";
	public static final String			AVAILABLE = "available";
	public static final String			TAGS = "tags";
	public static final String			THEME = "theme";
	public static final String			LAYOUT = "layout";
	public static final String			CATEGORIES = "categories";

	@SerializedName(ID)
	private int							mId;
	@SerializedName(LABEL)
	private String						mLabel;
	@SerializedName(TYPE)
	private String						mType;
	@SerializedName(DESCRIPTION)
	private String						mDescription;
	@SerializedName(OPENING_DATE)
	private String						mOpeningDate;
	@SerializedName(IS_NEW)
	private boolean 					mIsNew;
	@SerializedName(PICTURE)
	private String						mPicture;
	@SerializedName(AVAILABLE)
	private boolean						mAvailable;
	@SerializedName(TAGS)
	private List<String>				mTags;
	@SerializedName(LAYOUT)
	private Layout						mLayout;
	@SerializedName(CATEGORIES)
	private List<Category>				mCategories;
	@SerializedName(THEME)
	private Theme						mTheme;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mLabel)
				.append(mType)
				.append(mDescription)
				.append(mIsNew)
				.append(mOpeningDate)
				.append(mPicture)
				.append(mAvailable)
				.append(mTags)
				.append(mLayout)
				.append(mCategories)
				.append(mTheme)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Store rhs = (Store) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mLabel, rhs.mLabel)
				.append(mType, rhs.mType)
				.append(mDescription, rhs.mDescription)
				.append(mOpeningDate, rhs.mOpeningDate)
				.append(mIsNew, rhs.mIsNew)
				.append(mPicture, rhs.mPicture)
				.append(mAvailable, rhs.mAvailable)
				.append(mTags, rhs.mTags)
				.append(mLayout, rhs.mLayout)
				.append(mCategories, rhs.mCategories)
				.append(mTheme, rhs.mTheme)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(ID, mId)
				.append(LABEL, mLabel)
				.append(TYPE, mType)
				.append(DESCRIPTION, mDescription)
				.append(OPENING_DATE, mOpeningDate)
				.append(IS_NEW, mIsNew)
				.append(PICTURE, mPicture)
				.append(AVAILABLE, mAvailable)
				.append(TAGS, mTags)
				.append(LAYOUT, mLayout)
				.append(CATEGORIES, mCategories)
				.append(THEME, mTheme)
				.toString();
	}

	public int				getId() { return mId; }
	public String			getLabel() { return mLabel; }
	public String			getType() { return mType; }
	public String 			getDescription() { return mDescription; }
	public String			getOpeningDate() { return mOpeningDate; }
	public boolean			getNew() { return mIsNew; }
	public String			getPicture() { return mPicture; }
	public boolean			isAvailable() { return mAvailable; }
	public List<String>		getTags() { return mTags; }
	public Layout			getLayout() { return mLayout; }
	public List<Category>	getCategories() { return mCategories; }
	public Theme			getTheme() { return mTheme; }

	public void				setId(int id) { mId = id; }
	public void				setLabel(String label) { mLabel = label; }
	public void				setType(String type) { mType = type; }
	public void				setDescription(String description) { mDescription = description; }
	public void				setOpeningDate(String openingDate) { mOpeningDate = openingDate; }
	public void				setNew(boolean isNew) { mIsNew = isNew; }
	public void				setPicture(String picture) { mPicture = picture; }
	public void				setAvailable(boolean available) { mAvailable = available; }
	public void				setTags(List<String> tags) { mTags = tags; }
	public void				setLayout(Layout layout) { mLayout = layout; }
	public void				setCategories(List<Category> categories) { mCategories = categories; }
	public void				setTheme(Theme theme) { mTheme = theme; }
}
