package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 23/10/15.
 */
public class Product {

	private static final String			ID = "id";
	private static final String			LABEL = "label";
	private static final String			QUANTITY = "quantity";
	private static final String			IS_NEW = "is_new";
	private static final String			PICTURE = "picture";
	private static final String			REFERENCE = "reference";
	private static final String			DESCRIPTION = "description";
	private static final String			TAGS = "tags";
	private static final String			PRICE = "price";
	private static final String			PRICE_OLD = "price_old";
	private static final String			OPTIONS = "options";
	private static final String			OPTION_CATEGORIES = "option_categories";

	@SerializedName(ID)
	private int							mId;
	@SerializedName(LABEL)
	private String						mLabel;
	@SerializedName(QUANTITY)
	private int[]						mQuantity;
	@SerializedName(IS_NEW)
	private boolean						mIsNew;
	@SerializedName(PICTURE)
	private String						mPicture;
	@SerializedName(REFERENCE)
	private String						mReference;
	@SerializedName(DESCRIPTION)
	private String						mDescription;
	@SerializedName(TAGS)
	private List<String>				mTags;
	@SerializedName(PRICE)
	private Price						mPrice;
	@SerializedName(PRICE_OLD)
	private Price						mPriceOld;
	@SerializedName(OPTIONS)
	private List<Option>				mOptions;
	@SerializedName(OPTION_CATEGORIES)
	private List<OptionCategory>		mOptionCategories;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mLabel)
				.append(mQuantity)
				.append(mIsNew)
				.append(mPicture)
				.append(mReference)
				.append(mDescription)
				.append(mTags)
				.append(mPrice)
				.append(mPriceOld)
				.append(mOptions)
				.append(mOptionCategories)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Product rhs = (Product) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mLabel, rhs.mLabel)
				.append(mQuantity, rhs.mQuantity)
				.append(mIsNew, rhs.mIsNew)
				.append(mPicture, rhs.mPicture)
				.append(mReference, rhs.mReference)
				.append(mDescription, rhs.mDescription)
				.append(mTags, rhs.mTags)
				.append(mPrice, rhs.mPrice)
				.append(mPriceOld, rhs.mPriceOld)
				.append(mOptions, rhs.mOptions)
				.append(mOptionCategories, rhs.mOptionCategories)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(ID, mId)
				.append(LABEL, mLabel)
				.append(QUANTITY, mQuantity)
				.append(IS_NEW, mIsNew)
				.append(PICTURE, mPicture)
				.append(REFERENCE, mReference)
				.append(DESCRIPTION, mDescription)
				.append(TAGS, mTags)
				.append(PRICE, mPrice)
				.append(PRICE_OLD, mPriceOld)
				.append(OPTIONS, mOptions)
				.append(OPTION_CATEGORIES, mOptionCategories)
				.toString();
	}

	public int						getId() { return mId; }
	public String					getLabel() { return mLabel; }
	public int[]					getQuantity() { return mQuantity; }
	public boolean					getNew() { return mIsNew; }
	public String					getPicture() { return mPicture; }
	public String					getReference() { return mReference; }
	public String					getDescription() { return mDescription; }
	public List<String>				getTags() { return mTags; }
	public Price					getPrice() { return mPrice; }
	public Price					getPriceOld() { return mPriceOld; }
	public List<Option>				getOptions() { return mOptions; }
	public List<OptionCategory>		getmOptionCategories() { return mOptionCategories; }

	public void						setId(int id) { mId = id; }
	public void						setLabel(String label) { mLabel = label; }
	public void						setQuantity(int[] quantity) { mQuantity = quantity; }
	public void						setIsNew(boolean isNew) { mIsNew = isNew; }
	public void						setPicture(String picture) { mPicture = picture; }
	public void						setReference(String reference) { mReference = reference; }
	public void						setDescription(String description) { mDescription = description; }
	public void						setTags(List<String> tags) { mTags = tags; }
	public void						setPrice(Price price) { mPrice = price; }
	public void						setPriceOld(Price priceOld) { mPriceOld = priceOld; }
	public void						setOptions(List<Option> options) { mOptions = options; }
	public void						setOptionCategories(List<OptionCategory> optionCategories) { mOptionCategories = optionCategories; }
}
