package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 30/10/15.
 */
public class ConfigGetCorp extends ConfigContent {

	public static final String		STORES = "stores";

	@SerializedName(STORES)
	private List<Store>				mStores;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mStores)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		ConfigGetCorp rhs = (ConfigGetCorp) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mStores, rhs.mStores)
				.isEquals();
	}

	@Override
	public String		toString() {
		return new ToStringBuilder(this)
				.appendSuper(super.toString())
				.append(STORES, mStores)
				.toString();
	}

	public List<Store>			getStores() { return mStores; }

	public void					setStores(List<Store> stores) { mStores = stores; }
}
