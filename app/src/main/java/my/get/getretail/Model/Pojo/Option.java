package my.get.getretail.model.pojo;

import android.support.annotation.StringDef;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 23/10/15.
 */
public class Option {

	private static final String			ID = "id";
	private static final String			LABEL = "label";
	private static final String			TYPE = "type";
	private static final String			DATA = "data";

	@StringDef({
			OptionType.BOOLEAN_CHOICE,
			OptionType.SINGLE_CHOICE,
			OptionType.MULTIPLE_CHOICE
	})

	public @interface OptionType {
		String BOOLEAN_CHOICE = "boolean_choice";
		String SINGLE_CHOICE = "single_choice";
		String MULTIPLE_CHOICE = "multiple_choice";
	}

	@SerializedName(ID)
	private int							mId;
	@SerializedName(LABEL)
	private String						mLabel;
	@SerializedName(TYPE)
	private String						mType;
	@SerializedName(DATA)
	private List<OptionData>			mData;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mLabel)
				.append(mType)
				.append(mData)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Option rhs = (Option) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mLabel, rhs.mLabel)
				.append(mType, rhs.mType)
				.append(mData, rhs.mData)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(ID, mId)
				.append(LABEL, mLabel)
				.append(TYPE, mType)
				.append(DATA, mData)
				.toString();
	}

	public int				getId() { return mId; }
	public String			getLabel() { return mLabel; }
	public String			getType() { return mType; }
	public List<OptionData>	getData() { return mData; }

	public void				setId(int id) { mId = id; }
	public void				setLabel(String label) { mLabel = label; }
	public void				setType(String type) { mType = type; }
	public void				setData(List<OptionData> data) { mData = data; }
}
