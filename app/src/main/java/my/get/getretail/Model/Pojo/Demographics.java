package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 23/10/15.
 */
public class Demographics {

	private static final String			AGE = "age";
	private static final String			GENDER = "gender";
	private static final String			CITY = "city";
	private static final String			COUNTRY = "country";
	private static final String			INTERESTS = "interests";
	private static final String			EDUCATION = "education";
	private static final String			JOB = "job";

	@SerializedName(AGE)
	private int							mAge;
	@SerializedName(GENDER)
	private String						mGender;
	@SerializedName(CITY)
	private String						mCity;
	@SerializedName(COUNTRY)
	private String						mCountry;
	@SerializedName(INTERESTS)
	private List<String>				mInterests;
	@SerializedName(EDUCATION)
	private String						mEducation;
	@SerializedName(JOB)
	private String						mJob;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mAge)
				.append(mGender)
				.append(mCity)
				.append(mCountry)
				.append(mInterests)
				.append(mEducation)
				.append(mJob)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Demographics rhs = (Demographics) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mAge, rhs.mAge)
				.append(mGender, rhs.mGender)
				.append(mCity, rhs.mCity)
				.append(mCountry, rhs.mCountry)
				.append(mInterests, rhs.mInterests)
				.append(mEducation, rhs.mEducation)
				.append(mJob, rhs.mJob)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(AGE, mAge)
				.append(GENDER, mGender)
				.append(CITY, mCity)
				.append(COUNTRY, mCountry)
				.append(INTERESTS, mInterests)
				.append(EDUCATION, mEducation)
				.append(JOB, mJob)
				.toString();
	}

	public int			getAge() { return mAge; }
	public String		getGender() { return mGender; }
	public String		getCity() { return mCity; }
	public String		getCountry() { return mCountry; }
	public List<String>	getInterests() { return mInterests; }
	public String		getEducation() { return mEducation; }
	public String		getJob() { return mJob; }

	public void			setAge(int age) { mAge = age; }
	public void			setGender(String gender) { mGender = gender; }
	public void			setCity(String city) { mCity = city; }
	public void			setCountry(String country) { mCountry = mCountry; }
	public void			setInterests(List<String> interests) { mInterests = interests; }
	public void			setEducation(String education) { mEducation = education; }
	public void			setJob(String job) { mJob = job; }
}
