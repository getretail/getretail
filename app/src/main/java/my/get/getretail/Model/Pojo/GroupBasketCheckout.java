package my.get.getretail.model.pojo;

import android.support.annotation.StringDef;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 23/10/15.
 */
public class GroupBasketCheckout {

	private static final String			ID = "id";
	protected static final String		ID_CARD = "id_card";
	protected static final String		DATE = "date";
	protected static final String		TYPE = "type";
	private static final String			ORDERS = "orders";

	@SerializedName(ID)
	private int							mId;
	@SerializedName(ID_CARD)
	protected int						mIdCard;
	@SerializedName(DATE)
	protected String					mDate;
	@SerializedName(TYPE)
	protected String					mType;
	@SerializedName(ORDERS)
	private List<BasketCheckout>			mOrdersCheckout;

	@StringDef({
			OrderStatus.PENDING,
			OrderStatus.PAID,
			OrderStatus.CANCELED
	})
	public @interface OrderStatus {
		String PENDING = "pending";
		String PAID = "paid";
		String CANCELED = "canceled";
	}

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mIdCard)
				.append(mDate)
				.append(mType)
				.append(mOrdersCheckout)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		GroupBasketCheckout rhs = (GroupBasketCheckout) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mIdCard, rhs.mIdCard)
				.append(mDate, rhs.mDate)
				.append(mType, rhs.mType)
				.append(mOrdersCheckout, rhs.mOrdersCheckout)
				.isEquals();
	}

	@Override
	public String		toString() {
		return new ToStringBuilder(this)
				.appendSuper(super.toString())
				.append(ID, mId)
				.append(ID_CARD, mIdCard)
				.append(DATE, mDate)
				.append(TYPE, mType)
				.append(ORDERS, mOrdersCheckout)
				.toString();
	}

	public int						getId() { return mId; }
	public int						getIdCard() { return mIdCard; }
	public String					getDate() { return mDate; }
	public String					getType() { return mType; }
	public List<BasketCheckout>		getOrders() { return mOrdersCheckout; }

	public void						setId(int id) { mId = id; }
	public void						setIdCard(int idCard) { mIdCard = idCard; }
	public void						setDate(String date) { mDate = date; }
	public void						setType(String type) { mType = type; }
	public void						setOrders(List<BasketCheckout> ordersCheckout) { mOrdersCheckout = ordersCheckout; }
}
