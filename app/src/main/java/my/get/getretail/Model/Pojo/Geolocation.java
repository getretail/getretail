package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 22/10/15.
 */
public class Geolocation {

	private static final String			LATITUDE = "latitude";
	private static final String			LONGITUDE = "longitude";

	@SerializedName(LATITUDE)
	private double						mLatitude;
	@SerializedName(LONGITUDE)
	private double						mLongitude;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mLatitude)
				.append(mLongitude)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Geolocation rhs = (Geolocation) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mLatitude, rhs.mLatitude)
				.append(mLongitude, rhs.mLongitude)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(LATITUDE, mLatitude)
				.append(LONGITUDE, mLongitude)
				.toString();
	}

	public double		getLatitude() { return mLatitude; }
	public double		getLongitude() { return mLongitude; }

	public void			setLatitude(double latitude) { mLatitude = latitude; }
	public void			setLongitude(double longitude) { mLongitude = longitude; }
}
