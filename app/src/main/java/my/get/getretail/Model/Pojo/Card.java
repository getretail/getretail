package my.get.getretail.model.pojo;

import android.support.annotation.StringDef;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 23/10/15.
 */
public class Card {

	private static final String			ID = "id";
	private static final String			LABEL = "label";
	private static final String			LAST_DIGITS = "last_numbers";
	private static final String			EXPIRE = "expire";
	private static final String			OWNER = "owner";
	private static final String			TYPE = "type";

	@StringDef({
			PaymentCardType.VISA,
			PaymentCardType.MASTERCARD,
			PaymentCardType.AMEX,
	})
	public @interface PaymentCardType {
		String VISA = "visa";
		String MASTERCARD = "mastercard";
		String AMEX = "amex";
	}

	@SerializedName(ID)
	private int							mId;
	@SerializedName(LABEL)
	private String						mLabel;
	@SerializedName(LAST_DIGITS)
	private String						mLastNumbers;
	@SerializedName(EXPIRE)
	private String						mExpire;
	@SerializedName(OWNER)
	private String						mOwner;
	@SerializedName(TYPE)
	private String						mType;

	public Card(String label, String lastNumbers, String expire, String owner, String type) {
		mLabel = label;
		mLastNumbers = lastNumbers;
		mExpire = expire;
		mOwner = owner;
		mType = type;
	}

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mLabel)
				.append(mLastNumbers)
				.append(mExpire)
				.append(mOwner)
				.append(mType)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Card rhs = (Card) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mLabel, rhs.mLabel)
				.append(mLastNumbers, rhs.mLastNumbers)
				.append(mExpire, rhs.mExpire)
				.append(mOwner, rhs.mOwner)
				.append(mType, rhs.mType)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(ID, mId)
				.append(LABEL, mLabel)
				.append(LAST_DIGITS, mLastNumbers)
				.append(EXPIRE, mExpire)
				.append(OWNER, mOwner)
				.append(TYPE, mType)
				.toString();
	}

	public int				getId() { return mId; }
	public String			getLabel() { return mLabel; }
	public String			getLastNumbers() { return mLastNumbers; }
	public String			getExpire() { return mExpire; }
	public String			getOwner() { return mOwner; }
	public String			getType() { return mType; }

	public void				setId(int id) { mId = id; }
	public void				setLabel(String label) { mLabel = label; }
	public void				setLastNumbers(String lastNumbers) { mLastNumbers = lastNumbers; }
	public void				setExpire(String expire) { mExpire = expire; }
	public void				setOwner(String owner) { mOwner = owner; }
	public void				setType(String type) { mType = type; }
}
