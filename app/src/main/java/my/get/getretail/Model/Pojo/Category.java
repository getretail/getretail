package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 23/10/15.
 */
public class Category {

	public static final String			ID = "id";
	public static final String			LABEL = "label";
	public static final String			TYPE = "type";
	public static final String			IS_NEW = "is_new";
	public static final String			PICTURE = "picture";
	public static final String			TAGS = "tags";

	@SerializedName(ID)
	private int							mId;
	@SerializedName(LABEL)
	private String						mLabel;
	@SerializedName(TYPE)
	private String						mType;
	@SerializedName(IS_NEW)
	private boolean						mIsNew;
	@SerializedName(PICTURE)
	private String						mPicture;
	@SerializedName(TAGS)
	private List<String>				mTags;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mLabel)
				.append(mType)
				.append(mIsNew)
				.append(mPicture)
				.append(mTags)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Category rhs = (Category) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mLabel, rhs.mLabel)
				.append(mType, rhs.mType)
				.append(mIsNew, rhs.mIsNew)
				.append(mPicture, rhs.mPicture)
				.append(mTags, rhs.mTags)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(ID, mId)
				.append(LABEL, mLabel)
				.append(TYPE, mType)
				.append(IS_NEW, mIsNew)
				.append(PICTURE, mPicture)
				.append(TAGS, mTags)
				.toString();
	}

	public int				getId() { return mId; }
	public String			getLabel() { return mLabel; }
	public String			getType() { return mType; }
	public boolean			getIsNew() { return mIsNew; }
	public String			getPicture() { return mPicture; }
	public List<String>		getTags() { return mTags; }

	public void				setId(int id) { mId = id; }
	public void				setLabel(String label) { mLabel = label; }
	public void				setType(String type) { mType = type; }
	public void				setIsNew(boolean isNew) { mIsNew = isNew; }
	public void				setPicture(String picture) { mPicture = picture; }
	public void				setTags(List<String> tags) { mTags = tags; }
}
