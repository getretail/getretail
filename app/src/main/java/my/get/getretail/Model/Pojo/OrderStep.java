package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 01/11/15.
 */
public class OrderStep {

	private static final String		TITLE = "title";
	private static final String		LABEL = "label";
	private static final String		TYPE_ORDER_STEP = "type_order_step";
	private static final String		TYPE_ORDER_DISTRIBUTION = "type_order_distribution";

	@SerializedName(TITLE)
	private String					mTitle;
	@SerializedName(LABEL)
	private String					mLabel;
	@SerializedName(TYPE_ORDER_STEP)
	private int						mTypeOrderStep;
	@SerializedName(TYPE_ORDER_DISTRIBUTION)
	private int						mTypeOrderDistribution;

	public OrderStep(String title, int typeOrderStep, int typeOrderDelivery) {
		mTitle = title;
		mTypeOrderStep = typeOrderStep;
		mTypeOrderDistribution = typeOrderDelivery;
	}

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mTitle)
				.append(mLabel)
				.append(mTypeOrderStep)
				.append(mTypeOrderDistribution)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		OrderStep rhs = (OrderStep) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mTitle, rhs.mTitle)
				.append(mLabel, rhs.mLabel)
				.append(mTypeOrderStep, rhs.mTypeOrderStep)
				.append(mTypeOrderDistribution, rhs.mTypeOrderDistribution)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(TITLE, mTitle)
				.append(LABEL, mLabel)
				.append(TYPE_ORDER_STEP, mTypeOrderStep)
				.append(TYPE_ORDER_DISTRIBUTION, mTypeOrderDistribution)
				.toString();
	}

	public String			getTitle() { return mTitle; }
	public String			getLabel() { return mLabel; }
	public int				getTypeOrderStep() { return mTypeOrderStep; }
	public int				getTypeOrderDelivery() { return mTypeOrderDistribution; }

	public void				setTitle(String title) { mTitle = title; }
	public void				setLabel(String label) { mLabel = label; }
	public void				setTypeOrderStep(int typeOrderStep) { mTypeOrderStep = typeOrderStep; }
	public void				setTypeOrderDelivery(int typeOrderDelivery) { mTypeOrderDistribution = typeOrderDelivery; }
}
