package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 19/10/15.
 */

public class Customer {

	private static final String			ID = "id";
	private static final String			EMAIL = "email";
	private static final String			FIRST_NAME = "first_name";
	private static final String			LAST_NAME = "last_name";
	private static final String			PHONE = "phone";
	private static final String			PICTURE = "picture";
	private static final String			AVAILABLE_PROMO = "available_promo";
	private static final String			DEMOGRAPHICS = "demographics";
	private static final String			PAYMENT_CARDS = "payment_cards";
	private static final String			ADDRESSES = "addresses";
	private static final String			SHOPPING_CARTS = "shopping_cart";

    @SerializedName(ID)
    private int							mId;
    @SerializedName(EMAIL)
    private String						mEmail;
    @SerializedName(FIRST_NAME)
    private String						mFirstName;
    @SerializedName(LAST_NAME)
    private String						mLastName;
    @SerializedName(PHONE)
    private String						mPhone;
    @SerializedName(PICTURE)
    private String						mPicture;
	@SerializedName(AVAILABLE_PROMO)
	private boolean						mAvailablePromo;
	@SerializedName(DEMOGRAPHICS)
	private Demographics				mDemographics;
	@SerializedName(PAYMENT_CARDS)
	private List<Card> mCards;
	@SerializedName(ADDRESSES)
	private List<Address>				mAddresses;
	@SerializedName(SHOPPING_CARTS)
	private List<Basket> mBaskets;

	public Customer(String email, String firstName, String lastName, String phone) {
		mEmail = email;
		mFirstName = firstName;
		mLastName = lastName;
		mPhone = phone;
	}

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mEmail)
				.append(mFirstName)
				.append(mLastName)
				.append(mPhone)
				.append(mPicture)
				.append(mAvailablePromo)
				.append(mDemographics)
				.append(mCards)
				.append(mAddresses)
				.append(mBaskets)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Customer rhs = (Customer) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mEmail, rhs.mEmail)
				.append(mFirstName, rhs.mFirstName)
				.append(mLastName, rhs.mLastName)
				.append(mPhone, rhs.mPhone)
				.append(mPicture, rhs.mPicture)
				.append(mAvailablePromo, rhs.mAvailablePromo)
				.append(mDemographics, rhs.mDemographics)
				.append(mCards, rhs.mCards)
				.append(mAddresses, rhs.mAddresses)
				.append(mBaskets, rhs.mBaskets)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(ID, mId)
				.append(EMAIL, mEmail)
				.append(FIRST_NAME, mFirstName)
				.append(LAST_NAME, mLastName)
				.append(PHONE, mPhone)
				.append(PICTURE, mPicture)
				.append(AVAILABLE_PROMO, mAvailablePromo)
				.append(DEMOGRAPHICS, mDemographics)
				.append(PAYMENT_CARDS, mCards)
				.append(ADDRESSES, mAddresses)
				.append(SHOPPING_CARTS, mBaskets)
				.toString();
	}

	public int					getId() { return mId; }
	public String				getEmail() { return mEmail; }
	public String				getFirstName() { return mFirstName; }
	public String				getLastName() { return mLastName; }
	public String				getPhone() { return mPhone; }
	public String				getPicture() { return mPicture; }
	public boolean				getAvailablePromo() { return mAvailablePromo; }
	public Demographics			getDemographics() { return mDemographics; }
	public List<Card>	getPaymentCards() { return mCards; }
	public List<Address>		getAddresses() { return mAddresses; }
	public List<Basket>	getShoppingCarts() { return mBaskets; }

	public void					setId(int id) { mId = id; }
	public void					setEmail(String email) { mEmail = email; }
	public void					setFirstName(String firstName) { mFirstName = firstName; }
	public void					setLastName(String lastName) { mLastName = lastName; }
	public void					setPhone(String phone) { mPhone = phone; }
	public void					setPicture(String picture) { mPicture = picture; }
	public void					setAvailablePromo(boolean availablePromo) { mAvailablePromo = availablePromo; }
	public void					setDemographics(Demographics demographics) { mDemographics = demographics; }
	public void					setPaymentCards(List<Card> cards) { mCards = cards; }
	public void					setAddresses(List<Address> addresses) { mAddresses = addresses; }
	public void					setShoppingCarts(List<Basket> basket) { mBaskets = basket; }
}
