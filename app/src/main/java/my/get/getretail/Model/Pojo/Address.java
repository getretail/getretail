package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 23/10/15.
 */
public class Address {

	private static final String			ID = "id";
	private static final String			LABEL = "label";
	private static final String			STREET = "street";
	private static final String			CITY = "city";
	private static final String			ZIP_CODE = "zip_code";
	private static final String			COUNTRY = "country";
	private static final String			MISC = "misc";
	private static final String			ACCESS_CODE = "access_code";
	private static final String			COMPANY = "company";
	private static final String			FAVORITE = "favorite";
	private static final String			GEOLOCATION = "geolocation";

	@SerializedName(ID)
	private int							mId;
	@SerializedName(LABEL)
	private String						mLabel;
	@SerializedName(STREET)
	private String						mStreet;
	@SerializedName(CITY)
	private String						mCity;
	@SerializedName(ZIP_CODE)
	private String						mZipCode;
	@SerializedName(COUNTRY)
	private String						mCountry;
	@SerializedName(MISC)
	private String						mMisc;
	@SerializedName(ACCESS_CODE)
	private String						mAccessCode;
	@SerializedName(COMPANY)
	private String						mCompany;
	@SerializedName(FAVORITE)
	private boolean						mFavorite;
	@SerializedName(GEOLOCATION)
	private Geolocation					mGeolocation;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mLabel)
				.append(mStreet)
				.append(mCity)
				.append(mZipCode)
				.append(mCountry)
				.append(mMisc)
				.append(mAccessCode)
				.append(mCompany)
				.append(mFavorite)
				.append(mGeolocation)
				.toHashCode();
	}

	public Address(String label, String street, String city, String zipCode, String country) {
		mLabel = label;
		mStreet = street;
		mCity = city;
		mZipCode = zipCode;
		mCountry = country;
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Address rhs = (Address) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mLabel, rhs.mLabel)
				.append(mStreet, rhs.mStreet)
				.append(mCity, rhs.mCity)
				.append(mZipCode, rhs.mZipCode)
				.append(mCountry, rhs.mCountry)
				.append(mMisc, rhs.mMisc)
				.append(mAccessCode, rhs.mAccessCode)
				.append(mCompany, rhs.mCompany)
				.append(mFavorite, rhs.mFavorite)
				.append(mGeolocation, rhs.mGeolocation)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(ID, mId)
				.append(LABEL, mLabel)
				.append(STREET, mStreet)
				.append(CITY, mCity)
				.append(ZIP_CODE, mZipCode)
				.append(COUNTRY, mCountry)
				.append(MISC, mMisc)
				.append(ACCESS_CODE, mAccessCode)
				.append(COMPANY, mCompany)
				.append(FAVORITE, mFavorite)
				.append(GEOLOCATION, mGeolocation)
				.toString();
	}

	public int			getId() { return mId; }
	public String		getLabel() { return mLabel; }
	public String		getStreet() { return mStreet; }
	public String		getCity() { return mCity; }
	public String		getZipCode() { return mZipCode; }
	public String		getCountry() { return mCountry; }
	public String		getMisc() { return mMisc; }
	public String		getAccessCode() { return mAccessCode; }
	public String		getCompany() { return mCompany; }
	public boolean		isFavorite() { return mFavorite; }
	public Geolocation	getGeolocation() { return mGeolocation; }

	public void			setId(int id) { mId = id; }
	public void			setLabel(String label) { mLabel = label; }
	public void			setStreet(String street) { mStreet = street; }
	public void			setCity(String city) { mCity = city; }
	public void			setZipCode(String zipCode) { mZipCode = zipCode; }
	public void			setCountry(String country) { mCountry = country; }
	public void			setMisc(String misc) { mMisc = misc; }
	public void			setAccessCode(String accessCode) { mAccessCode = accessCode; }
	public void			setCompany(String company) { mCompany = company; }
	public void			setFavorite(boolean favorite) { mFavorite = favorite; }
	public void			setGeolocation(Geolocation geolocation) { mGeolocation = geolocation; }
}
