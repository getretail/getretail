package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 23/10/15.
 */
public class Basket {

	private static final String			ID = "id";
	private static final String			ID_STORE = "id_store";
	private static final String			AMOUNT = "amount";
	private static final String			PRODUCTS = "products";

	@SerializedName(ID)
	private int							mId;
	@SerializedName(ID_STORE)
	private int							mIdStore;
	@SerializedName(AMOUNT)
	private Price						mAmount;
	@SerializedName(PRODUCTS)
	private List<Product>				mProducts;

	public Basket(List<Product> products) {
		mProducts = products;
	}

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mIdStore)
				.append(mAmount)
				.append(mProducts)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Basket rhs = (Basket) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mIdStore, rhs.mIdStore)
				.append(mAmount, rhs.mAmount)
				.append(mProducts, rhs.mProducts)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(ID, mId)
				.append(AMOUNT, mAmount)
				.append(ID_STORE, mIdStore)
				.append(PRODUCTS, mProducts)
				.toString();
	}

	public int				getId() { return mId; }
	public int				getIdStore() { return mIdStore; }
	public Price			getAmount() { return mAmount; }
	public List<Product>	getProducts() { return mProducts; }

	public void				setId(int id) { mId = id; }
	public void				setIdStore(int idStore) { mIdStore = idStore; }
	public void				setAmount(Price amount) { mAmount = amount; }
	public void				setProducts(List<Product> products) { mProducts = products; }
}
