package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 25/11/15.
 */
public class OrderHistory {

	private static final String			ID = "id";
	private static final String			LABEL_SHOP = "label_shop";
	private static final String			LABEL_ADDRESS = "label_address";
	private static final String			LABEL_PROMO = "label_promo";
	private static final String			DELIVERY_DATE = "delivery_date";
	private static final String			BASKET = "basket";

	@SerializedName(ID)
	private int							mId;
	@SerializedName(LABEL_SHOP)
	private String						mLabelShop;
	@SerializedName(LABEL_ADDRESS)
	private String						mLabelAddress;
	@SerializedName(LABEL_PROMO)
	private String						mLabelPromo;
	@SerializedName(DELIVERY_DATE)
	private String						mDeliveryDate;
	@SerializedName(BASKET)
	private Basket						mBasket;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mLabelShop)
				.append(mLabelAddress)
				.append(mLabelPromo)
				.append(mDeliveryDate)
				.append(mBasket)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		OrderHistory rhs = (OrderHistory) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mLabelShop, rhs.mLabelShop)
				.append(mLabelAddress, rhs.mLabelAddress)
				.append(mLabelPromo, rhs.mLabelPromo)
				.append(mDeliveryDate, rhs.mDeliveryDate)
				.append(mBasket, rhs.mBasket)
				.isEquals();
	}

	@Override
	public String		toString() {
		return new ToStringBuilder(this)
				.appendSuper(super.toString())
				.append(ID, mId)
				.append(LABEL_SHOP, mLabelShop)
				.append(LABEL_ADDRESS, mLabelAddress)
				.append(LABEL_PROMO, mLabelPromo)
				.append(DELIVERY_DATE, mDeliveryDate)
				.append(BASKET, mBasket)
				.toString();
	}

	public int						getId() { return mId; }
	public String					getLabelShop() { return mLabelShop; }
	public String					getLabelAddress() { return mLabelAddress; }
	public String					getLabelPromo() { return mLabelPromo; }
	public String					getDeliveryDate() { return mDeliveryDate; }
	public Basket					getBasket() { return mBasket; }

	public void						setId(int id) { mId = id; }
	public void						setLabelShop(String labelShop) { mLabelShop = labelShop; }
	public void						setLabelAddress(String labelAddress) { mLabelAddress = labelAddress; }
	public void						setLabelPromo(String labelPromo) { mLabelPromo = labelPromo; }
	public void						setDeliveryDate(String deliveryDate) { mDeliveryDate = deliveryDate; }
	public void						setBasket(Basket basket) { mBasket = basket; }
}
