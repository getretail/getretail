package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 23/10/15.
 */
public class Day {

	private static final String			ID = "id";
	private static final String			LABEL = "label";
	private static final String			TIME_RANGE = "time_range";

	@SerializedName(ID)
	private int							mId;
	@SerializedName(LABEL)
	private String						mLabel;
	@SerializedName(TIME_RANGE)
	private List<TimeRange>				mTimeRange;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mLabel)
				.append(mTimeRange)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Day rhs = (Day) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mLabel, rhs.mLabel)
				.append(mTimeRange, rhs.mTimeRange)
				.isEquals();
	}

	@Override
	public String		toString() {
		return new ToStringBuilder(this)
				.appendSuper(super.toString())
				.append(ID, mId)
				.append(LABEL, mLabel)
				.append(TIME_RANGE, mTimeRange)
				.toString();
	}

	public int					getId() { return mId; }
	public String				getLabel() { return mLabel; }
	public List<TimeRange>		getTimeRange() { return mTimeRange; }

	public void					setId(int id) { mId = id; }
	public void					setLabel(String label) { mLabel = label; }
	public void					setTimeRange(List<TimeRange> timeRange) { mTimeRange = timeRange; }
}
