package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Currency;

/**
 * Created by qlitzler on 23/10/15.
 */
public class ConfigContent {

	public static final String			VERSION = "version";
	public static final String			TERMS_CONDITIONS = "terms_conditions";
	public static final String			CONCEPT = "concept";
	public static final String			FAQ = "faq";
	public static final String			TWITTER = "twitter";
	public static final String			FACEBOOK = "facebook";
	public static final String			PINTEREST = "pinterest";
	public static final String			INSTAGRAM = "instagram";
	public static final String			CURRENCY = "currency";

	@SerializedName(VERSION)
	protected String					mVersion;
	@SerializedName(TERMS_CONDITIONS)
	protected String					mTermsConditions;
	@SerializedName(CONCEPT)
	protected String					mConcept;
	@SerializedName(FAQ)
	protected String					mFaq;
	@SerializedName(TWITTER)
	protected String					mTwitter;
	@SerializedName(FACEBOOK)
	protected String					mFacebook;
	@SerializedName(PINTEREST)
	protected String					mPinterest;
	@SerializedName(INSTAGRAM)
	protected String					mInstagram;
	@SerializedName(CURRENCY)
	protected Currency					mCurrency;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.append(super.hashCode())
				.append(mVersion)
				.append(mTermsConditions)
				.append(mConcept)
				.append(mFaq)
				.append(mTwitter)
				.append(mFacebook)
				.append(mPinterest)
				.append(mInstagram)
				.append(mCurrency)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		ConfigContent rhs = (ConfigContent) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mVersion, rhs.mVersion)
				.append(mTermsConditions, rhs.mTermsConditions)
				.append(mConcept, rhs.mConcept)
				.append(mFaq, rhs.mFaq)
				.append(mTwitter, rhs.mTwitter)
				.append(mFacebook, rhs.mFacebook)
				.append(mPinterest, rhs.mPinterest)
				.append(mInstagram, rhs.mInstagram)
				.append(mCurrency, rhs.mCurrency)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(VERSION, mVersion)
				.append(TERMS_CONDITIONS, mTermsConditions)
				.append(CONCEPT, mConcept)
				.append(FAQ, mFaq)
				.append(TWITTER, mTwitter)
				.append(FACEBOOK, mFacebook)
				.append(PINTEREST, mPinterest)
				.append(INSTAGRAM, mInstagram)
				.append(CURRENCY, mCurrency)
				.toString();
	}

	public String			getVersion() { return mVersion; }
	public String			getTermsConditions() { return mTermsConditions; }
	public String			getConcept() { return mConcept; }
	public String			getFaq() { return mFaq; }
	public String			getTwitter() { return mTwitter; }
	public String			getFacebook() { return mFacebook; }
	public String			getPinterest() { return mPinterest; }
	public String			getInstagram() { return mInstagram; }
	public Currency			getCurrency() { return mCurrency; }

	public void				setVersion(String version) { mVersion = version; }
	public void				setTermsConditions(String termsConditions) { mTermsConditions = termsConditions; }
	public void				setConcept(String concept) { mConcept = concept; }
	public void				setFaq(String faq) { mFaq = faq; }
	public void				setTwitter(String twitter) { mTwitter = twitter; }
	public void				setFacebook(String facebook) { mFacebook = facebook; }
	public void				setPinterest(String pinterest) { mPinterest = pinterest; }
	public void				setInstagram(String instagram) { mInstagram = instagram; }
	public void				setCurrency(Currency currency) { mCurrency = currency; }
}
