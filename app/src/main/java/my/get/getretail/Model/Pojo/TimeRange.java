package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 23/10/15.
 */
public class TimeRange {

	private static final String			DATE_START = "date_start";
	private static final String			DATE_END = "date_end";

	@SerializedName(DATE_START)
	private String						mDateStart;
	@SerializedName(DATE_END)
	private String						mDateEnd;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mDateStart)
				.append(mDateEnd)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		TimeRange rhs = (TimeRange) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mDateStart, rhs.mDateStart)
				.append(mDateEnd, rhs.mDateEnd)
				.isEquals();
	}

	@Override
	public String		toString() {
		return new ToStringBuilder(this)
				.appendSuper(super.toString())
				.append(DATE_START, mDateStart)
				.append(DATE_END, mDateEnd)
				.toString();
	}

	public String		getDateStart() { return mDateStart; }
	public String		getDateEnd() { return mDateEnd; }

	public void			setDateStart(String dateStart) { mDateStart = dateStart; }
	public void			setDateEnd(String dateEnd) { mDateEnd = dateEnd; }
}
