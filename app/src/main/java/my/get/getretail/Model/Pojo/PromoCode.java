package my.get.getretail.model.pojo;

import android.support.annotation.IntDef;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 23/10/15.
 */
public class PromoCode {

	private static final String			ID = "id";
	private static final String			KEY = "key";
	private static final String			DATE_START = "date_start";
	private static final String			DATE_END = "date_end";
	private static final String			FLAT = "flat";
	private static final String			PROPORTIONAL = "proportional";
	private static final String			STATUS = "status";

	@IntDef({
			PromoCodeStatus.ACTIVE,
			PromoCodeStatus.EXPIRED,
			PromoCodeStatus.USED
	})
	public @interface PromoCodeStatus {
		int ACTIVE = 0;
		int EXPIRED = 1;
		int USED = 2;
	}

	@SerializedName(ID)
	private int							mId;
	@SerializedName(KEY)
	private String						mKey;
	@SerializedName(DATE_START)
	private String						mDateStart;
	@SerializedName(DATE_END)
	private String						mDateEnd;
	@SerializedName(FLAT)
	private int							mFlat;
	@SerializedName(PROPORTIONAL)
	private int							mProportional;
	@SerializedName(STATUS)
	private PromoCodeStatus				mStatus;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mKey)
				.append(mDateStart)
				.append(mDateEnd)
				.append(mFlat)
				.append(mProportional)
				.append(mStatus)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		PromoCode rhs = (PromoCode) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mKey, rhs.mKey)
				.append(mDateStart, rhs.mDateStart)
				.append(mDateEnd, rhs.mDateEnd)
				.append(mFlat, rhs.mFlat)
				.append(mProportional, rhs.mProportional)
				.append(mStatus, rhs.mStatus)
				.isEquals();
	}

	@Override
	public String		toString() {
		return new ToStringBuilder(this)
				.appendSuper(super.toString())
				.append(ID, mId)
				.append(KEY, mKey)
				.append(DATE_START, mDateStart)
				.append(DATE_END, mDateEnd)
				.append(FLAT, mFlat)
				.append(PROPORTIONAL, mProportional)
				.append(STATUS, mStatus)
				.toString();
	}

	public int				getId() { return mId; }
	public String			getKey() { return mKey; }
	public String			getDateStart() { return mDateStart; }
	public String			getDateEnd() { return mDateEnd; }
	public int				getFlat() { return mFlat; }
	public int				getProportional() { return mProportional; }
	public PromoCodeStatus	getStatus() { return mStatus; }

	public void				setId(int id) { mId = id; }
	public void				setKey(String key) { mKey = key; }
	public void				setStart(String dateStart) { mDateStart = dateStart; }
	public void				setEnd(String dateEnd) { mDateEnd = dateEnd; }
	public void				setFlat(int flat) { mFlat = flat; }
	public void				setProportional(int proportional) { mProportional = proportional; }
	public void				setStatus(PromoCodeStatus status) { mStatus = status; }
}
