package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 26/10/15.
 */
public class Error {

	private static final String			ERROR = "error";
	private static final String			CODE = "code";
	private static final String			MESSAGE = "message";

	@SerializedName(ERROR)
	private boolean						mError;
	@SerializedName(CODE)
	private int							mCode;
	@SerializedName(MESSAGE)
	private String						mMessage;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mError)
				.append(mCode)
				.append(mMessage)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Error rhs = (Error) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mError, rhs.mError)
				.append(mCode, rhs.mCode)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(ERROR, mError)
				.append(CODE, mCode)
				.append(MESSAGE, mMessage)
				.toString();
	}

	public String			getMessage() { return mMessage; }
	public int				getCode() { return mCode; }
	public boolean			isError() { return mError; }

	public void				setError(boolean error) { mError = error; }
	public void				setCode(int code) { mCode = code; }
	public void				setMessage(String message) { mMessage = message; }
}
