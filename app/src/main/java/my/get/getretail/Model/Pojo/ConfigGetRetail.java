package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 30/10/15.
 */
public class ConfigGetRetail extends ConfigContent{

	public static final String			THEME = "theme";
	public static final String			CATEGORIES = "categories";

	@SerializedName(THEME)
	protected Theme						mTheme;
	@SerializedName(CATEGORIES)
	private List<Category>				mCategories;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mTheme)
				.append(mCategories)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		ConfigGetRetail rhs = (ConfigGetRetail) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mTheme, rhs.mTheme)
				.append(mCategories, rhs.mCategories)
				.isEquals();
	}

	@Override
	public String		toString() {
		return new ToStringBuilder(this)
				.appendSuper(super.toString())
				.append(THEME, mTheme)
				.append(CATEGORIES, mCategories)
				.toString();
	}

	public Theme			getTheme() { return mTheme; }
	public List<Category>	getCategories() { return mCategories; }

	public void				setTheme(Theme theme) { mTheme = theme; }
	public void				setCategories(List<Category> categories) { mCategories = categories; }
}
