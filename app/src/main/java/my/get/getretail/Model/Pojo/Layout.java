package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 20/10/15.
 */
public class Layout {

	private static final String			FOCUS = "focus";

	@SerializedName(FOCUS)
	private boolean						mFocus;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mFocus)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Layout rhs = (Layout) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mFocus, rhs.mFocus)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(FOCUS, mFocus)
				.toString();
	}

	public boolean			isFocus() { return mFocus; }

	public void				setFocus(boolean focus) { mFocus = focus; }
}
