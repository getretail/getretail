package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 23/10/15.
 */
public class GroupOrderHistory {

	private static final String			ID = "id";
	private static final String			LABEL_CARD = "label_card";
	private static final String			DATE = "date";
	private static final String			PRICE = "price";
	private static final String			STATUS = "status";
	private static final String			ORDERS = "orders";

	@SerializedName(ID)
	private int							mId;
	@SerializedName(LABEL_CARD)
	private String						mLabelCard;
	@SerializedName(DATE)
	private String						mDate;
	@SerializedName(PRICE)
	private Price						mPrice;
	@SerializedName(STATUS)
	private String						mStatus;
	@SerializedName(ORDERS)
	private List<OrderHistory>			mOrders;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mLabelCard)
				.append(mDate)
				.append(mPrice)
				.append(mStatus)
				.append(mOrders)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		GroupOrderHistory rhs = (GroupOrderHistory) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mLabelCard, rhs.mLabelCard)
				.append(mDate, rhs.mDate)
				.append(mPrice, rhs.mPrice)
				.append(mStatus, rhs.mStatus)
				.append(mOrders, rhs.mOrders)
				.isEquals();
	}

	@Override
	public String		toString() {
		return new ToStringBuilder(this)
				.appendSuper(super.toString())
				.append(ID, mId)
				.append(LABEL_CARD, mLabelCard)
				.append(DATE, mDate)
				.append(PRICE, mPrice)
				.append(STATUS, mStatus)
				.append(ORDERS, mOrders)
				.toString();
	}

	public int						getId() { return mId; }
	public String					getLabelCard() { return mLabelCard; }
	public String					getDate() { return mDate; }
	public Price					getPrice() { return mPrice; }
	public String					getStatus() { return mStatus; }
	public List<OrderHistory>		getOrders() { return mOrders; }

	public void						setId(int id) { mId = id; }
	public void						setLabelCard(String labelCard) { mLabelCard = labelCard; }
	public void						setDate(String date) { mDate = date; }
	public void						setPrice(Price price) { mPrice = price; }
	public void						setStatus(String status) { mStatus = status; }
	public void						setOrders(List<OrderHistory> orders) { mOrders = orders; }
}
