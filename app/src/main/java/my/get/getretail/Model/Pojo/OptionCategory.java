package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by qlitzler on 24/11/15.
 */
public class OptionCategory {

	private static final String			ID = "id";
	private static final String			LABEL = "label";
	private static final String			OPTIONS = "options";

	@SerializedName(ID)
	private int							mId;
	@SerializedName(LABEL)
	private String						mLabel;
	@SerializedName(OPTIONS)
	private List<Option>				mOptions;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mLabel)
				.append(mOptions)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		OptionCategory rhs = (OptionCategory) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mLabel, rhs.mLabel)
				.append(mOptions, rhs.mOptions)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(ID, mId)
				.append(LABEL, mLabel)
				.append(OPTIONS, mOptions)
				.toString();
	}

	public int				getId() { return mId; }
	public String			getLabel() { return mLabel; }
	public List<Option>		getOptions() { return mOptions; }

	public void				setId(int id) { mId = id; }
	public void				setLabel(String label) { mLabel = label; }
	public void				setOptions(List<Option> options) { mOptions = options; }
}
