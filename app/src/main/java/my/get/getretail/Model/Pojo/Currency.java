package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 23/10/15.
 */
public class Currency {

	private static final String			LABEL = "label";
	private static final String			SYMBOL = "symbol";

	@SerializedName(LABEL)
	private String						mLabel;
	@SerializedName(SYMBOL)
	private String						mSymbol;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(hashCode())
				.append(mLabel)
				.append(mSymbol)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Currency rhs = (Currency) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mLabel, rhs.mLabel)
				.append(mSymbol, rhs.mSymbol)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(LABEL, mLabel)
				.append(SYMBOL, mSymbol)
				.toString();
	}

	public String		getLabel() { return mLabel; }
	public String		getSymbol() { return mSymbol; }

	public void			setLabel(String label) { mLabel = label; }
	public void			setSymbol(String symbol) { mSymbol = symbol; }
}
