package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 23/10/15.
 */
public class OptionData {

	private static final String			ID = "id";
	private static final String			LABEL = "label";
	private static final String			PRICE = "price";

	@SerializedName(ID)
	private int							mId;
	@SerializedName(LABEL)
	private String						mLabel;
	@SerializedName(PRICE)
	private Price						mPrice;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mId)
				.append(mLabel)
				.append(mPrice)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		OptionData rhs = (OptionData) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mId, rhs.mId)
				.append(mLabel, rhs.mLabel)
				.append(mPrice, rhs.mPrice)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(ID, mId)
				.append(LABEL, mLabel)
				.append(PRICE, mPrice)
				.toString();
	}

	public int			getId() { return mId; }
	public String		getLabel() { return mLabel; }
	public Price		getPrice() { return mPrice; }

	public void			setId(int id) { mId = id; }
	public void			setLabel(String label) { mLabel = label; }
	public void			setActive(Price price) { mPrice = price; }
}
