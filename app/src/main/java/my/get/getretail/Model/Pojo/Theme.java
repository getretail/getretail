package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 20/10/15.
 */
public class Theme {

	public static final String			PERSONAL_MESSAGE = "personal_message";
	public static final String			CATEGORY_PARALLAX = "category_parallax";
	public static final String			BACKGROUND_COLOR = "background_color";
	public static final String			PRICE_TEXT_COLOR = "price_test_color";
	public static final String			PRODUCT_NAME_TEXT_COLOR = "product_name_text_color";
	public static final String			BUY_PANEL_BACKGROUND_COLOR = "buy_panel_background_color";
	public static final String			BUY_PANEL_TEXT_COLOR = "buy_panel_text_color";
	public static final String			BUY_OPTION_TEXT_COLOR = "buy_option_text_color";
	public static final String			BUY_BUTTON_BACKGROUND_COLOR = "buy_button_background_color";
	public static final String			BUY_BUTTON_TEXT_COLOR = "buy_button_text_color";
	public static final String			BUY_BUTTON_TEXT = "buy_button_text";
	public static final String			BUY_BUTTON_RADIUS = "buy_button_radius";
	public static final String			BUY_BUTTON_BACKGROUND_IMAGE = "buy_button_background_image";
	public static final String			BUY_BASKET_TEXT_COLOR = "buy_basket_text_color";
	public static final String			SHARE_BUTTON_COLOR = "share_button_color";
	public static final String			SHARE_MESSAGE_TEXT = "share_message_text";
	public static final String			SHARE_MAIL_TEXT = "share_mail_text";
	public static final String			INFO_BUTTON_COLOR = "info_button_color";
	public static final String			INFO_TEXT_COLOR = "info_text_color";
	public static final String			INFO_BACKGROUND_COLOR = "info_background_color";
	public static final String			SHARE_FACEBOOK_URL = "share_facebook_url";
	public static final String			SHARE_TWITTER_URL = "share_twitter_url";
	public static final String			MENU_BACKGROUND_IMAGE = "menu_background_image";
	public static final String			MENU_BACKGROUND_COLOR = "menu_background_color";
	public static final String			MENU_TEXT_COLOR = "menu_text_color";
	public static final String			MENU_ICON_COLOR = "menu_icon_color";
	public static final String			NAVIGATION_BAR_BACKGROUND_COLOR = "navigation_bar_background_color";
	public static final String			NAVIGATION_BAR_TEXT_COLOR = "navigation_bar_text_color";
	public static final String			NAVIGATION_BAR_BASKET_COLOR = "navigation_bar_basket_color";
	public static final String			CATEGORY_TITLE_POSITION = "category_title_position";
	public static final String			TAKEAWAY = "takeaway";
	public static final String			EATIN = "eatin";
	public static final String			DELIVERY = "delivery";

	@SerializedName(PERSONAL_MESSAGE)
	private boolean						mPersonalMessage;
	@SerializedName(CATEGORY_PARALLAX)
	private boolean						mCategoryParallax;
	@SerializedName(BACKGROUND_COLOR)
	private int							mBackgroundColor;
	@SerializedName(PRICE_TEXT_COLOR)
	private int							mPriceTextColor;
	@SerializedName(PRODUCT_NAME_TEXT_COLOR)
	private int							mProductNameTextColor;
	@SerializedName(BUY_PANEL_BACKGROUND_COLOR)
	private int							mBuyPanelBackgroundColor;
	@SerializedName(BUY_PANEL_TEXT_COLOR)
	private int							mBuyPanelTextColor;
	@SerializedName(BUY_OPTION_TEXT_COLOR)
	private int							mBuyOptionTextColor;
	@SerializedName(BUY_BUTTON_BACKGROUND_COLOR)
	private int							mBuyButtonBackgroundColor;
	@SerializedName(BUY_BUTTON_TEXT_COLOR)
	private int							mBuyButtonTextColor;
	@SerializedName(BUY_BUTTON_TEXT)
	private String						mBuyButtonText;
	@SerializedName(BUY_BUTTON_RADIUS)
	private int							mBuyButtonRadius;
	@SerializedName(BUY_BUTTON_BACKGROUND_IMAGE)
	private String						mBuyButtonBackgroundImage;
	@SerializedName(BUY_BASKET_TEXT_COLOR)
	private int							mBuyBasketTextColor;
	@SerializedName(SHARE_BUTTON_COLOR)
	private int							mShareButtonColor;
	@SerializedName(SHARE_MESSAGE_TEXT)
	private String						mShareMessageText;
	@SerializedName(SHARE_MAIL_TEXT)
	private String						mShareMailText;
	@SerializedName(INFO_BUTTON_COLOR)
	private int							mInfoButtonColor;
	@SerializedName(INFO_TEXT_COLOR)
	private int							mInfoTextColor;
	@SerializedName(INFO_BACKGROUND_COLOR)
	private int							mInfoBackgroundColor;
	@SerializedName(SHARE_FACEBOOK_URL)
	private String						mShareFacebookUrl;
	@SerializedName(SHARE_TWITTER_URL)
	private String						mShareTwitterUrl;
	@SerializedName(MENU_BACKGROUND_IMAGE)
	private String						mMenuBackgroundImage;
	@SerializedName(MENU_BACKGROUND_COLOR)
	private int							mMenuBackgroundColor;
	@SerializedName(MENU_TEXT_COLOR)
	private int							mMenuTextColor;
	@SerializedName(MENU_ICON_COLOR)
	private int							mMenuIconColor;
	@SerializedName(NAVIGATION_BAR_BACKGROUND_COLOR)
	private int							mNavigationBarBackgroundColor;
	@SerializedName(NAVIGATION_BAR_TEXT_COLOR)
	private int							mNavigationBarTextColor;
	@SerializedName(NAVIGATION_BAR_BASKET_COLOR)
	private int							mNavigationBarBasketColor;
	@SerializedName(CATEGORY_TITLE_POSITION)
	private String						mCategoryTitlePosition;
	@SerializedName(TAKEAWAY)
	private DeliveryInfo				mTakeAwayInfo;
	@SerializedName(EATIN)
	private DeliveryInfo				mEatInInfo;
	@SerializedName(DELIVERY)
	private DeliveryInfo				mDeliveryInfo;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mPersonalMessage)
				.append(mCategoryParallax)
				.append(mBackgroundColor)
				.append(mPriceTextColor)
				.append(mProductNameTextColor)
				.append(mBuyPanelBackgroundColor)
				.append(mBuyPanelTextColor)
				.append(mBuyOptionTextColor)
				.append(mBuyButtonBackgroundColor)
				.append(mBuyButtonTextColor)
				.append(mBuyButtonText)
				.append(mBuyButtonRadius)
				.append(mBuyButtonBackgroundImage)
				.append(mBuyBasketTextColor)
				.append(mShareButtonColor)
				.append(mShareMessageText)
				.append(mShareMailText)
				.append(mInfoButtonColor)
				.append(mInfoTextColor)
				.append(mInfoBackgroundColor)
				.append(mShareFacebookUrl)
				.append(mShareTwitterUrl)
				.append(mMenuBackgroundImage)
				.append(mMenuBackgroundColor)
				.append(mMenuTextColor)
				.append(mMenuIconColor)
				.append(mNavigationBarBackgroundColor)
				.append(mNavigationBarTextColor)
				.append(mNavigationBarBasketColor)
				.append(mCategoryTitlePosition)
				.append(mTakeAwayInfo)
				.append(mEatInInfo)
				.append(mDeliveryInfo)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Theme rhs = (Theme) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mPersonalMessage, rhs.mPersonalMessage)
				.append(mCategoryParallax, rhs.mCategoryParallax)
				.append(mBackgroundColor, rhs.mBackgroundColor)
				.append(mPriceTextColor, rhs.mPriceTextColor)
				.append(mProductNameTextColor, rhs.mProductNameTextColor)
				.append(mBuyPanelBackgroundColor, rhs.mBuyPanelBackgroundColor)
				.append(mBuyPanelTextColor, rhs.mBuyPanelTextColor)
				.append(mBuyOptionTextColor, rhs.mBuyOptionTextColor)
				.append(mBuyButtonBackgroundColor, rhs.mBuyButtonBackgroundColor)
				.append(mBuyButtonTextColor, rhs.mBuyButtonTextColor)
				.append(mBuyButtonText, rhs.mBuyButtonText)
				.append(mBuyButtonRadius, rhs.mBuyButtonRadius)
				.append(mBuyButtonBackgroundImage, rhs.mBuyButtonBackgroundImage)
				.append(mBuyBasketTextColor, rhs.mBuyBasketTextColor)
				.append(mShareButtonColor, rhs.mShareButtonColor)
				.append(mShareMessageText, rhs.mShareMessageText)
				.append(mShareMailText, rhs.mShareMailText)
				.append(mInfoButtonColor, rhs.mInfoButtonColor)
				.append(mInfoTextColor, rhs.mInfoTextColor)
				.append(mInfoBackgroundColor, rhs.mInfoBackgroundColor)
				.append(mShareFacebookUrl, rhs.mShareFacebookUrl)
				.append(mShareTwitterUrl, rhs.mShareTwitterUrl)
				.append(mMenuBackgroundImage, rhs.mMenuBackgroundImage)
				.append(mMenuBackgroundColor, rhs.mMenuBackgroundColor)
				.append(mMenuTextColor, rhs.mMenuTextColor)
				.append(mMenuIconColor, rhs.mMenuIconColor)
				.append(mNavigationBarBackgroundColor, rhs.mNavigationBarBackgroundColor)
				.append(mNavigationBarTextColor, rhs.mNavigationBarTextColor)
				.append(mNavigationBarBasketColor, rhs.mNavigationBarBasketColor)
				.append(mCategoryTitlePosition, rhs.mCategoryTitlePosition)
				.append(mTakeAwayInfo, rhs.mTakeAwayInfo)
				.append(mEatInInfo, rhs.mEatInInfo)
				.append(mDeliveryInfo, rhs.mDeliveryInfo)
		.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(PERSONAL_MESSAGE, mPersonalMessage)
				.append(CATEGORY_PARALLAX, mCategoryParallax)
				.append(BACKGROUND_COLOR, mBackgroundColor)
				.append(PRICE_TEXT_COLOR, mPriceTextColor)
				.append(PRODUCT_NAME_TEXT_COLOR, mProductNameTextColor)
				.append(BUY_PANEL_BACKGROUND_COLOR, mBuyPanelBackgroundColor)
				.append(BUY_PANEL_TEXT_COLOR, mBuyPanelTextColor)
				.append(BUY_OPTION_TEXT_COLOR, mBuyOptionTextColor)
				.append(BUY_BUTTON_BACKGROUND_COLOR, mBuyButtonBackgroundColor)
				.append(BUY_BUTTON_TEXT_COLOR, mBuyButtonTextColor)
				.append(BUY_BUTTON_TEXT, mBuyButtonText)
				.append(BUY_BUTTON_RADIUS, mBuyButtonRadius)
				.append(BUY_BUTTON_BACKGROUND_IMAGE, mBuyButtonBackgroundImage)
				.append(BUY_BASKET_TEXT_COLOR, mBuyBasketTextColor)
				.append(SHARE_BUTTON_COLOR, mShareButtonColor)
				.append(SHARE_MESSAGE_TEXT, mShareMessageText)
				.append(SHARE_MAIL_TEXT, mShareMailText)
				.append(INFO_BUTTON_COLOR, mInfoButtonColor)
				.append(INFO_TEXT_COLOR, mInfoTextColor)
				.append(INFO_BACKGROUND_COLOR, mInfoBackgroundColor)
				.append(SHARE_FACEBOOK_URL, mShareFacebookUrl)
				.append(SHARE_TWITTER_URL, mShareTwitterUrl)
				.append(MENU_BACKGROUND_IMAGE, mMenuBackgroundImage)
				.append(MENU_BACKGROUND_COLOR, mMenuBackgroundColor)
				.append(MENU_TEXT_COLOR, mMenuTextColor)
				.append(MENU_ICON_COLOR, mMenuIconColor)
				.append(NAVIGATION_BAR_BACKGROUND_COLOR, mNavigationBarBackgroundColor)
				.append(NAVIGATION_BAR_TEXT_COLOR, mNavigationBarTextColor)
				.append(NAVIGATION_BAR_BASKET_COLOR, mNavigationBarBasketColor)
				.append(CATEGORY_TITLE_POSITION, mCategoryTitlePosition)
				.append(TAKEAWAY, mTakeAwayInfo)
				.append(EATIN, mEatInInfo)
				.append(DELIVERY, mDeliveryInfo)
				.toString();
	}

	public boolean			isPersonalMessage() { return mPersonalMessage; }
	public boolean			isCategoryParallax() { return mCategoryParallax; }
	public int				getBackgroundColor() { return mBackgroundColor; }
	public int				getPriceTextColor() { return mPriceTextColor; }
	public int				getProductNameTextColor() { return mProductNameTextColor; }
	public int				getBuyPanelTextColor() { return mBuyPanelTextColor; }
	public int				getBuyPanelBackgroundColor() { return mBuyPanelBackgroundColor; }
	public int				getBuyOptionTextColor() { return mBuyOptionTextColor; }
	public int				getBuyButtonBackgroundColor() { return mBuyButtonBackgroundColor; }
	public int				getBuyButtonTextColor() { return mBuyButtonTextColor; }
	public String			getBuyButtonText() { return mBuyButtonText; }
	public int				getBuyButtonRadius() { return mBuyButtonRadius; }
	public String			getBuyButtonBackgroundImage() { return mBuyButtonBackgroundImage; }
	public int				getBuyBasketTextColor() { return mBuyBasketTextColor; }
	public int				getShareButtonColor() { return mShareButtonColor; }
	public String			getShareMessageText() { return mShareMessageText; }
	public String			getShareMailText() { return mShareMailText; }
	public int				getInfoTextColor() { return mInfoTextColor; }
	public int				getInfoButtonColor() { return mInfoButtonColor; }
	public int				getInfoBackgroundColor() { return mInfoBackgroundColor; }
	public String			getShareFacebookUrl() { return mShareFacebookUrl; }
	public String			getShareTwitterUrl() { return mShareTwitterUrl; }
	public String			getMenuBackgroundImage() { return mMenuBackgroundImage; }
	public int				getMenuTextColor() { return mMenuTextColor; }
	public int				getMenuBackgroundColor() { return mMenuBackgroundColor; }
	public int				getMenuIconColor() { return mMenuIconColor; }
	public int				getNavigationBarBackgroundColor() { return mNavigationBarBackgroundColor; }
	public int				getNavigationBarTextColor() { return mNavigationBarTextColor; }
	public int				getNavigationBarBasketColor() { return mNavigationBarBasketColor; }
	public String			getCategoryTitlePosition() { return mCategoryTitlePosition; }
	public DeliveryInfo		getTakeAwayInfo() { return mTakeAwayInfo; }
	public DeliveryInfo		getEatInInfo() { return mEatInInfo; }
	public DeliveryInfo		getDeliveryInfo() { return mDeliveryInfo; }

	public void				setPersonalMessage(boolean personalMessage) { mPersonalMessage = personalMessage; }
	public void				setCategoryParallax(boolean categoryParallax) { mCategoryParallax = categoryParallax; }
	public void				setBackgroundColor(int backgroundColor) { mBackgroundColor = backgroundColor; }
	public void				setPriceTextColor(int priceTextColor) { mPriceTextColor = priceTextColor; }
	public void				setProductNameTextColor(int productNameTextColor) { mProductNameTextColor = productNameTextColor; }
	public void				setBuyPanelBackgroundColor(int buyPanelBackgroundColor) { mBuyPanelBackgroundColor = buyPanelBackgroundColor; }
	public void				setBuyPanelTextColor(int buyPanelTextColor) { mBuyPanelTextColor = buyPanelTextColor; }
	public void				setBuyOptionTextColor(int buyOptionTextColor) { mBuyOptionTextColor = buyOptionTextColor; }
	public void				setBuyButtonBackgroundColor(int buyButtonBackgroundColor) { mBuyButtonBackgroundColor = buyButtonBackgroundColor; }
	public void				setBuyButtonTextColor(int buyButtonTextColor) { mBuyButtonTextColor = buyButtonTextColor; }
	public void				setBuyButtonText(String buyButtonText) { mBuyButtonText = buyButtonText; }
	public void				setBuyButtonRadius(int buyButtonRadius) { mBuyButtonRadius = buyButtonRadius; }
	public void				setBuyButtonBackgroundImage(String buyButtonBackgroundImage) { mBuyButtonBackgroundImage = buyButtonBackgroundImage; }
	public void				setBuyBasketTextColor(int buyBasketTextColor) { mBuyBasketTextColor = buyBasketTextColor; }
	public void				setShareButtonColor(int shareButtonColor) { mShareButtonColor = shareButtonColor; }
	public void				setShareMessageText(String shareMessageText) { mShareMessageText = shareMessageText; }
	public void				setShareMailText(String shareMailText) { mShareMailText = shareMailText; }
	public void				setInfoButtonColor(int infoButtonColor) { mInfoButtonColor = infoButtonColor; }
	public void				setInfoTextColor(int infoTextColor) { mInfoTextColor = infoTextColor; }
	public void				setInfoBackgroundColor(int infoBackgroundColor) { mInfoBackgroundColor = infoBackgroundColor; }
	public void				setShareFacebookUrl(String shareFacebookUrl) { mShareFacebookUrl = shareFacebookUrl; }
	public void				setShareTwitterUrl(String shareTwitterUrl) { mShareTwitterUrl = shareTwitterUrl; }
	public void				setMenuBackgroundImage(String menuBackgroundImage) { mMenuBackgroundImage = menuBackgroundImage; }
	public void				setMenuBackgroundColor(int menuBackgroundColor) { mMenuBackgroundColor = menuBackgroundColor; }
	public void				setMenuTextColor(int menuTextColor) { mMenuTextColor = menuTextColor; }
	public void				setMenuIconColor(int menuIconColor) { mMenuIconColor = menuIconColor; }
	public void				setNavigationBarBackgroundColor(int navigationBarBackgroundColor) { mNavigationBarBackgroundColor = navigationBarBackgroundColor; }
	public void				setNavigationBarTextColor(int navigationBarTextColor) { mNavigationBarTextColor = navigationBarTextColor; }
	public void				setNavigationBarBasketColor(int navigationBarBasketColor) { mNavigationBarBasketColor = navigationBarBasketColor; }
	public void				setCategoryTitlePosition(String categoryTitlePosition) { mCategoryTitlePosition = categoryTitlePosition; }
	public void				setTakeAwayInfo(DeliveryInfo takeAwayInfo) { mTakeAwayInfo = takeAwayInfo; }
	public void				setEatInInfo(DeliveryInfo eatInInfo) { mEatInInfo = eatInInfo; }
	public void				setDeliveryInfo(DeliveryInfo deliveryInfo) { mDeliveryInfo = deliveryInfo; }
}
