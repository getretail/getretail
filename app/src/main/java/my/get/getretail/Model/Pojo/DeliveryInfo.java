package my.get.getretail.model.pojo;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by qlitzler on 20/10/15.
 */
public class DeliveryInfo {

	public static final String			ACTIVE = "active";
	public static final String			SUCCESS_MESSAGE = "success_message";

	@SerializedName(ACTIVE)
	private boolean			mActive;
	@SerializedName(SUCCESS_MESSAGE)
	private String			mSuccessMessage;

	@Override
	public int			hashCode() {
		return new HashCodeBuilder()
				.appendSuper(super.hashCode())
				.append(mActive)
				.append(mSuccessMessage)
				.toHashCode();
	}

	@Override
	public boolean		equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		DeliveryInfo rhs = (DeliveryInfo) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(mActive, rhs.mActive)
				.append(mSuccessMessage, rhs.mSuccessMessage)
				.isEquals();
	}

	public String		toString() {
		return new ToStringBuilder(this)
				.append(ACTIVE, mActive)
				.append(SUCCESS_MESSAGE, mSuccessMessage)
				.toString();
	}

	public boolean		isActive() { return mActive; }
	public String		getSuccessMessage() { return mSuccessMessage; }

	public void			setActive(boolean active) { mActive = active; }
	public void			setSuccessMessage(String successMessage) { mSuccessMessage = successMessage; }
}
