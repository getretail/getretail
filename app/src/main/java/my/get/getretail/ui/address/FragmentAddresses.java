package my.get.getretail.ui.address;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import my.get.getretail.R;
import my.get.getretail.model.pojo.Address;
import my.get.getretail.model.pojo.Customer;
import my.get.getretail.network.NetworkFragmentRequest;
import my.get.getretail.network.request.RequestCustomer;
import my.get.getretail.ui.base.factory.FactoryViewHolder;
import my.get.getretail.ui.base.getcorp.AGetCorpFragment;
import my.get.getretail.ui.base.getcorp.AGetCorpFragmentList;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 29/10/15.
 */
public class FragmentAddresses extends AGetCorpFragmentList<Address> {

	public static final String				TAG = "ADDRESSES";

	private NetworkRequestLogin				mNetworkRequestLogin;

	public static FragmentAddresses			newInstance() {
		FragmentAddresses		fragment = new FragmentAddresses();
		fragment.mTypeViewHolder = FactoryViewHolder.ADDRESS_CUSTOMER;
		fragment.mIdLayout = R.layout.fragment_addresses;
		fragment.mIdTitle = R.string.title_addresses;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view =  super.onCreateView(inflater, container, savedInstanceState);
		performLogin();
		return view;
	}

	@Override
	public void			onDestroy() {
		super.onDestroy();
		mNetworkRequestLogin = null;
	}

	@Override
	public void			findViewsById(View view) {
		super.findViewsById(view);
	}

	@Override
	public void			factories() {
		super.factories();
		mNetworkRequestLogin = new NetworkRequestLogin(this);
	}

	@Override
	public void			updateUI(Context context) {
		super.updateUI(context);
	}

	@Override
	public void			callbackUpdateUI() {
		super.callbackUpdateUI();
	}

	private void		performLogin() {
		RequestCustomer.login(mActivity.mModuleApi.getService()).enqueue(mNetworkRequestLogin);
	}

	static class NetworkRequestLogin extends NetworkFragmentRequest<Customer> {

		NetworkRequestLogin(AGetCorpFragment fragment) {
			super(fragment);
		}

		@Override
		public void		onResponse(Response<Customer> response, Retrofit retrofit) {
			if (response.errorBody() != null) {
				System.out.println("Network problem");
			} else {
				FragmentAddresses	fragment = (FragmentAddresses) mFragment.get();
				if (fragment != null) {
					fragment.mList = response.body().getAddresses();
					fragment.callbackUpdateUI();
				}
			}
		}
	}
}
