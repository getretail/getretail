package my.get.getretail.ui.base.viewholder.history;

import android.view.View;
import android.widget.TextView;

import my.get.getretail.R;
import my.get.getretail.model.pojo.GroupOrderHistory;
import my.get.getretail.ui.base.getcorp.AGetCorpViewHolder;
import my.get.getretail.ui.history.AFragmentHistory;

/**
 * Created by qlitzler on 20/11/15.
 */
public class VHHistory extends AGetCorpViewHolder<GroupOrderHistory> {

	protected TextView		mLabelHistory;

	public VHHistory(AFragmentHistory fragment, View viewItem, int viewType) {
		super(fragment, viewItem, viewType);
	}

	@Override
	public void				findViewsById(View view, int viewType) {
		mLabelHistory = (TextView) view.findViewById(R.id.label_history);
	}

	@Override
	public void				setFields(GroupOrderHistory orderHistory, int position) {
		setLabelHistory(orderHistory.getLabelCard());
	}

	@Override
	public void				onClick(View view) {

	}

	public void				setLabelHistory(CharSequence labelCard) {
		mLabelHistory.setText(labelCard);
	}
}
