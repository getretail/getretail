package my.get.getretail.ui.history;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import my.get.getretail.R;
import my.get.getretail.ui.base.getcorp.AGetCorpAdapterPager;
import my.get.getretail.ui.base.factory.FactoryFragment;

/**
 * Created by qlitzler on 05/11/15.
 */
public class AdapterHistoryPager extends AGetCorpAdapterPager {

	private static final int		TAB_COUNT = 2;
	public static final int			TAB_CURRENT = 0;
	public static final int			TAB_PAST = 1;

	public AdapterHistoryPager(FragmentManager fragmentManager) {
		super(fragmentManager);
	}

	public void			addTabs(Context context, TabLayout tabLayout) {
		tabLayout.addTab(tabLayout.newTab().setText(context.getString(R.string.title_current_orders)));
		tabLayout.addTab(tabLayout.newTab().setText(context.getString(R.string.title_past_orders)));
		tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
	}

	@Override
	public Fragment		getItem(int position) {

		int typeFragment;

		switch (position) {
			case TAB_CURRENT:
				typeFragment = FactoryFragment.HISTORY_CURRENT;
				break;
			case TAB_PAST:
				typeFragment = FactoryFragment.HISTORY_PAST;
				break;
			default:
				typeFragment = -1;
				break;
		}
		return FactoryFragment.newInstance(typeFragment, null);
	}

	@Override
	public int			getCount() {
		return TAB_COUNT;
	}
}
