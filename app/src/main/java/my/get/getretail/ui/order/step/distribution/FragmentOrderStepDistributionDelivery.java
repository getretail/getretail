package my.get.getretail.ui.order.step.distribution;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import my.get.getretail.R;
import my.get.getretail.model.pojo.Schedule;
import my.get.getretail.network.NetworkFragmentRequest;
import my.get.getretail.network.request.RequestDelivery;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 09/11/15.
 */
public class FragmentOrderStepDistributionDelivery extends AFragmentOrderStepDistribution {

	public static final String							TAG = "ORDER_STEP_DISTRIBUTION_DELIVERY";

	private int											mIdStore;
	private int											mIdAddress;
	private Schedule									mSchedule;

	private NetworkRequestDelivery						mNetworkRequestDelivery;

	public static FragmentOrderStepDistributionDelivery	newInstance(Bundle args) {
		FragmentOrderStepDistributionDelivery fragment = new FragmentOrderStepDistributionDelivery();
		fragment.mIdLayout = R.layout.fragment_order_step_distribution;
		fragment.mIdTitle = R.string.title_delivery;
		fragment.mTypeOrderDelivery = args.getInt(TYPE_ORDER_DELIVERY);
		fragment.mIdAddress = args.getInt(ID_ADDRESS);
		fragment.mIdStore = args.getInt(ID_STORE);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		fetchSchedule();
		return view;
	}

	@Override
	public void			onDestroy() {
		super.onDestroy();
	}

	@Override
	public void			findViewsById(View view) {

	}

	@Override
	public void			factories() {
		mNetworkRequestDelivery = new NetworkRequestDelivery(this);
	}

	@Override
	public void			updateUI(Context context) {

	}

	@Override
	public void			callbackUpdateUI() {

	}

	private void		fetchSchedule() {
		RequestDelivery.getSchedule(mActivity.mModuleApi.getService(), mIdAddress, mIdStore).enqueue(mNetworkRequestDelivery);
	}

	static class NetworkRequestDelivery extends NetworkFragmentRequest<Schedule> {

		NetworkRequestDelivery(FragmentOrderStepDistributionDelivery fragment) {
			super(fragment);
		}

		@Override
		public void		onResponse(Response<Schedule> response, Retrofit retrofit) {
			if (response.errorBody() != null) {
				System.out.println("Network problem");
			} else {
				FragmentOrderStepDistributionDelivery fragment = (FragmentOrderStepDistributionDelivery) mFragment.get();
				if (fragment != null) {
					fragment.mSchedule = response.body();
					fragment.callbackUpdateUI();
				}
			}
		}
	}
}
