package my.get.getretail.ui.cart;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import my.get.getretail.R;
import my.get.getretail.model.pojo.Basket;
import my.get.getretail.model.pojo.Customer;
import my.get.getretail.network.NetworkFragmentRequest;
import my.get.getretail.network.request.RequestCustomer;
import my.get.getretail.ui.base.getcorp.AGetCorpFragment;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 29/10/15.
 */
public class FragmentCart extends AGetCorpFragment {

	public static final String		TAG = "BASKET";

	private NetworkRequestLogin		mNetworkRequestLogin;
	private List<Basket>		mCarts;

	public static FragmentCart		newInstance() {
		FragmentCart		fragment = new FragmentCart();
		fragment.mIdLayout = R.layout.fragment_cart;
		fragment.mIdTitle = R.string.title_cart;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		fetchShoppingCart();
		return view;
	}

	@Override
	public void			onDestroy() {
		super.onDestroy();
		mNetworkRequestLogin = null;
	}

	@Override
	public void			findViewsById(View view) {

	}

	@Override
	public void			factories() {
		mNetworkRequestLogin = new NetworkRequestLogin(this);
	}

	@Override
	public void			updateUI(Context context) {

	}

	@Override
	public void			callbackUpdateUI() {

	}

	private void		fetchShoppingCart() {
		RequestCustomer.login(mActivity.mModuleApi.getService()).enqueue(mNetworkRequestLogin);
	}

	static class NetworkRequestLogin extends NetworkFragmentRequest<Customer> {

		NetworkRequestLogin(AGetCorpFragment fragment) {
			super(fragment);
		}

		@Override
		public void		onResponse(Response<Customer> response, Retrofit retrofit) {
			if (response.errorBody() != null) {
				System.out.println("Network problem");
			} else {
				FragmentCart fragment = (FragmentCart) mFragment.get();
				if (fragment != null) {
					fragment.mCarts = response.body().getShoppingCarts();
					fragment.callbackUpdateUI();
				}
			}
		}
	}
}
