package my.get.getretail.ui.home;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import my.get.getretail.R;
import my.get.getretail.ui.base.factory.FactoryViewHolder;
import my.get.getretail.ui.base.getcorp.AGetCorpActivity;
import my.get.getretail.ui.base.getcorp.AGetCorpViewHolder;

/**
 * Created by qlitzler on 29/10/15.
 */
public class AdapterDrawer extends RecyclerView.Adapter<AGetCorpViewHolder> {

	public static final int			STORES = 1;
	public static final int			ADDRESSES = 2;
	public static final int			CARDS = 3;
	public static final int			HISTORY = 4;
	public static final int			PROFILE = 5;
	public static final int			CART = 6;
	public static final int			CONCEPT = 7;
	public static final int			SUPPORT = 8;
	public static final int			DRAWER_ITEM_COUNT = 9;
	public static final int			HEADER = 100;
	public static final int			TITLE = 101;

	protected AGetCorpActivity		mActivity;
	protected LayoutInflater		mLayoutInflater;
	private int						mTypeViewHolder;

	public AdapterDrawer(AGetCorpActivity activity, int typeViewHolder) {
		mTypeViewHolder = typeViewHolder;
		mActivity = activity;
		mLayoutInflater = LayoutInflater.from(mActivity.getBaseContext());
	}

	@Override
	public void							onBindViewHolder(AGetCorpViewHolder holder, int position) {
		holder.setFields(null, position);
	}

	@Override
	public AGetCorpViewHolder			onCreateViewHolder(ViewGroup parent, int viewType) {
		View	view;
		int		idLayout;

		idLayout = FactoryViewHolder.getIdLayout(mTypeViewHolder);
		view = viewType(parent, viewType, idLayout);
		return FactoryViewHolder.newInstance(mTypeViewHolder, mActivity, view, viewType);
	}

	public View							viewType(ViewGroup parent, int viewType, int idLayout) {
		if (viewType == HEADER) {
			return mLayoutInflater.inflate(R.layout.single_drawer_header_item, parent, false);
		} else {
			return mLayoutInflater.inflate(R.layout.single_drawer_item, parent, false);
		}
	}

	@Override
	public int							getItemViewType(int position) {
		if (position == 0)
			return HEADER;
		return TITLE;
	}

	@Override
	public int							getItemCount() {
		return DRAWER_ITEM_COUNT;
	}
}
