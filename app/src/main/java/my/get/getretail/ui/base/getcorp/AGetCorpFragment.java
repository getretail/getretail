package my.get.getretail.ui.base.getcorp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by qlitzler on 30/10/15.
 */
public abstract class AGetCorpFragment extends android.support.v4.app.Fragment implements IGetCorpMethods {

	private static final String	SAVE_ID_LAYOUT = "id_layout";
	private static final String	SAVE_ID_TITLE = "id_title";

	protected AGetCorpActivity	mActivity;
	protected int				mIdLayout;
	protected int				mIdTitle;

	@Override
	public void					onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			mIdLayout = savedInstanceState.getInt(SAVE_ID_LAYOUT);
			mIdTitle = savedInstanceState.getInt(SAVE_ID_TITLE);
		}
		mActivity = (AGetCorpActivity) getActivity();
		mActivity.setTitle(mIdTitle);
	}

	@Override
	public void					onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(SAVE_ID_LAYOUT, mIdLayout);
		outState.putInt(SAVE_ID_TITLE, mIdTitle);
	}

	@Override
	public View					onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(mIdLayout, container, false);
		findViewsById(view);
		factories();
		updateUI(getContext());
		return view;
	}

	@Override
	public void					onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

}
