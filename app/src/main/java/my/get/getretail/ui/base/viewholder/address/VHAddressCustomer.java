package my.get.getretail.ui.base.viewholder.address;

import android.view.View;

import my.get.getretail.ui.address.FragmentAddresses;

/**
 * Created by qlitzler on 17/11/15.
 */
public class VHAddressCustomer extends AVHAddress {

	public VHAddressCustomer(FragmentAddresses fragment, View viewItem, int viewType) {
		super(fragment, viewItem, viewType);
	}

	@Override
	public void				onClick(View view) {

	}
}
