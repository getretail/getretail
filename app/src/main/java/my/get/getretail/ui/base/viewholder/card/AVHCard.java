package my.get.getretail.ui.base.viewholder.card;

import android.view.View;
import android.widget.TextView;

import my.get.getretail.R;
import my.get.getretail.model.pojo.Card;
import my.get.getretail.ui.base.getcorp.AGetCorpFragment;
import my.get.getretail.ui.base.getcorp.AGetCorpViewHolder;

/**
 * Created by qlitzler on 17/11/15.
 */
public class AVHCard extends AGetCorpViewHolder<Card> {

	protected TextView		mLabelCard;

	public AVHCard(AGetCorpFragment fragment, View viewItem, int viewType) {
		super(fragment, viewItem, viewType);
	}

	@Override
	public void				findViewsById(View view, int viewType) {
		mLabelCard = (TextView) view.findViewById(R.id.label_card);
	}

	@Override
	public void				setFields(Card card, int position) {
		setLabelCard(card.getLabel());
	}

	@Override
	public void				onClick(View view) {

	}

	public void				setLabelCard(CharSequence labelCard) {
		mLabelCard.setText(labelCard);
	}
}
