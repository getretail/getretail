package my.get.getretail.ui.base.factory;

import android.view.View;

import my.get.getretail.R;
import my.get.getretail.ui.address.FragmentAddresses;
import my.get.getretail.ui.base.getcorp.AGetCorpActivity;
import my.get.getretail.ui.base.getcorp.AGetCorpFragmentList;
import my.get.getretail.ui.base.getcorp.AGetCorpViewHolder;
import my.get.getretail.ui.base.viewholder.address.VHAddressCustomer;
import my.get.getretail.ui.base.viewholder.address.VHAddressCustomerForResult;
import my.get.getretail.ui.base.viewholder.address.VHAddressShop;
import my.get.getretail.ui.base.viewholder.card.VHCard;
import my.get.getretail.ui.base.viewholder.card.VHCardForResult;
import my.get.getretail.ui.base.viewholder.history.VHHistory;
import my.get.getretail.ui.base.viewholder.home.VHDrawer;
import my.get.getretail.ui.card.FragmentCards;
import my.get.getretail.ui.history.AFragmentHistory;
import my.get.getretail.ui.order.step.address.FragmentOrderStepAddressCustomer;
import my.get.getretail.ui.order.step.address.FragmentOrderStepAddressShop;
import my.get.getretail.ui.order.step.card.FragmentOrderStepCard;

/**
 * Created by qlitzler on 17/11/15.
 */
public class FactoryViewHolder {

	private static final int		ERROR = -1;
	public static final int			DRAWER = 0;
	public static final int			ADDRESS_CUSTOMER = 1;
	public static final int			ADDRESS_CUSTOMER_FOR_RESULT = 2;
	public static final int			ADDRESS_SHOP = 3;
	public static final int			CARDS = 4;
	public static final int			CARDS_FOR_RESULT = 5;
	public static final int			HISTORY = 6;

	public static AGetCorpViewHolder	newInstance(
			int typeViewHolder,
			AGetCorpActivity activity,
			View viewItem,
			int viewType
	) {
		AGetCorpViewHolder		viewHolder = null;

		switch (typeViewHolder) {
			case DRAWER:
				viewHolder = new VHDrawer(
						activity,
						viewItem,
						viewType
				);
				break;
		}
		return  viewHolder;
	}

	public static AGetCorpViewHolder	newInstance(
			int typeViewHolder,
			AGetCorpFragmentList fragment,
			View viewItem,
			int viewType
	) {
		AGetCorpViewHolder				viewHolder = null;

		switch (typeViewHolder) {
			case ADDRESS_CUSTOMER:
				viewHolder =  new VHAddressCustomer(
						(FragmentAddresses)fragment,
						viewItem,
						viewType);
				break;
			case ADDRESS_CUSTOMER_FOR_RESULT:
				viewHolder = new VHAddressCustomerForResult(
						(FragmentOrderStepAddressCustomer)fragment,
						viewItem,
						viewType
				);
				break;
			case ADDRESS_SHOP:
				viewHolder = new VHAddressShop(
						(FragmentOrderStepAddressShop)fragment,
						viewItem,
						viewType
				);
				break;
			case CARDS:
				viewHolder = new VHCard(
						(FragmentCards)fragment,
						viewItem,
						viewType
				);
				break;
			case CARDS_FOR_RESULT:
				viewHolder = new VHCardForResult(
						(FragmentOrderStepCard)fragment,
						viewItem,
						viewType
				);
				break;
			case HISTORY:
				viewHolder = new VHHistory(
						(AFragmentHistory)fragment,
						viewItem,
						viewType
				);
				break;
		}
		return viewHolder;
	}

	public static int					getIdLayout(int typeViewHolder) {
		int								idLayout = ERROR;

		switch (typeViewHolder) {
			case DRAWER:
				idLayout = R.layout.single_drawer_item;
				break;
			case ADDRESS_CUSTOMER:
			case ADDRESS_CUSTOMER_FOR_RESULT:
				idLayout = R.layout.single_address_customer;
				break;
			case ADDRESS_SHOP:
				idLayout = R.layout.single_address_shop;
				break;
			case CARDS:
			case CARDS_FOR_RESULT:
				idLayout = R.layout.single_card;
				break;
			case HISTORY:
				idLayout = R.layout.single_history;
				break;
		}
		return idLayout;
	}
}
