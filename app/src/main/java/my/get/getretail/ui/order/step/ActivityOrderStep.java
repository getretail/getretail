package my.get.getretail.ui.order.step;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import my.get.getretail.R;
import my.get.getretail.ui.base.factory.FactoryFragment;
import my.get.getretail.ui.base.getcorp.AGetCorpActivity;

/**
 * Created by qlitzler on 01/11/15.
 */
public class ActivityOrderStep extends AGetCorpActivity {

	public static final String		TYPE_ORDER_STEP = "type_order_step";
	public static final String		TYPE_ORDER_DISTRIBUTION = "type_order_distribution";
	public static final String		ID_STORE = "id_store";

	@Override
	protected void			onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected int 			getLayoutId() {
		return R.layout.activity_order_step;
	}

	@Override
	public boolean			onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void			onDestroy() {
		super.onDestroy();
	}

	@Override
	public void				findViewsById(View view) {
	}

	@Override
	public void				factories() {
		Intent intent = getIntent();
		int		typeOrderStep = intent.getIntExtra(TYPE_ORDER_STEP, -1);
		if (mFragmentManager.findFragmentByTag(
				FactoryFragment.getTag(typeOrderStep)) == null
				) {
			mFragmentManager.beginTransaction()
					.replace(
							R.id.fragment_holder,
							FactoryFragment.newInstance(typeOrderStep, intent.getExtras()),
							FactoryFragment.getTag(typeOrderStep)
					)
					.commit();
		}
	}

	@Override
	public void				updateUI(Context context) {

	}

	@Override
	public void				callbackUpdateUI() {

	}
}
