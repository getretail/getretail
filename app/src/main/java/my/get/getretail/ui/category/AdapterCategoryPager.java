package my.get.getretail.ui.category;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.List;

import my.get.getretail.model.pojo.Category;
import my.get.getretail.model.pojo.Store;
import my.get.getretail.ui.base.getcorp.AGetCorpAdapterPager;
import my.get.getretail.ui.base.factory.FactoryFragment;

/**
 * Created by qlitzler on 04/11/15.
 */
public class AdapterCategoryPager extends AGetCorpAdapterPager<Store> {

	public static final String		ID_STORE = "id_store";
	public static final String		ID_CATEGORY = "id_category";

	public AdapterCategoryPager(FragmentManager fragmentManager, Store store) {
		super(fragmentManager, store);
	}

	public void				addTabs(Context context, TabLayout tabLayout) {
		List<Category> categories = mObject.getCategories();
		for (Category category : categories) {
			tabLayout.addTab(tabLayout.newTab().setText(category.getLabel()));
		}
		tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
	}

	@Override
	public Fragment			getItem(int position) {
		Bundle args = new Bundle(2);

		args.putInt(ID_STORE, mObject.getId());
		args.putInt(ID_CATEGORY, mObject.getCategories().get(0).getId());
		return FactoryFragment.newInstance(FactoryFragment.PRODUCTS, args);
	}

	@Override
	public int				getCount() {
		return mObject != null ? mObject.getCategories().size() : 0;
	}
}
