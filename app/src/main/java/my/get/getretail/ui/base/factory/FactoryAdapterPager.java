package my.get.getretail.ui.base.factory;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import my.get.getretail.model.pojo.Store;
import my.get.getretail.ui.base.getcorp.AGetCorpAdapterPager;
import my.get.getretail.ui.category.AdapterCategoryPager;
import my.get.getretail.ui.history.AdapterHistoryPager;
import my.get.getretail.ui.order.AdapterDistributionPager;

/**
 * Created by qlitzler on 05/11/15.
 */
public class FactoryAdapterPager {

	public static final int		HISTORY = 0;
	public static final int		DELIVERY = 1;
	public static final int		CATEGORY = 2;

	public static AGetCorpAdapterPager	newInstance(
			int typePagerAdapter,
			FragmentManager fragmentManager,
			Object object
	) {
		AGetCorpAdapterPager			pagerAdapter;

		switch (typePagerAdapter) {
			case HISTORY:
				pagerAdapter = new AdapterHistoryPager(fragmentManager);
				break;
			case DELIVERY:
				pagerAdapter = new AdapterDistributionPager(fragmentManager, (Bundle)object);
				break;
			case CATEGORY:
				pagerAdapter = new AdapterCategoryPager(fragmentManager, (Store)object);
				break;
			default:
				pagerAdapter = null;
				break;
		}
		return pagerAdapter;
	}
}
