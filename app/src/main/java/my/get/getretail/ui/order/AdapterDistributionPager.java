package my.get.getretail.ui.order;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import my.get.getretail.R;
import my.get.getretail.ui.base.factory.FactoryFragment;
import my.get.getretail.ui.base.getcorp.AGetCorpAdapterPager;
import my.get.getretail.ui.order.step.ActivityOrderStep;

/**
 * Created by qlitzler on 31/10/15.
 */
public class AdapterDistributionPager extends AGetCorpAdapterPager<Bundle> {

	private static final int		TAB_COUNT = 3;
	private static final int		TAB_EAT_IN = 0;
	private static final int		TAB_TAKEAWAY = 1;
	private static final int		TAB_DELIVERY = 2;

	public AdapterDistributionPager(FragmentManager fragmentManager, Bundle args) {
		super(fragmentManager, args);
	}

	public void				addTabs(Context context, TabLayout tabLayout) {
		tabLayout.addTab(tabLayout.newTab().setText(context.getString(R.string.title_eat_in)));
		tabLayout.addTab(tabLayout.newTab().setText(context.getString(R.string.title_takeaway)));
		tabLayout.addTab(tabLayout.newTab().setText(context.getString(R.string.title_delivery)));
		tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
	}

	@Override
	public Fragment			getItem(int position) {
		int		typeFragment;

		switch (position) {
			case TAB_EAT_IN:
				typeFragment = FactoryFragment.ORDER_EAT_IN;
				break;
			case TAB_TAKEAWAY:
				typeFragment = FactoryFragment.ORDER_TAKEAWAY;
				break;
			case TAB_DELIVERY:
				typeFragment = FactoryFragment.ORDER_DELIVERY;
				break;
			default:
				typeFragment = -1;
				break;
		}
		mObject.putInt(ActivityOrderStep.TYPE_ORDER_DISTRIBUTION, typeFragment);
		return FactoryFragment.newInstance(typeFragment, mObject);
	}

	@Override
	public int				getCount() {
		return TAB_COUNT;
	}
}
