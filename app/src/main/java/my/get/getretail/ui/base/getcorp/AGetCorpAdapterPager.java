package my.get.getretail.ui.base.getcorp;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by qlitzler on 05/11/15.
 */
public abstract class AGetCorpAdapterPager<T> extends FragmentStatePagerAdapter {

	protected T			mObject;

	public AGetCorpAdapterPager(FragmentManager fragmentManager) {
		super(fragmentManager);
	}

	public AGetCorpAdapterPager(FragmentManager fragmentManager, T object) {
		super(fragmentManager);
		mObject = object;
	}

	public abstract	void	addTabs(Context context, TabLayout tabLayout);

	public void				setObject(T object) { mObject = object; }
}
