package my.get.getretail.ui.order;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;

import my.get.getretail.R;
import my.get.getretail.model.pojo.OrderStep;
import my.get.getretail.ui.base.factory.FactoryOrderStep;

/**
 * Created by qlitzler on 31/10/15.
 */
public class FragmentOrderTakeaway extends AFragmentOrder {

	public static final String			TAG = "ORDER_TAKEAWAY";

	public static FragmentOrderTakeaway	newInstance(Bundle args) {
		FragmentOrderTakeaway		fragment = new FragmentOrderTakeaway();
		fragment.mIdLayout = R.layout.fragment_order;
		fragment.mIdTitle = R.string.title_order;
		fragment.mTypeOrderDistribution = args.getInt(TYPE_ORDER_DISTRIBUTION);
		fragment.mLabelCard = args.getString(ActivityOrder.LABEL_CARD);
		fragment.mLabelAddressShop = args.getString(ActivityOrder.LABEL_ADDRESS_SHOP);
		return fragment;
	}

	@Override
	public void		onDestroy() {
		super.onDestroy();
	}


	@Override
	public void		factories() {
		super.factories();
		OrderStep stepAddress = FactoryOrderStep.newInstance(FactoryOrderStep.ADDRESS_SHOPS, mTypeOrderDistribution);
		if (mLabelAddressShop != null) {
			stepAddress.setLabel(mLabelAddressShop);
		}
		mOrderSteps.add(stepAddress);
		mOrderSteps.add(FactoryOrderStep.newInstance(FactoryOrderStep.DISTRIBUTION_STORE, mTypeOrderDistribution));
	}

	@Override
	public void		findViewsById(View view) {
		mGridView = (GridView) view.findViewById(R.id.grid);
	}

	@Override
	public void		updateUI(Context context) {
		super.updateUI(context);
	}

	@Override
	public void		callbackUpdateUI() {

	}
}
