package my.get.getretail.ui.order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import my.get.getretail.R;
import my.get.getretail.model.pojo.OrderStep;

/**
 * Created by qlitzler on 01/11/15.
 */
public class AdapterGridOrderSteps extends BaseAdapter {

	private List<OrderStep>		mOrderSteps;

	private Context				mContext;

	AdapterGridOrderSteps(Context context, List<OrderStep> orderSteps) {
		mOrderSteps = orderSteps;
		mContext = context;
	}

	@Override
	public int			getCount() {
		return mOrderSteps.size();
	}

	@Override
	public Object		getItem(int pos) {
		return mOrderSteps.get(pos);
	}

	@Override
	public long			getItemId(int position) {
		return position;
	}

	public int			getTypeOrderStep(int position) {
		return mOrderSteps.get(position).getTypeOrderStep();
	}

	public int			getTypeOrderDelivery(int position) {
		return mOrderSteps.get(position).getTypeOrderDelivery();
	}

	@Override
	public View			getView(int position, View convertView, ViewGroup parent) {
		View				view;
		ViewHolderSteps		viewHolder;

		view = convertView;
		if (convertView == null) {
			LayoutInflater	inflater = LayoutInflater.from(mContext);
			view = inflater.inflate(R.layout.single_step_item, null);
			viewHolder = new ViewHolderSteps(view);
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolderSteps) view.getTag();
		}
		OrderStep		orderStep = mOrderSteps.get(position);

		viewHolder.setTitle(orderStep.getTitle());
		viewHolder.setLabel(orderStep.getLabel());
		return view;
	}

	static class ViewHolderSteps {

		private TextView		mTitleOrderStep;
		private TextView		mLabelOrderStep;

		public ViewHolderSteps(View view) {
			mTitleOrderStep = (TextView)view.findViewById(R.id.title_order_step);
			mLabelOrderStep = (TextView)view.findViewById(R.id.label_order_step);
		}

		public void		setTitle(CharSequence title) {
			mTitleOrderStep.setText(title);
		}
		public void		setLabel(CharSequence label) {
			mLabelOrderStep.setText(label);
		}
	}
}
