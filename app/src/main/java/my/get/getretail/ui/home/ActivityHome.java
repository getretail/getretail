package my.get.getretail.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import my.get.getretail.R;
import my.get.getretail.ui.base.factory.FactoryFragment;
import my.get.getretail.ui.base.factory.FactoryViewHolder;
import my.get.getretail.ui.base.getcorp.AGetCorpActivity;

/**
 * Created by qlitzler on 30/10/15.
 */
public class ActivityHome extends AGetCorpActivity {

	private static String						SELECTED_FRAGMENT_TYPE = "selected_fragment_type";

	public int									mSelectedFragmentType;

	public RelativeLayout						mFragmentHolder;
	private RecyclerView.LayoutManager			mLayoutManager;
	private AdapterDrawer						mAdapterDrawer;
	private RecyclerView						mRecyclerView;

	private ActionBarDrawerToggle				mDrawerToggle;

	@Override
	protected void			onCreate(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			mSelectedFragmentType = savedInstanceState.getInt(SELECTED_FRAGMENT_TYPE);
		} else {
			mSelectedFragmentType = FactoryFragment.STORES;
		}
		super.onCreate(savedInstanceState);
	}

	@Override
	protected int getLayoutId() {
		return R.layout.activity_home;
	}

	@Override
	public void				onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(SELECTED_FRAGMENT_TYPE, mSelectedFragmentType);
	}

	@Override
	protected void			onDestroy() {
		super.onDestroy();
	}

	@Override
	public boolean			onOptionsItemSelected(android.view.MenuItem item) {
		return mDrawerToggle.onOptionsItemSelected(item);
	}

	@Override
	public void				findViewsById(View view) {
		mFragmentHolder = (RelativeLayout) findViewById(R.id.fragment_holder);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mRecyclerView = (RecyclerView) findViewById(R.id.drawer_list_item);
	}

	@Override
	public void				factories() {
		if (mFragmentManager.findFragmentByTag(FactoryFragment.getTag(mSelectedFragmentType)) == null) {
			mFragmentManager.beginTransaction()
					.replace(
							R.id.fragment_holder,
							FactoryFragment.newInstance(mSelectedFragmentType, null),
							FactoryFragment.getTag(mSelectedFragmentType)
					)
					.commit();
		}
		mAdapterDrawer = new AdapterDrawer(this, FactoryViewHolder.DRAWER);
		mLayoutManager = new LinearLayoutManager(this);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open_drawer, R.string.close_drawer) {

			@Override
			public void		onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
			}

			@Override
			public void		onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
			}
		};
	}

	@Override
	public void				updateUI(Context context) {
		mRecyclerView.setHasFixedSize(true);
		mRecyclerView.setAdapter(mAdapterDrawer);
		mRecyclerView.setLayoutManager(mLayoutManager);
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerToggle.syncState();
	}

	@Override
	public void 			callbackUpdateUI() {

	}
}
