package my.get.getretail.ui.splash;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import my.get.getretail.R;
import my.get.getretail.model.pojo.Customer;
import my.get.getretail.network.NetworkActivityRequest;
import my.get.getretail.network.request.RequestCustomer;
import my.get.getretail.ui.base.NavigationDispatcher;
import my.get.getretail.ui.base.getcorp.AGetCorpActivity;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 25/11/15.
 */
public class ActivitySplashScreen extends AGetCorpActivity {

	private Customer						mCustomer;
	private NetworkRequestLogin				mNetworkRequestLogin;

	@Override
	protected void			onCreate(Bundle savedInstanceState) {
		mIdLayout = R.layout.activity_splash_screen;
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void			onDestroy() {
		super.onDestroy();
	}

	@Override
	public void				findViewsById(View view) {

	}

	@Override
	public void				factories() {
		mNetworkRequestLogin = new NetworkRequestLogin(this);
	}

	@Override
	public void				updateUI(Context context) {

	}

	@Override
	public void				callbackUpdateUI() {
		mModuleCustomer.setCustomer(mCustomer);
		NavigationDispatcher.activityHome(this);
	}

	@Override
	public void				performRequest() {
		RequestCustomer.login(mModuleApi.getService()).enqueue(mNetworkRequestLogin);
	}

	static class NetworkRequestLogin extends NetworkActivityRequest<Customer> {

		NetworkRequestLogin(AGetCorpActivity activity) {
			super(activity);
		}

		@Override
		public void			onResponse(Response<Customer> response, Retrofit retrofit) {
			if (response.errorBody() != null) {
				System.out.println("Network problem");
			} else {
				ActivitySplashScreen fragment = (ActivitySplashScreen) mActivity.get();
				if (fragment != null) {
					fragment.mCustomer = response.body();
					fragment.callbackUpdateUI();
				}
			}
		}
	}
}
