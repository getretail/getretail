package my.get.getretail.ui.order.step;

import my.get.getretail.ui.base.getcorp.AGetCorpFragment;

/**
 * Created by qlitzler on 01/11/15.
 */
public abstract class AFragmentOrderStep extends AGetCorpFragment {

	protected static final String		TYPE_ORDER_DELIVERY = "type_order_delivery";
	protected static final String		ID_STORE = "id_store";
	protected static final String		ID_SHOP = "id_shop";
	protected static final String		ID_ADDRESS = "id_address";

	protected int						mTypeOrderDelivery;
}
