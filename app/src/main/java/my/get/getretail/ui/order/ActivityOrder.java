package my.get.getretail.ui.order;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;

import java.lang.ref.WeakReference;

import my.get.getretail.R;
import my.get.getretail.ui.base.factory.FactoryAdapterPager;
import my.get.getretail.ui.base.getcorp.AGetCorpActivity;

/**
 * Created by qlitzler on 31/10/15.
 */
public class ActivityOrder extends AGetCorpActivity {

	public static final String				LABEL_ADDRESS_CUSTOMER = "label_address_customer";
	public static final String				LABEL_ADDRESS_SHOP = "label_address_shop";
	public static final String				LABEL_CARD = "label_card";

	private ViewPager						mViewPager;
	private TabLayout						mTabLayout;

	private AdapterDistributionPager		mAdapterDistributionPager;
	private ListenerSelectedTab				mListenerSelectedTab;
	private ViewPager.OnPageChangeListener	mListenerOnPageChange;

	private Bundle							mBundle;

	@Override
	protected void			onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected int			getLayoutId() {
		return R.layout.activity_order;
	}

	@Override
	protected void			onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			saveBundle(data.getExtras());
			mAdapterDistributionPager.setObject(mBundle);
			int position = mViewPager.getCurrentItem();
			mViewPager.setAdapter(mAdapterDistributionPager);
			mViewPager.setCurrentItem(position);
		}
	}

	@Override
	public boolean			onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void			onDestroy() {
		super.onDestroy();
		mListenerOnPageChange = null;
		mListenerSelectedTab = null;
	}

	private void			saveBundle(Bundle args) {
		String		labelAddressCustomer = args.getString(LABEL_ADDRESS_CUSTOMER);
		String		labelAddressShop = args.getString(LABEL_ADDRESS_SHOP);
		String		labelCard = args.getString(LABEL_CARD);

		if (labelAddressCustomer != null) {
			mBundle.putString(LABEL_ADDRESS_CUSTOMER, labelAddressCustomer);
		}
		if (labelAddressShop != null) {
			mBundle.putString(LABEL_ADDRESS_SHOP, labelAddressShop);
		}
		if (labelCard != null) {
			mBundle.putString(LABEL_CARD, labelCard);
		}
	}

	@Override
	public void				findViewsById(View view) {
		mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
		mViewPager = (ViewPager) findViewById(R.id.pager);
	}

	@Override
	public void				factories() {
		mBundle = new Bundle(3);
		mAdapterDistributionPager = (AdapterDistributionPager) FactoryAdapterPager.newInstance(
				FactoryAdapterPager.DELIVERY,
				getSupportFragmentManager(),
				mBundle
		);
		mListenerOnPageChange = new TabLayout.TabLayoutOnPageChangeListener(mTabLayout);
		mListenerSelectedTab = new ListenerSelectedTab(this);
	}

	@Override
	public void				updateUI(Context context) {
		mAdapterDistributionPager.addTabs(getBaseContext(), mTabLayout);
		mViewPager.setAdapter(mAdapterDistributionPager);
		mViewPager.addOnPageChangeListener(mListenerOnPageChange);
		mTabLayout.setOnTabSelectedListener(mListenerSelectedTab);
	}

	@Override
	public void				callbackUpdateUI() {

	}

	static class			ListenerSelectedTab implements TabLayout.OnTabSelectedListener {

		private WeakReference<ActivityOrder>		mActivity;

		ListenerSelectedTab(ActivityOrder activity) {
			mActivity = new WeakReference<>(activity);
		}

		@Override
		public void			onTabSelected(TabLayout.Tab tab) {
			mActivity.get().mViewPager.setCurrentItem(tab.getPosition());
		}

		@Override
		public void			onTabUnselected(TabLayout.Tab tab) {

		}

		@Override
		public void			onTabReselected(TabLayout.Tab tab) {

		}
	}
}
