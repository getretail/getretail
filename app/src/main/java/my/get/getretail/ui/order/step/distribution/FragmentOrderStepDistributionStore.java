package my.get.getretail.ui.order.step.distribution;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import my.get.getretail.R;
import my.get.getretail.model.pojo.Shop;
import my.get.getretail.network.NetworkFragmentRequest;
import my.get.getretail.network.request.RequestStore;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 09/11/15.
 */
public class FragmentOrderStepDistributionStore extends AFragmentOrderStepDistribution {

	public static final String							TAG = "ORDER_STEP_DISTRIBUTION_STORE";

	private Shop										mShops;
	private int											mIdStore;
	private int											mIdShop;

	private NetworkRequestStore							mNetworkRequestStore;

	public static FragmentOrderStepDistributionStore	newInstance(Bundle args) {
		FragmentOrderStepDistributionStore fragment = new FragmentOrderStepDistributionStore();
		fragment.mIdLayout = R.layout.fragment_order_step_distribution;
		fragment.mIdTitle = R.string.title_delivery;
		fragment.mTypeOrderDelivery = args.getInt(TYPE_ORDER_DELIVERY);
		fragment.mIdStore = args.getInt(ID_STORE);
		fragment.mIdShop = args.getInt(ID_SHOP);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		fetchShops();
		return view;
	}

	@Override
	public void			onDestroy() {
		super.onDestroy();
	}

	@Override
	public void			findViewsById(View view) {

	}

	@Override
	public void			factories() {
		mNetworkRequestStore = new NetworkRequestStore(this);
	}

	@Override
	public void			updateUI(Context context) {

	}

	@Override
	public void			callbackUpdateUI() {

	}

	private void		fetchShops() {
		RequestStore.getShops(mActivity.mModuleApi.getService(), mIdStore).enqueue(mNetworkRequestStore);
	}

	static class NetworkRequestStore extends NetworkFragmentRequest<List<Shop>> {

		NetworkRequestStore(FragmentOrderStepDistributionStore fragment) {
			super(fragment);
		}

		@Override
		public void		onResponse(Response<List<Shop>> response, Retrofit retrofit) {
			if (response.errorBody() != null) {
				System.out.println("Network problem");
			} else {
				FragmentOrderStepDistributionStore fragment = (FragmentOrderStepDistributionStore) mFragment.get();
				if (fragment != null) {
					fragment.mShops = response.body().get(fragment.mIdShop);
					fragment.callbackUpdateUI();
				}
			}
		}
	}
}
