package my.get.getretail.ui.base.viewholder.home;

import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.TextView;

import my.get.getretail.R;
import my.get.getretail.ui.base.factory.FactoryFragment;
import my.get.getretail.ui.base.getcorp.AGetCorpActivity;
import my.get.getretail.ui.base.getcorp.AGetCorpViewHolder;
import my.get.getretail.ui.home.ActivityHome;
import my.get.getretail.ui.home.AdapterDrawer;

/**
 * Created by qlitzler on 18/11/15.
 */
public class VHDrawer extends AGetCorpViewHolder {

	private TextView		mItemTitle;

	public VHDrawer(AGetCorpActivity activity, View viewItem, int viewType) {
		super(activity, viewItem, viewType);
	}


	@Override
	public void				findViewsById(View view, int viewType) {
		mItemTitle = (TextView) view.findViewById(R.id.item_title);
	}

	@Override
	public void				setFields(Object object, int position) {
		int			itemTitle;

		switch (position) {
			case 0:
				itemTitle = R.string.item_title_header;
				break;
			case AdapterDrawer.STORES:
				itemTitle = R.string.item_title_stores;
				break;
			case AdapterDrawer.ADDRESSES:
				itemTitle = R.string.item_title_addresses;
				break;
			case AdapterDrawer.CARDS:
				itemTitle = R.string.item_title_cards;
				break;
			case AdapterDrawer.HISTORY:
				itemTitle = R.string.item_title_history;
				break;
			case AdapterDrawer.PROFILE:
				itemTitle = R.string.item_title_profile;
				break;
			case AdapterDrawer.CART:
				itemTitle = R.string.item_title_cart;
				break;
			case AdapterDrawer.CONCEPT:
				itemTitle = R.string.item_title_concept;
				break;
			case AdapterDrawer.SUPPORT:
				itemTitle = R.string.item_title_support;
				break;
			default:
				itemTitle = -1;
				break;
		}
		setItemTitle(mActivity.getBaseContext().getString(itemTitle));
	}

	@Override
	public void						onClick(View v) {
		int					adapterPosition;
		int					typeFragment;
		FragmentManager fragmentManager = mActivity.getSupportFragmentManager();

		adapterPosition = getAdapterPosition();
		typeFragment = getFragment(adapterPosition);
		ActivityHome activity = (ActivityHome)mActivity;
		activity.mSelectedFragmentType = typeFragment;
		if (typeFragment != FactoryFragment.ERROR) {
			fragmentManager.beginTransaction()
					.replace(
							R.id.fragment_holder,
							FactoryFragment.newInstance(typeFragment, null),
							FactoryFragment.getTag(typeFragment)
					)
					.commit();
		}
		mActivity.mDrawerLayout.closeDrawers();
	}

	public static int					getFragment(int position) {
		int				typeFragment;

		switch (position) {
			case AdapterDrawer.STORES:
				typeFragment = FactoryFragment.STORES;
				break;
			case AdapterDrawer.ADDRESSES:
				typeFragment = FactoryFragment.ADDRESSES;
				break;
			case AdapterDrawer.CARDS:
				typeFragment = FactoryFragment.CARDS;
				break;
			case AdapterDrawer.HISTORY:
				typeFragment = FactoryFragment.HISTORY;
				break;
			case AdapterDrawer.PROFILE:
				typeFragment = FactoryFragment.PROFILE;
				break;
			case AdapterDrawer.CART:
				typeFragment = FactoryFragment.CART;
				break;
			case AdapterDrawer.CONCEPT:
				typeFragment = FactoryFragment.CONCEPT;
				break;
			case AdapterDrawer.SUPPORT:
				typeFragment = FactoryFragment.SUPPORT;
				break;
			default:
				typeFragment = FactoryFragment.ERROR;
				break;
		}
		return typeFragment;
	}

	public void				setItemTitle(CharSequence itemTitle) {
		mItemTitle.setText(itemTitle);
	}
}
