package my.get.getretail.ui.base.viewholder.address;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import my.get.getretail.ui.order.ActivityOrder;
import my.get.getretail.ui.order.step.address.FragmentOrderStepAddressCustomer;

/**
 * Created by qlitzler on 17/11/15.
 */
public class VHAddressCustomerForResult extends AVHAddress {

	public VHAddressCustomerForResult(FragmentOrderStepAddressCustomer fragment, View viewItem, int viewType) {
		super(fragment, viewItem, viewType);
	}

	@Override
	public void						onClick(View v) {
		Intent data = new Intent();
		data.putExtra(ActivityOrder.LABEL_ADDRESS_CUSTOMER, mLabelAddress.getText());
		mFragment.getActivity().setResult(Activity.RESULT_OK, data);
		mFragment.getActivity().finish();
	}
}
