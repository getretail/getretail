package my.get.getretail.ui.concept;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import my.get.getretail.R;
import my.get.getretail.ui.base.factory.FactoryFragment;
import my.get.getretail.ui.base.getcorp.AGetCorpFragment;
import my.get.getretail.ui.home.ActivityHome;

/**
 * Created by qlitzler on 03/11/15.
 */
public class FragmentCGV extends AGetCorpFragment {

	public static final String		TAG = "CGV";

	public static FragmentCGV		newInstance() {
		FragmentCGV		fragment = new FragmentCGV();
		fragment.mIdLayout = R.layout.fragment_cgv;
		fragment.mIdTitle = R.string.title_cgv;
		return fragment;
	}

	@Override
	public void		onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActivityHome	activity = (ActivityHome) mActivity;
		activity.mSelectedFragmentType = FactoryFragment.CGV;
		setHasOptionsMenu(true);
	}

	@Override
	public void		onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_cgv, menu);
	}

	@Override
	public boolean	onOptionsItemSelected(MenuItem item) {
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		int id = item.getItemId();
		if (id == R.id.action_concept) {
			transaction
					.replace(
							R.id.fragment_holder,
							FactoryFragment.newInstance(FactoryFragment.CONCEPT, null),
							FactoryFragment.getTag(FactoryFragment.CONCEPT)
					)
					.commit();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void		onDestroy() {
		super.onDestroy();
	}

	@Override
	public void		findViewsById(View view) {

	}

	@Override
	public void		factories() {

	}

	@Override
	public void		updateUI(Context context) {

	}

	@Override
	public void		callbackUpdateUI() {

	}
}
