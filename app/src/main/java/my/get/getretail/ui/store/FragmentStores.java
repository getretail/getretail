package my.get.getretail.ui.store;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.List;

import my.get.getretail.R;
import my.get.getretail.model.pojo.ConfigGetCorp;
import my.get.getretail.model.pojo.Store;
import my.get.getretail.module.ModuleShared;
import my.get.getretail.network.NetworkFragmentRequest;
import my.get.getretail.network.request.RequestConfig;
import my.get.getretail.ui.base.NavigationDispatcher;
import my.get.getretail.ui.base.getcorp.AGetCorpFragment;
import retrofit.Response;
import retrofit.Retrofit;

public class FragmentStores extends AGetCorpFragment {

	public static final String				TAG = "CATALOG";

	private NetworkRequestContentConfig		mNetworkRequestContentConfig;
	private View.OnClickListener			mFakeOnClickListener;
	private TextView						mFakeButton;
	private List<Store>						mStores;

	public static FragmentStores			newInstance() {
		FragmentStores fragment = new FragmentStores();
		fragment.mIdLayout = R.layout.fragment_stores;
		fragment.mIdTitle = R.string.title_stores;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		fetchConfiguration();
		return view;
	}

	@Override
	public void				onDestroy() {
		super.onDestroy();
		mNetworkRequestContentConfig = null;
	}

	@Override
	public void				findViewsById(View view) {
		mFakeButton = (TextView) view.findViewById(android.R.id.text1);
	}

	@Override
	public void				factories() {
		mNetworkRequestContentConfig = new NetworkRequestContentConfig(this, mActivity.mModuleShared);
	}

	@Override
	public void				updateUI(Context context) {

	}

	@Override
	public void 			callbackUpdateUI() {
		mFakeOnClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				NavigationDispatcher.activityCategories(getActivity(), mStores.get(0).getId());
			}
		};
		mFakeButton.setOnClickListener(mFakeOnClickListener);
	}

	private void				fetchConfiguration() {
		RequestConfig.getConfigGetCorp(mActivity.mModuleApi.getService()).enqueue(mNetworkRequestContentConfig);
	}

	static class NetworkRequestContentConfig extends NetworkFragmentRequest<ConfigGetCorp> {

		final private WeakReference<ModuleShared>		mModuleShared;

		NetworkRequestContentConfig(AGetCorpFragment fragment, ModuleShared moduleShared) {
			super(fragment);
			mModuleShared = new WeakReference<>(moduleShared);
		}

		@Override
		public void			onResponse(Response<ConfigGetCorp> response, Retrofit retrofit) {
			if (response.errorBody() != null) {
				System.out.println("Network problem");
			} else {
				ConfigGetCorp		config = response.body();
				FragmentStores		fragment = (FragmentStores) mFragment.get();
				if (fragment != null) {
					fragment.mStores = config.getStores();
					mModuleShared.get().writeStoreTheme(fragment.mStores);
					fragment.callbackUpdateUI();
				}
			}
		}
	}
}
