package my.get.getretail.ui.history;

import java.util.List;

import my.get.getretail.model.pojo.GroupOrderHistory;
import my.get.getretail.network.NetworkFragmentRequest;
import my.get.getretail.ui.base.getcorp.AGetCorpFragment;
import my.get.getretail.ui.base.getcorp.AGetCorpFragmentList;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 06/11/15.
 */
public abstract class AFragmentHistory extends AGetCorpFragmentList<GroupOrderHistory> {

	protected NetworkRequestOrderHistory	mNetworkRequestOrderHistory;

	@Override
	public void			onDestroy() {
		super.onDestroy();
		mNetworkRequestOrderHistory = null;
	}

	@Override
	public void			factories() {
		super.factories();
		mNetworkRequestOrderHistory = new NetworkRequestOrderHistory(this);
	}

	@Override
	public void			callbackUpdateUI() {
		super.callbackUpdateUI();
	}

	static class 		NetworkRequestOrderHistory extends NetworkFragmentRequest<List<GroupOrderHistory>> {

		NetworkRequestOrderHistory(AGetCorpFragment fragment) {
			super(fragment);
		}

		@Override
		public void		onResponse(Response<List<GroupOrderHistory>> response, Retrofit retrofit) {
			if (response.errorBody() != null) {
				System.out.println("Network problem");
			} else {
				AFragmentHistory fragment = (AFragmentHistory) mFragment.get();
				if (fragment != null) {
					fragment.mList = response.body();
					fragment.callbackUpdateUI();
				}
			}
		}
	}

}
