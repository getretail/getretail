package my.get.getretail.ui.support;

import android.content.Context;
import android.view.View;

import my.get.getretail.R;
import my.get.getretail.ui.base.getcorp.AGetCorpFragment;

/**
 * Created by qlitzler on 29/10/15.
 */
public class FragmentSupport extends AGetCorpFragment {

	public static final String		TAG = "SUPPORT";

	public static FragmentSupport	newInstance() {
		FragmentSupport		fragment = new FragmentSupport();
		fragment.mIdLayout = R.layout.fragment_support;
		fragment.mIdTitle = R.string.title_support;
		return fragment;
	}

	@Override
	public void		onDestroy() {
		super.onDestroy();
	}

	@Override
	public void		findViewsById(View view) {

	}

	@Override
	public void		factories() {

	}

	@Override
	public void		updateUI(Context context) {

	}

	@Override
	public void		callbackUpdateUI() {

	}
}
