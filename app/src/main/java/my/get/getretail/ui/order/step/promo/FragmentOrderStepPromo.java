package my.get.getretail.ui.order.step.promo;

import android.content.Context;
import android.view.View;

import my.get.getretail.R;
import my.get.getretail.ui.order.step.AFragmentOrderStep;

/**
 * Created by qlitzler on 01/11/15.
 */
public class FragmentOrderStepPromo extends AFragmentOrderStep {

	public static final String				TAG = "ORDER_STEP_PROMO";

	public static FragmentOrderStepPromo	newInstance() {
		FragmentOrderStepPromo fragment = new FragmentOrderStepPromo();
		fragment.mIdLayout = R.layout.fragment_order_step_promo;
		fragment.mIdTitle = R.string.title_promo;
		return fragment;
	}

	@Override
	public void		onDestroy() {
		super.onDestroy();
	}

	@Override
	public void		findViewsById(View view) {

	}

	@Override
	public void		factories() {

	}

	@Override
	public void		updateUI(Context context) {

	}

	@Override
	public void		callbackUpdateUI() {

	}
}
