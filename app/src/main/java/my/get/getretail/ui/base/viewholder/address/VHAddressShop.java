package my.get.getretail.ui.base.viewholder.address;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import my.get.getretail.R;
import my.get.getretail.model.pojo.Shop;
import my.get.getretail.ui.base.getcorp.AGetCorpViewHolder;
import my.get.getretail.ui.order.ActivityOrder;
import my.get.getretail.ui.order.step.address.FragmentOrderStepAddressShop;

/**
 * Created by qlitzler on 17/11/15.
 */
public class VHAddressShop extends AGetCorpViewHolder<Shop> {

	private TextView		mLabelShop;

	public VHAddressShop(FragmentOrderStepAddressShop fragment, View viewItem, int viewType) {
		super(fragment, viewItem, viewType);
	}

	@Override
	public void				findViewsById(View view, int viewType) {
		mLabelShop = (TextView) view.findViewById(R.id.label_address);
	}

	@Override
	public void				setFields(Shop shop, int position) {
		setLabelShop(shop.getLabel());
	}

	@Override
	public void				onClick(View v) {
		Intent data = new Intent();
		data.putExtra(ActivityOrder.LABEL_ADDRESS_SHOP, mLabelShop.getText());
		mFragment.getActivity().setResult(Activity.RESULT_OK, data);
		mFragment.getActivity().finish();
	}

	public void				setLabelShop(CharSequence labelShop) {
		mLabelShop.setText(labelShop);
	}
}