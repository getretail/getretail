package my.get.getretail.ui.base.getcorp;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by qlitzler on 10/11/15.
 */
public abstract class AGetCorpViewHolder<T> extends RecyclerView.ViewHolder implements View.OnClickListener {

	protected AGetCorpFragment	mFragment;
	protected AGetCorpActivity	mActivity;

	public AGetCorpViewHolder(View viewItem, int viewType) {
		super(viewItem);
		findViewsById(viewItem, viewType);
		viewItem.setOnClickListener(this);
	}

	public AGetCorpViewHolder(AGetCorpFragment fragment, View viewItem, int viewType) {
		this(viewItem, viewType);
		mFragment = fragment;
	}

	public AGetCorpViewHolder(AGetCorpActivity activity, View viewItem, int viewType) {
		this(viewItem, viewType);
		mActivity = activity;
	}

	protected abstract void		findViewsById(View view, int viewType);

	public abstract void		setFields(T object, int position);
}
