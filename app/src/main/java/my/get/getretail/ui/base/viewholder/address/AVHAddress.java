package my.get.getretail.ui.base.viewholder.address;

import android.view.View;
import android.widget.TextView;

import my.get.getretail.R;
import my.get.getretail.model.pojo.Address;
import my.get.getretail.ui.base.getcorp.AGetCorpFragment;
import my.get.getretail.ui.base.getcorp.AGetCorpViewHolder;

/**
 * Created by qlitzler on 17/11/15.
 */
public abstract class AVHAddress extends AGetCorpViewHolder<Address> {

	protected TextView		mLabelAddress;

	public AVHAddress(AGetCorpFragment fragment, View viewItem, int viewType) {
		super(fragment, viewItem, viewType);
	}

	@Override
	public void				findViewsById(View view, int viewType) {
		mLabelAddress = (TextView) view.findViewById(R.id.label_address);
	}

	@Override
	public void				setFields(Address address, int position) {
		setLabelAddress(address.getLabel());
	}

	public void				setLabelAddress(CharSequence labelAddress) {
		mLabelAddress.setText(labelAddress);
	}
}
