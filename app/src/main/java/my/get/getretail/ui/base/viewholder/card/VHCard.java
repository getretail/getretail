package my.get.getretail.ui.base.viewholder.card;

import android.view.View;

import my.get.getretail.ui.card.FragmentCards;

/**
 * Created by qlitzler on 17/11/15.
 */
public class VHCard extends AVHCard {

	public VHCard(FragmentCards fragment, View viewItem, int viewType) {
		super(fragment, viewItem, viewType);
	}

	@Override
	public void				onClick(View view) {
		super.onClick(view);
	}

}
