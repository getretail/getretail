package my.get.getretail.ui.base.viewholder.card;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import my.get.getretail.ui.order.ActivityOrder;
import my.get.getretail.ui.order.step.card.FragmentOrderStepCard;

/**
 * Created by qlitzler on 17/11/15.
 */
public class VHCardForResult extends AVHCard {

	public VHCardForResult(FragmentOrderStepCard fragment, View viewItem, int viewType) {
		super(fragment, viewItem, viewType);
	}

	@Override
	public void				onClick(View view) {
		super.onClick(view);
		Intent data = new Intent();
		data.putExtra(ActivityOrder.LABEL_CARD, mLabelCard.getText());
		mFragment.getActivity().setResult(Activity.RESULT_OK, data);
		mFragment.getActivity().finish();
	}

}
