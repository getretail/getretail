package my.get.getretail.ui.category;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;

import java.lang.ref.WeakReference;
import java.util.List;

import my.get.getretail.R;
import my.get.getretail.model.pojo.ConfigGetCorp;
import my.get.getretail.model.pojo.Store;
import my.get.getretail.network.NetworkActivityRequest;
import my.get.getretail.network.request.RequestConfig;
import my.get.getretail.ui.base.factory.FactoryAdapterPager;
import my.get.getretail.ui.base.getcorp.AGetCorpActivity;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 04/11/15.
 */
public class ActivityCategories extends AGetCorpActivity {
	
	public static final String				ID_STORE = "id_stores";

	private List<Store>						mStores;

	private TabLayout						mTabLayout;
	private AdapterCategoryPager			mAdapterCategoryPager;
	private ViewPager						mViewPager;

	private ListenerSelectedTab				mListenerSelectedTab;
	private ViewPager.OnPageChangeListener	mListenerOnPageChange;
	private NetworkRequestCategories		mNetworkRequestCategories;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		fetchConfiguration();
	}

	@Override
	protected int 			getLayoutId() {
		return R.layout.activity_categories;
	}

	@Override
	protected void			onDestroy() {
		super.onDestroy();
		mNetworkRequestCategories = null;
		mListenerOnPageChange = null;
		mListenerSelectedTab = null;
	}

	@Override
	public boolean			onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void				findViewsById(View view) {
		mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
		mViewPager = (ViewPager) findViewById(R.id.pager);
	}

	@Override
	public void				factories() {
		mNetworkRequestCategories = new NetworkRequestCategories(this);
		mListenerOnPageChange = new TabLayout.TabLayoutOnPageChangeListener(mTabLayout);
		mListenerSelectedTab = new ListenerSelectedTab(this);
		mAdapterCategoryPager = (AdapterCategoryPager) FactoryAdapterPager.newInstance(
				FactoryAdapterPager.CATEGORY,
				getSupportFragmentManager(),
				null
		);
	}

	@Override
	public void				updateUI(Context context) {

	}

	public void				callbackFactories() {
	}

	@Override
	public void				callbackUpdateUI() {
		mAdapterCategoryPager.setObject(mStores.get(0));
		mViewPager.setAdapter(mAdapterCategoryPager);
		mAdapterCategoryPager.addTabs(getBaseContext(), mTabLayout);
		mViewPager.addOnPageChangeListener(mListenerOnPageChange);
		mTabLayout.setOnTabSelectedListener(mListenerSelectedTab);
	}

	private void			fetchConfiguration() {
		RequestConfig.getConfigGetCorp(mModuleApi.getService()).enqueue(mNetworkRequestCategories);
	}

	static class NetworkRequestCategories extends NetworkActivityRequest<ConfigGetCorp> {

		NetworkRequestCategories(ActivityCategories activity) {
			super(activity);
		}

		@Override
		public void			onResponse(Response<ConfigGetCorp> response, Retrofit retrofit) {
			if (response.errorBody() != null) {
				System.out.println("Network problem");
			} else {
				ConfigGetCorp		config = response.body();
				ActivityCategories	activity = (ActivityCategories) mActivity.get();
				if (activity != null) {
					activity.mStores = config.getStores();
					activity.callbackFactories();
					activity.callbackUpdateUI();
				}
			}
		}
	}

	static class ListenerSelectedTab implements TabLayout.OnTabSelectedListener {

		private WeakReference<ActivityCategories> mActivity;

		ListenerSelectedTab(ActivityCategories activity) {
			mActivity = new WeakReference<>(activity);
		}

		@Override
		public void			onTabSelected(TabLayout.Tab tab) {
			mActivity.get().mViewPager.setCurrentItem(tab.getPosition());
		}

		@Override
		public void			onTabUnselected(TabLayout.Tab tab) {

		}

		@Override
		public void			onTabReselected(TabLayout.Tab tab) {

		}
	}
}
