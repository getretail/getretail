package my.get.getretail.ui.base.getcorp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;

import my.get.getretail.R;
import my.get.getretail.module.ModuleApi;
import my.get.getretail.module.ModuleCustomer;
import my.get.getretail.module.ModuleShared;

/**
 * Created by qlitzler on 30/10/15.
 */

public abstract class AGetCorpActivity extends AppCompatActivity implements IGetCorpMethods {

	public Context				mContext;
	public ModuleApi			mModuleApi;
	public ModuleShared			mModuleShared;
	public ModuleCustomer		mModuleCustomer;
	public DrawerLayout			mDrawerLayout;

	protected Toolbar			mToolbar;
	protected ActionBar			mActionBar;
	protected FragmentManager	mFragmentManager;

	@Override
	protected void				onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_abstract);
		RelativeLayout viewRoot = (RelativeLayout) findViewById(R.id.layout_abstract);
		View view = getLayoutInflater().inflate(getLayoutId(), viewRoot, true);
		setupToolBar();

		mContext = getApplicationContext();
		mModuleApi = getModuleApi(mContext);
		mModuleCustomer = getModuleCustomer(mContext);
		mModuleShared = getModuleSharedPreference(mContext);
		mFragmentManager = getSupportFragmentManager();

		findViewsById(view);
		factories();
		updateUI(getBaseContext());
	}

	protected abstract int			getLayoutId();

	private void				setupToolBar() {
		mToolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
		mActionBar = getSupportActionBar();
		if (mActionBar != null) {
			mActionBar.setDisplayHomeAsUpEnabled(true);
			mActionBar.setHomeButtonEnabled(true);
		}
	}

	private Object				getService(Context context, String name) {
		return context.getSystemService(name);
	}

	protected ModuleApi			getModuleApi(Context context) {
		return (ModuleApi) getService(context, ModuleApi.class.getName());
	}

	protected ModuleShared		getModuleSharedPreference(Context context) {
		return (ModuleShared) getService(context, ModuleShared.class.getName());
	}

	protected ModuleCustomer	getModuleCustomer(Context context) {
		return (ModuleCustomer) getService(context, ModuleCustomer.class.getName());
	}
}
