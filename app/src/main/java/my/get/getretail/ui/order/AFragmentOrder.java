package my.get.getretail.ui.order;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import my.get.getretail.model.pojo.OrderStep;
import my.get.getretail.ui.base.NavigationDispatcher;
import my.get.getretail.ui.base.factory.FactoryOrderStep;
import my.get.getretail.ui.base.getcorp.AGetCorpFragment;

/**
 * Created by qlitzler on 01/11/15.
 */
public abstract class AFragmentOrder extends AGetCorpFragment implements AdapterView.OnItemClickListener{

	protected static final String	TYPE_ORDER_DISTRIBUTION = "type_order_distribution";

	protected int					mTypeOrderDistribution;
	protected List<OrderStep>		mOrderSteps;

	protected GridView				mGridView;
	protected AdapterGridOrderSteps	mAdapterGridOrderSteps;

	protected String				mLabelAddressCustomer;
	protected String				mLabelAddressShop;
	protected String				mLabelCard;

	@Override
	public void					onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String		tmp;
		if (savedInstanceState != null) {
			tmp = savedInstanceState.getString(ActivityOrder.LABEL_ADDRESS_CUSTOMER);
			if (tmp != null) {
				mLabelAddressCustomer = tmp;
			}
			tmp = savedInstanceState.getString(ActivityOrder.LABEL_ADDRESS_SHOP);
			if (tmp != null) {
				mLabelAddressShop = tmp;
			}
			tmp = savedInstanceState.getString(ActivityOrder.LABEL_CARD);
			if (tmp != null) {
				mLabelCard = tmp;
			}
		}
	}

	@Override
	public void					onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(ActivityOrder.LABEL_ADDRESS_CUSTOMER, mLabelAddressCustomer);
		outState.putString(ActivityOrder.LABEL_ADDRESS_SHOP, mLabelAddressShop);
		outState.putString(ActivityOrder.LABEL_CARD, mLabelCard);
	}

	@Override
	public void			onItemClick(AdapterView<?> adapter, View view, int position, long id) {
		int typeOrderStep = mAdapterGridOrderSteps.getTypeOrderStep(position);
		int typeOrderDelivery = mAdapterGridOrderSteps.getTypeOrderDelivery(position);
		NavigationDispatcher.activityForResultOrderStep(getActivity(), typeOrderStep, typeOrderDelivery);
	}

	@Override
	public void			factories() {
		mOrderSteps = new ArrayList<>();
		OrderStep stepCard = FactoryOrderStep.newInstance(FactoryOrderStep.CARD, mTypeOrderDistribution);
		if (mLabelCard != null) {
			stepCard.setLabel(mLabelCard);
		}
		mOrderSteps.add(stepCard);
		mOrderSteps.add(FactoryOrderStep.newInstance(FactoryOrderStep.MESSAGE, mTypeOrderDistribution));
		mAdapterGridOrderSteps = new AdapterGridOrderSteps(getContext(), mOrderSteps);
	}

	@Override
	public void			updateUI(Context context) {
		mGridView.setAdapter(mAdapterGridOrderSteps);
		mGridView.setOnItemClickListener(this);
	}
}
