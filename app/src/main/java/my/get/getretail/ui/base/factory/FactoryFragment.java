package my.get.getretail.ui.base.factory;

import android.os.Bundle;

import my.get.getretail.ui.address.FragmentAddresses;
import my.get.getretail.ui.base.getcorp.AGetCorpFragment;
import my.get.getretail.ui.card.FragmentCards;
import my.get.getretail.ui.cart.FragmentCart;
import my.get.getretail.ui.concept.FragmentCGV;
import my.get.getretail.ui.concept.FragmentConcept;
import my.get.getretail.ui.history.FragmentHistory;
import my.get.getretail.ui.history.FragmentHistoryCurrent;
import my.get.getretail.ui.history.FragmentHistoryPast;
import my.get.getretail.ui.order.FragmentOrderDelivery;
import my.get.getretail.ui.order.FragmentOrderEatIn;
import my.get.getretail.ui.order.FragmentOrderTakeaway;
import my.get.getretail.ui.order.step.address.FragmentOrderStepAddressCustomer;
import my.get.getretail.ui.order.step.address.FragmentOrderStepAddressShop;
import my.get.getretail.ui.order.step.card.FragmentOrderStepCard;
import my.get.getretail.ui.order.step.distribution.FragmentOrderStepDistributionDelivery;
import my.get.getretail.ui.order.step.distribution.FragmentOrderStepDistributionStore;
import my.get.getretail.ui.order.step.message.FragmentOrderStepMessage;
import my.get.getretail.ui.order.step.promo.FragmentOrderStepPromo;
import my.get.getretail.ui.product.FragmentProducts;
import my.get.getretail.ui.profile.FragmentProfile;
import my.get.getretail.ui.store.FragmentStores;
import my.get.getretail.ui.support.FragmentSupport;

/**
 * Created by qlitzler on 04/11/15.
 */
public class FactoryFragment {

	public static final int			ERROR = -1;
	public static final int			ADDRESSES = 0;
	public static final int			CARDS = 1;
	public static final int			CART = 2;
	public static final int			CGV = 3;
	public static final int			CONCEPT = 4;
	public static final int			HISTORY = 5;
	public static final int			HISTORY_CURRENT = 6;
	public static final int			HISTORY_PAST = 7;
	public static final int			ORDER_DELIVERY = 8;
	public static final int			ORDER_EAT_IN = 9;
	public static final int			ORDER_TAKEAWAY = 10;
	public static final int			ORDER_STEP_ADDRESS_CUSTOMER = 11;
	public static final int			ORDER_STEP_ADDRESS_SHOPS = 12;
	public static final int			ORDER_STEP_CARD = 13;
	public static final int			ORDER_STEP_DISTRIBUTION_DELIVERY = 14;
	public static final int			ORDER_STEP_DISTRIBUTION_STORE = 15;
	public static final int			ORDER_STEP_MESSAGE = 16;
	public static final int			ORDER_STEP_PROMO = 17;
	public static final int			PRODUCTS = 18;
	public static final int			PROFILE = 19;
	public static final int			STORES = 20;
	public static final int			SUPPORT = 21;

	public static AGetCorpFragment	newInstance(int typeFragment, Bundle args) {
		AGetCorpFragment			fragment;

		switch (typeFragment) {
			case ADDRESSES:
				fragment = FragmentAddresses.newInstance();
				break;
			case CARDS:
				fragment = FragmentCards.newInstance();
				break;
			case CART:
				fragment = FragmentCart.newInstance();
				break;
			case CGV:
				fragment = FragmentCGV.newInstance();
				break;
			case CONCEPT:
				fragment = FragmentConcept.newInstance();
				break;
			case HISTORY:
				fragment = FragmentHistory.newInstance();
				break;
			case HISTORY_CURRENT:
				fragment = FragmentHistoryCurrent.newInstance();
				break;
			case HISTORY_PAST:
				fragment = FragmentHistoryPast.newInstance();
				break;
			case ORDER_DELIVERY:
				fragment = FragmentOrderDelivery.newInstance(args);
				break;
			case ORDER_EAT_IN:
				fragment = FragmentOrderEatIn.newInstance(args);
				break;
			case ORDER_TAKEAWAY:
				fragment = FragmentOrderTakeaway.newInstance(args);
				break;
			case ORDER_STEP_ADDRESS_CUSTOMER:
				fragment = FragmentOrderStepAddressCustomer.newInstance();
				break;
			case ORDER_STEP_ADDRESS_SHOPS:
				fragment = FragmentOrderStepAddressShop.newInstance(args);
				break;
			case ORDER_STEP_CARD:
				fragment = FragmentOrderStepCard.newInstance();
				break;
			case ORDER_STEP_DISTRIBUTION_DELIVERY:
				fragment = FragmentOrderStepDistributionDelivery.newInstance(args);
				break;
			case ORDER_STEP_DISTRIBUTION_STORE:
				fragment = FragmentOrderStepDistributionStore.newInstance(args);
				break;
			case ORDER_STEP_MESSAGE:
				fragment = FragmentOrderStepMessage.newInstance();
				break;
			case ORDER_STEP_PROMO:
				fragment = FragmentOrderStepPromo.newInstance();
				break;
			case PRODUCTS:
				fragment = FragmentProducts.newInstance(args);
				break;
			case PROFILE:
				fragment = FragmentProfile.newInstance();
				break;
			case STORES:
				fragment = FragmentStores.newInstance();
				break;
			case SUPPORT:
				fragment = FragmentSupport.newInstance();
				break;
			default:
				fragment = null;
				break;
		}
		return fragment;
	}

	public static String				getTag(int typeFragment) {
		String							tag;

		switch (typeFragment) {
			case ADDRESSES:
				tag = FragmentAddresses.TAG;
				break;
			case CARDS:
				tag = FragmentCards.TAG;
				break;
			case CART:
				tag = FragmentCart.TAG;
				break;
			case CGV:
				tag = FragmentCGV.TAG;
				break;
			case CONCEPT:
				tag = FragmentConcept.TAG;
				break;
			case HISTORY:
				tag = FragmentHistory.TAG;
				break;
			case HISTORY_CURRENT:
				tag = FragmentHistoryCurrent.TAG;
				break;
			case HISTORY_PAST:
				tag = FragmentHistoryPast.TAG;
				break;
			case ORDER_DELIVERY:
				tag = FragmentOrderDelivery.TAG;
				break;
			case ORDER_EAT_IN:
				tag = FragmentOrderEatIn.TAG;
				break;
			case ORDER_TAKEAWAY:
				tag = FragmentOrderTakeaway.TAG;
				break;
			case ORDER_STEP_ADDRESS_CUSTOMER:
				tag = FragmentOrderStepAddressCustomer.TAG;
				break;
			case ORDER_STEP_ADDRESS_SHOPS:
				tag = FragmentOrderStepAddressShop.TAG;
				break;
			case ORDER_STEP_CARD:
				tag = FragmentOrderStepCard.TAG;
				break;
			case ORDER_STEP_DISTRIBUTION_DELIVERY:
				tag = FragmentOrderStepDistributionDelivery.TAG;
				break;
			case ORDER_STEP_DISTRIBUTION_STORE:
				tag = FragmentOrderStepDistributionStore.TAG;
				break;
			case ORDER_STEP_MESSAGE:
				tag = FragmentOrderStepMessage.TAG;
				break;
			case ORDER_STEP_PROMO:
				tag = FragmentOrderStepPromo.TAG;
				break;
			case PRODUCTS:
				tag = FragmentProducts.TAG;
				break;
			case PROFILE:
				tag = FragmentProfile.TAG;
				break;
			case STORES:
				tag = FragmentStores.TAG;
				break;
			case SUPPORT:
				tag = FragmentSupport.TAG;
				break;
			default:
				tag = null;
				break;
		}
		return tag;
	}
}
