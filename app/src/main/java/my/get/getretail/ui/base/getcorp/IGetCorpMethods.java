package my.get.getretail.ui.base.getcorp;

import android.content.Context;
import android.view.View;

/**
 * Created by qlitzler on 05/11/15.
 */
public interface IGetCorpMethods {
	void		findViewsById(View view);
	void		updateUI(Context context);
	void		callbackUpdateUI();
	void		factories();
}
