package my.get.getretail.ui.history;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import my.get.getretail.R;
import my.get.getretail.network.request.RequestOrder;
import my.get.getretail.ui.base.factory.FactoryViewHolder;

/**
 * Created by qlitzler on 05/11/15.
 */
public class FragmentHistoryCurrent extends AFragmentHistory {

	public static final String				TAG = "HISTORY_CURRENT";

	public static FragmentHistoryCurrent	newInstance() {
		FragmentHistoryCurrent fragment = new FragmentHistoryCurrent();
		fragment.mIdLayout = R.layout.fragment_history_current;
		fragment.mIdTitle = R.string.title_history;
		fragment.mTypeViewHolder = FactoryViewHolder.HISTORY;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		fetchOrderPending();
		return view;
	}

	@Override
	public void		onDestroy() {
		super.onDestroy();
	}

	@Override
	public void		factories() {
		super.factories();
	}

	@Override
	public void		findViewsById(View view) {
		super.findViewsById(view);
	}

	@Override
	public void		updateUI(Context context) {
		super.updateUI(context);
	}

	private void	fetchOrderPending() {
		RequestOrder.getOrders(mActivity.mModuleApi.getService(), "pending").enqueue(mNetworkRequestOrderHistory);
	}
}
