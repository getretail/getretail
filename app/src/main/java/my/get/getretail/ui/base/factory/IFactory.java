package my.get.getretail.ui.base.factory;

/**
 * Created by qlitzler on 18/11/15.
 */
public interface IFactory<T> {

	T		newInstance(Object ... args);
}
