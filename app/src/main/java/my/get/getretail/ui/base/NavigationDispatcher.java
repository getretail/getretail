package my.get.getretail.ui.base;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import my.get.getretail.R;
import my.get.getretail.ui.category.ActivityCategories;
import my.get.getretail.ui.order.step.ActivityOrderStep;
import my.get.getretail.ui.product.ActivityProducts;

/**
 * Created by qlitzler on 28/10/15.
 */
public class NavigationDispatcher {

	private static Uri		getScheme(String scheme, String host) {
		return Uri.parse(scheme + "://" + host);
	}

	public static void		activityHome(Activity from) {
		final Intent nextIntent = new Intent(Intent.ACTION_VIEW, getScheme(from.getString(R.string.scheme_app), from.getString(R.string.host_home)));
		from.startActivity(nextIntent);
	}

	public static void		activityCategories(Activity from, int idStore) {
		final Intent nextIntent = new Intent(Intent.ACTION_VIEW, getScheme(from.getString(R.string.scheme_app), from.getString(R.string.host_categories)));
		Bundle		args = new Bundle(1);
		args.putInt(ActivityCategories.ID_STORE, idStore);
		nextIntent.putExtras(args);
		from.startActivity(nextIntent);
	}

	public static void		activityProducts(Activity from, int idStore, int idCategory) {
		final Intent nextIntent = new Intent(Intent.ACTION_VIEW, getScheme(from.getString(R.string.scheme_app), from.getString(R.string.host_products)));
		Bundle		args = new Bundle(1);
		args.putInt(ActivityCategories.ID_STORE, idStore);
		args.putInt(ActivityProducts.ID_CATEGORY, idCategory);
		nextIntent.putExtras(args);
		from.startActivity(nextIntent);
	}

	public static void		activityOrder(Activity from) {
		final Intent nextIntent = new Intent(Intent.ACTION_VIEW, getScheme(from.getString(R.string.scheme_app), from.getString(R.string.host_order)));
		from.startActivity(nextIntent);
	}

	public static void		activityForResultOrderStep(Activity from, int typeOrderStep, int typeOrderDistribution) {
		final Intent nextIntent = new Intent(Intent.ACTION_VIEW, getScheme(from.getString(R.string.scheme_app), from.getString(R.string.host_order_step)));
		nextIntent.putExtra(ActivityOrderStep.TYPE_ORDER_STEP, typeOrderStep);
		nextIntent.putExtra(ActivityOrderStep.TYPE_ORDER_DISTRIBUTION, typeOrderDistribution);
		from.startActivityForResult(nextIntent, typeOrderStep);
	}

}
