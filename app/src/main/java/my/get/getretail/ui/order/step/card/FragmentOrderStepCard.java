package my.get.getretail.ui.order.step.card;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import my.get.getretail.R;
import my.get.getretail.model.pojo.Customer;
import my.get.getretail.model.pojo.Card;
import my.get.getretail.network.NetworkFragmentRequest;
import my.get.getretail.ui.base.factory.FactoryViewHolder;
import my.get.getretail.ui.base.getcorp.AGetCorpFragmentList;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 01/11/15.
 */
public class FragmentOrderStepCard extends AGetCorpFragmentList<Card> {

	public static final String			TAG = "ORDER_STEP_CARD";

	private NetworkRequestLogin			mNetworkRequestLogin;

	public static FragmentOrderStepCard	newInstance() {
		FragmentOrderStepCard fragment = new FragmentOrderStepCard();
		fragment.mIdLayout = R.layout.fragment_order_step_card;
		fragment.mIdTitle = R.string.title_cards;
		fragment.mTypeViewHolder = FactoryViewHolder.CARDS_FOR_RESULT;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		fetchUserCreditCards();
		return view;
	}

	@Override
	public void		onDestroy() {
		super.onDestroy();
		mNetworkRequestLogin = null;
	}

	@Override
	public void			findViewsById(View view) {
		super.findViewsById(view);
	}

	@Override
	public void			factories() {
		super.factories();
		mNetworkRequestLogin = new NetworkRequestLogin(this);
	}

	@Override
	public void			updateUI(Context context) {
		super.updateUI(context);
	}

	@Override
	public void			callbackUpdateUI() {
		super.callbackUpdateUI();
	}

	private void		fetchUserCreditCards() {
//		RequestCustomer.login(mActivity.mModuleApi.getService()).enqueue(mNetworkRequestLogin);
	}

	static class NetworkRequestLogin extends NetworkFragmentRequest<Customer> {

		NetworkRequestLogin(FragmentOrderStepCard fragment) {
			super(fragment);
		}

		@Override
		public void		onResponse(Response<Customer> response, Retrofit retrofit) {
			if (response.errorBody() != null) {
				System.out.println("Network problem");
			} else {
				FragmentOrderStepCard fragment = (FragmentOrderStepCard) mFragment.get();
				if (fragment != null) {
					fragment.mList = response.body().getPaymentCards();
					fragment.callbackUpdateUI();
				}
			}
		}
	}
}
