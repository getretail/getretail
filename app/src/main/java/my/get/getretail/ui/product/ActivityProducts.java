package my.get.getretail.ui.product;

import android.content.Context;
import android.view.MenuItem;
import android.view.View;

import my.get.getretail.R;
import my.get.getretail.ui.base.factory.FactoryFragment;
import my.get.getretail.ui.base.getcorp.AGetCorpActivity;

/**
 * Created by qlitzler on 31/10/15.
 */
public class ActivityProducts extends AGetCorpActivity {

	public static final String		ID_CATEGORY = "id_category";

	@Override
	protected int 			getLayoutId() {
		return R.layout.activity_products;
	}

	@Override
	public boolean			onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void			onDestroy() {
		super.onDestroy();
	}

	@Override
	public void				findViewsById(View view) {

	}

	@Override
	public void				factories() {
		if (mFragmentManager.findFragmentByTag(
				FactoryFragment.getTag(FactoryFragment.PRODUCTS)) == null
				) {
			mFragmentManager.beginTransaction()
					.replace(R.id.fragment_holder,
							FactoryFragment.newInstance(
									FactoryFragment.PRODUCTS,
									getIntent().getExtras()
							),
							FactoryFragment.getTag(FactoryFragment.PRODUCTS)
					)
					.commit();
		}
	}

	@Override
	public void				updateUI(Context context) {

	}

	@Override
	public void				callbackUpdateUI() {

	}
}
