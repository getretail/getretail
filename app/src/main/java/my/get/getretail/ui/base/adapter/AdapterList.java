package my.get.getretail.ui.base.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import my.get.getretail.ui.base.factory.FactoryViewHolder;
import my.get.getretail.ui.base.getcorp.AGetCorpFragmentList;
import my.get.getretail.ui.base.getcorp.AGetCorpViewHolder;

/**
 * Created by qlitzler on 10/11/15.
 */
public class AdapterList extends RecyclerView.Adapter<AGetCorpViewHolder> {

	private int						mTypeViewHolder;

	protected AGetCorpFragmentList	mFragment;
	protected LayoutInflater		mLayoutInflater;
	protected List<?>				mList;

	public AdapterList(AGetCorpFragmentList fragment, int typeViewHolder) {
		mTypeViewHolder = typeViewHolder;
		mFragment = fragment;
		mLayoutInflater = LayoutInflater.from(mFragment.getContext());
	}

	@Override
	public void					onBindViewHolder(AGetCorpViewHolder holder, int position) {
		holder.setFields(mList.get(position), position);
	}

	@Override
	public AGetCorpViewHolder	onCreateViewHolder(ViewGroup parent, int viewType) {
		View	view;
		int		idLayout;

		idLayout = FactoryViewHolder.getIdLayout(mTypeViewHolder);
		view = viewType(parent, viewType, idLayout);
		return FactoryViewHolder.newInstance(mTypeViewHolder, mFragment, view, viewType);
	}

	protected View				viewType(ViewGroup parent, int viewType, int idLayout) {
		return mLayoutInflater.inflate(idLayout, parent, false);
	}

	@Override
	public int					getItemCount() {
		return mList != null ? mList.size() : 0;
	}

	public void					setList(List<?> list) { mList = list; }
}
