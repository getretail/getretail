package my.get.getretail.ui.history;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.lang.ref.WeakReference;

import my.get.getretail.R;
import my.get.getretail.ui.base.factory.FactoryAdapterPager;
import my.get.getretail.ui.base.getcorp.AGetCorpFragment;

/**
 * Created by qlitzler on 29/10/15.
 */
public class FragmentHistory extends AGetCorpFragment {

	public static final String				TAG = "HISTORY";

	private static final String				SAVE_SELECTED_TAB = "selected_tab";

	private int								mSelectedTab;

	private ViewPager						mViewPager;
	private TabLayout						mTabLayout;
	private AdapterHistoryPager				mAdapterHistoryPager;

	private ListenerSelectedTab				mListenerSelectedTab;
	private ViewPager.OnPageChangeListener	mListenerOnPageChange;

	public static FragmentHistory			newInstance() {
		FragmentHistory		fragment = new FragmentHistory();
		fragment.mIdLayout = R.layout.fragment_history;
		fragment.mIdTitle = R.string.title_history;
		return fragment;
	}

	@Override
	public void			onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			mSelectedTab = savedInstanceState.getInt(SAVE_SELECTED_TAB);
		} else {
			mSelectedTab = AdapterHistoryPager.TAB_CURRENT;
		}
	}

	@Override
	public void			onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(SAVE_SELECTED_TAB, mSelectedTab);
	}

	@Override
	public void			onDestroy() {
		super.onDestroy();
	}

	public void			findViewsById(View view) {
		mTabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
		mViewPager = (ViewPager) view.findViewById(R.id.pager);
	}

	@Override
	public void			factories() {
		mAdapterHistoryPager = (AdapterHistoryPager) FactoryAdapterPager.newInstance(
				FactoryAdapterPager.HISTORY,
				getFragmentManager(),
				null
		);
		mListenerSelectedTab = new ListenerSelectedTab(this);
	}

	@Override
	public void			updateUI(Context context) {
		mAdapterHistoryPager.addTabs(context, mTabLayout);
		mViewPager.setAdapter(mAdapterHistoryPager);
		mViewPager.addOnPageChangeListener(mListenerOnPageChange);
		mTabLayout.setOnTabSelectedListener(mListenerSelectedTab);
		mTabLayout.getTabAt(mSelectedTab).select();
		mListenerOnPageChange = new TabLayout.TabLayoutOnPageChangeListener(mTabLayout);
	}

	@Override
	public void			callbackUpdateUI() {

	}

	static class		ListenerSelectedTab implements TabLayout.OnTabSelectedListener {

		private WeakReference<FragmentHistory>	mFragment;

		ListenerSelectedTab(FragmentHistory fragment) {
			mFragment = new WeakReference<>(fragment);
		}

		@Override
		public void		onTabSelected(TabLayout.Tab tab) {
			FragmentHistory		fragment = mFragment.get();
			if (fragment != null) {
				fragment.mSelectedTab = tab.getPosition();
				fragment.mViewPager.setCurrentItem(fragment.mSelectedTab);
			}
		}

		@Override
		public void		onTabUnselected(TabLayout.Tab tab) {

		}

		@Override
		public void		onTabReselected(TabLayout.Tab tab) {

		}
	}
}
