package my.get.getretail.ui.base.factory;

import my.get.getretail.model.pojo.OrderStep;

/**
 * Created by qlitzler on 06/11/15.
 */
public class FactoryOrderStep {

	public static final int		CARD = 0;
	public static final int		ADDRESS_CUSTOMER = 1;
	public static final int		ADDRESS_SHOPS = 2;
	public static final int		DISTRIBUTION_DELIVERY = 3;
	public static final int		DISTRIBUTION_STORE = 4;
	public static final int		MESSAGE = 5;
	public static final int		PROMO = 6;

	public static final String	TITLE_CARD = "Payment Card";
	public static final String	TITLE_ADDRESS = "Address";
	public static final String	TITLE_DELIVERY = "Delivery";
	public static final String	TITLE_MESSAGE = "Message";
	public static final String	TITLE_PROMO = "Promo";

	public static OrderStep		newInstance(int typeOrderStep, int typeOrderDelivery) {
		OrderStep				orderStep;

		switch (typeOrderStep) {
			case CARD:
				orderStep = new OrderStep(TITLE_CARD, FactoryFragment.ORDER_STEP_CARD, typeOrderDelivery);
				break;
			case ADDRESS_CUSTOMER:
				orderStep = new OrderStep(TITLE_ADDRESS, FactoryFragment.ORDER_STEP_ADDRESS_CUSTOMER, typeOrderDelivery);
				break;
			case ADDRESS_SHOPS:
				orderStep = new OrderStep(TITLE_ADDRESS, FactoryFragment.ORDER_STEP_ADDRESS_SHOPS, typeOrderDelivery);
				break;
			case DISTRIBUTION_DELIVERY:
				orderStep = new OrderStep(TITLE_DELIVERY, FactoryFragment.ORDER_STEP_DISTRIBUTION_DELIVERY, typeOrderDelivery);
				break;
			case DISTRIBUTION_STORE:
				orderStep = new OrderStep(TITLE_DELIVERY, FactoryFragment.ORDER_STEP_DISTRIBUTION_STORE, typeOrderDelivery);
				break;
			case MESSAGE:
				orderStep = new OrderStep(TITLE_MESSAGE, FactoryFragment.ORDER_STEP_MESSAGE, typeOrderDelivery);
				break;
			case PROMO:
				orderStep = new OrderStep(TITLE_PROMO, FactoryFragment.ORDER_STEP_PROMO, typeOrderDelivery);
				break;
			default:
				orderStep = null;
				break;
		}
		return orderStep;
	}
}
