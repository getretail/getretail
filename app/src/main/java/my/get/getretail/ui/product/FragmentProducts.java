package my.get.getretail.ui.product;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import my.get.getretail.R;
import my.get.getretail.model.pojo.Product;
import my.get.getretail.network.NetworkFragmentRequest;
import my.get.getretail.network.request.RequestStore;
import my.get.getretail.ui.base.NavigationDispatcher;
import my.get.getretail.ui.base.getcorp.AGetCorpFragment;
import my.get.getretail.ui.category.AdapterCategoryPager;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 03/11/15.
 */
public class FragmentProducts extends AGetCorpFragment {

	public static final String				TAG = "PRODUCTS";

	// Data
	private int								mIdCategory;
	private int								mIdStore;

	private List<Product>					mProducts;

	// UI
	private View							mView;
	private TextView						mFakeButton;

	private NetworkRequestProducts			mNetworkRequestProducts;
	private View.OnClickListener			mFakeOnClickListener;

	public static FragmentProducts			newInstance(Bundle bundle) {
		FragmentProducts		fragment = new FragmentProducts();
		fragment.mIdLayout = R.layout.fragment_products;
		fragment.mIdTitle = R.string.title_stores;
		fragment.mIdStore = bundle.getInt(AdapterCategoryPager.ID_STORE);
		fragment.mIdCategory = bundle.getInt(AdapterCategoryPager.ID_CATEGORY);
		return fragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		mView = super.onCreateView(inflater, container, savedInstanceState);
		fetchProducts();
		return mView;
	}

	@Override
	public void			onDestroy() {
		super.onDestroy();
		mNetworkRequestProducts = null;
	}

	@Override
	public void			findViewsById(View view) {
		mFakeButton = (TextView) view.findViewById(android.R.id.text1);
	}

	@Override
	public void			factories() {

	}

	@Override
	public void			updateUI(Context context) {

	}

	@Override
	public void			callbackUpdateUI() {
		mFakeOnClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				NavigationDispatcher.activityOrder(getActivity());
			}
		};
		mFakeButton.setOnClickListener(mFakeOnClickListener);
	}

	private void		fetchProducts() {
		mNetworkRequestProducts = new NetworkRequestProducts(this);
		RequestStore.getProducts(mActivity.mModuleApi.getService(), mIdCategory).enqueue(mNetworkRequestProducts);
	}

	static class NetworkRequestProducts extends NetworkFragmentRequest<List<Product>> {

		NetworkRequestProducts(FragmentProducts fragment) {
			super(fragment);
		}

		@Override
		public void		onResponse(Response<List<Product>> response, Retrofit retrofit) {
			if (response.errorBody() != null) {
				//TODO
			} else {
				FragmentProducts fragment = (FragmentProducts) mFragment.get();
				if (fragment != null) {
					fragment.mProducts = response.body();
					fragment.callbackUpdateUI();
				}
			}
		}
	}
}
