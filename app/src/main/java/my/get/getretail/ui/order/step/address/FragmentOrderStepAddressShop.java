package my.get.getretail.ui.order.step.address;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import my.get.getretail.R;
import my.get.getretail.model.pojo.Shop;
import my.get.getretail.network.NetworkFragmentRequest;
import my.get.getretail.network.request.RequestStore;
import my.get.getretail.ui.base.factory.FactoryViewHolder;
import my.get.getretail.ui.base.getcorp.AGetCorpFragmentList;
import my.get.getretail.ui.order.step.ActivityOrderStep;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 09/11/15.
 */
public class FragmentOrderStepAddressShop extends AGetCorpFragmentList<Shop> {

	public static final String						TAG = "ORDER_STEP_ADDRESS_SHOP";

	private int										mIdStore;

	private NetworkRequestStore						mNetworkRequestStore;

	public static FragmentOrderStepAddressShop newInstance(Bundle args) {
		FragmentOrderStepAddressShop fragment = new FragmentOrderStepAddressShop();
		fragment.mIdLayout = R.layout.fragment_order_step_address_shops;
		fragment.mIdTitle = R.string.title_addresses;
		fragment.mIdStore = args.getInt(ActivityOrderStep.ID_STORE);
		fragment.mTypeViewHolder = FactoryViewHolder.ADDRESS_SHOP;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		fetchShopAddresses();
		return view;
	}

	@Override
	public void			onDestroy() {
		super.onDestroy();
		mNetworkRequestStore = null;
	}

	@Override
	public void			findViewsById(View view) {
		super.findViewsById(view);
	}

	@Override
	public void			factories() {
		super.factories();
		mNetworkRequestStore = new NetworkRequestStore(this);
	}

	@Override
	public void			updateUI(Context context) {
		super.updateUI(context);
	}

	@Override
	public void			callbackUpdateUI() {
		super.callbackUpdateUI();
	}

	public void			fetchShopAddresses() {
		RequestStore.getShops(mActivity.mModuleApi.getService(), mIdStore).enqueue(mNetworkRequestStore);
	}

	static class NetworkRequestStore extends NetworkFragmentRequest<List<Shop>> {

		NetworkRequestStore(FragmentOrderStepAddressShop fragment) {
			super(fragment);
		}

		@Override
		public void		onResponse(Response<List<Shop>> response, Retrofit retrofit) {
			if (response.errorBody() != null) {
				System.out.println("Network problem");
			} else {
				FragmentOrderStepAddressShop fragment = (FragmentOrderStepAddressShop) mFragment.get();
				if (fragment != null) {
					fragment.mList = response.body();
					fragment.callbackUpdateUI();
				}
			}
		}
	}
}
