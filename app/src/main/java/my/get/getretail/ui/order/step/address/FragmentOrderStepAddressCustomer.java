package my.get.getretail.ui.order.step.address;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import my.get.getretail.R;
import my.get.getretail.model.pojo.Address;
import my.get.getretail.model.pojo.Customer;
import my.get.getretail.network.NetworkFragmentRequest;
import my.get.getretail.ui.base.factory.FactoryViewHolder;
import my.get.getretail.ui.base.getcorp.AGetCorpFragmentList;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 09/11/15.
 */
public class FragmentOrderStepAddressCustomer extends AGetCorpFragmentList<Address> {

	public static final String						TAG = "ORDER_STEP_ADDRESS_CUSTOMER";

	private NetworkRequestLogin						mNetworkRequestLogin;

	public static FragmentOrderStepAddressCustomer	newInstance() {
		FragmentOrderStepAddressCustomer fragment = new FragmentOrderStepAddressCustomer();
		fragment.mIdLayout = R.layout.fragment_order_step_address_customer;
		fragment.mIdTitle = R.string.title_addresses;
		fragment.mTypeViewHolder = FactoryViewHolder.ADDRESS_CUSTOMER_FOR_RESULT;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		fetchUserAddresses();
		return view;
	}

	@Override
	public void			onDestroy() {
		super.onDestroy();
		mNetworkRequestLogin = null;
	}

	@Override
	public void			findViewsById(View view) {
		super.findViewsById(view);
	}

	@Override
	public void			factories() {
		super.factories();
		mNetworkRequestLogin = new NetworkRequestLogin(this);
	}

	@Override
	public void			updateUI(Context context) {
		super.updateUI(context);
	}

	@Override
	public void			callbackUpdateUI() {
		super.callbackUpdateUI();
	}

	private void		fetchUserAddresses() {
//		RequestCustomer.login(mActivity.mModuleApi.getService()).enqueue(mNetworkRequestLogin);
	}

	static class NetworkRequestLogin extends NetworkFragmentRequest<Customer> {

		NetworkRequestLogin(FragmentOrderStepAddressCustomer fragment) {
			super(fragment);
		}

		@Override
		public void		onResponse(Response<Customer> response, Retrofit retrofit) {
			if (response.errorBody() != null) {
				System.out.println("Network problem");
			} else {
				FragmentOrderStepAddressCustomer fragment = (FragmentOrderStepAddressCustomer) mFragment.get();
				if (fragment != null) {
					fragment.mList = response.body().getAddresses();
					fragment.callbackUpdateUI();
				}
			}
		}
	}
}
