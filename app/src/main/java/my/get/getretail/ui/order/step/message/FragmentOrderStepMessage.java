package my.get.getretail.ui.order.step.message;

import android.content.Context;
import android.view.View;

import my.get.getretail.R;
import my.get.getretail.ui.order.step.AFragmentOrderStep;

/**
 * Created by qlitzler on 01/11/15.
 */
public class FragmentOrderStepMessage extends AFragmentOrderStep {

	public static final String				TAG = "ORDER_STEP_MESSAGE";

	public static FragmentOrderStepMessage	newInstance() {
		FragmentOrderStepMessage fragment = new FragmentOrderStepMessage();
		fragment.mIdLayout = R.layout.fragment_order_step_message;
		fragment.mIdTitle = R.string.title_message;
		return fragment;
	}

	@Override
	public void		onDestroy() {
		super.onDestroy();
	}

	@Override
	public void		findViewsById(View view) {

	}

	@Override
	public void		factories() {

	}

	@Override
	public void		updateUI(Context context) {

	}

	@Override
	public void		callbackUpdateUI() {

	}
}
