package my.get.getretail.ui.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import my.get.getretail.R;
import my.get.getretail.model.pojo.Customer;
import my.get.getretail.network.NetworkFragmentRequest;
import my.get.getretail.ui.base.getcorp.AGetCorpFragment;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 29/10/15.
 */
public class FragmentProfile extends AGetCorpFragment {

	public static final String			TAG = "PROFILE";

	private Customer					mProfile;

	private NetworkRequestProfile		mNetworkRequestProfile;

	public static FragmentProfile		newInstance() {
		FragmentProfile		fragment = new FragmentProfile();
		fragment.mIdLayout = R.layout.fragment_profile;
		fragment.mIdTitle = R.string.title_profile;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		fetchProfile();
		return view;
	}

	@Override
	public void			onDestroy() {
		super.onDestroy();
		mNetworkRequestProfile = null;
	}

	@Override
	public void			findViewsById(View view) {

	}

	@Override
	public void			factories() {
		mNetworkRequestProfile = new NetworkRequestProfile(this);
	}

	@Override
	public void			updateUI(Context context) {

	}

	@Override
	public void			callbackUpdateUI() {

	}

	private void		fetchProfile() {
//		RequestCustomer.login(mActivity.mModuleApi.getService()).enqueue(mNetworkRequestProfile);
	}

	static class NetworkRequestProfile extends NetworkFragmentRequest<Customer> {

		NetworkRequestProfile(AGetCorpFragment fragment) {
			super(fragment);
		}

		@Override
		public void		onResponse(Response<Customer> response, Retrofit retrofit) {
			if (response.errorBody() != null) {
				System.out.println("Network problem");
			} else {
				FragmentProfile fragment = (FragmentProfile) mFragment.get();
				if (fragment != null) {
					fragment.mProfile = response.body();
					fragment.callbackUpdateUI();
				}
			}
		}
	}
}
