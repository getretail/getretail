package my.get.getretail.ui.base.getcorp;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import my.get.getretail.ui.base.adapter.AdapterList;

/**
 * Created by qlitzler on 10/11/15.
 */
public abstract class AGetCorpFragmentList<T> extends AGetCorpFragment {

	private static final String				SAVE_TYPE_VIEW_HOLDER = "save_type_view_holder";

	protected AdapterList					mAdapterList;
	protected List<T>						mList;
	protected RecyclerView					mRecyclerView;
	protected RecyclerView.LayoutManager	mLayoutManager;
	protected int							mTypeViewHolder;

	@Override
	public void			onCreate(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			mTypeViewHolder = savedInstanceState.getInt(SAVE_TYPE_VIEW_HOLDER);
		}
		super.onCreate(savedInstanceState);
	}

	@Override
	public void			onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(SAVE_TYPE_VIEW_HOLDER, mTypeViewHolder);
	}

	public void			findViewsById(View view) {
		mRecyclerView = (RecyclerView) view.findViewById(android.R.id.list);
	}

	public void			factories() {
		mLayoutManager = new LinearLayoutManager(getActivity());
		mAdapterList = new AdapterList(this, mTypeViewHolder);
	}

	public void			updateUI(Context context) {
		mRecyclerView.setLayoutManager(mLayoutManager);
		mRecyclerView.setAdapter(mAdapterList);
	}

	public void			callbackUpdateUI() {
		mAdapterList.setList(mList);
		mAdapterList.notifyDataSetChanged();
	}
}
