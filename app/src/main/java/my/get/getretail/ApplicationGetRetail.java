package my.get.getretail;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import my.get.getretail.module.ModuleApi;
import my.get.getretail.module.ModuleCustomer;
import my.get.getretail.module.ModuleShared;

/**
 * Created by qlitzler on 20/10/15.
 */
public class ApplicationGetRetail extends Application {

	private ProviderAppComponent	mProviderAppComponent;

	@Override
	public void		onCreate() {
		super.onCreate();
		Context context = this.getApplicationContext();
		mProviderAppComponent = new ProviderAppComponent(new ModuleApi(context), new ModuleShared(context), new ModuleCustomer(this));
	}

	@Override
	public Object	getSystemService(@NonNull String name) {
		if (mProviderAppComponent != null) {
			Object service = mProviderAppComponent.getService(name);
			if (service != null) {
				return service;
			}
		}
		return super.getSystemService(name);
	}
}
