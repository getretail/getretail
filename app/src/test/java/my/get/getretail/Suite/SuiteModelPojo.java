package my.get.getretail.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import my.get.getretail.model.pojo.TestCustomer;
import my.get.getretail.model.pojo.TestDeliveryInfo;
import my.get.getretail.model.pojo.TestLayout;
import my.get.getretail.model.pojo.TestStore;
import my.get.getretail.model.pojo.TestTheme;

// Runs all unit tests.
@RunWith(Suite.class)
@Suite.SuiteClasses(
		{
				TestCustomer.class,
				TestDeliveryInfo.class,
				TestLayout.class,
				TestStore.class,
				TestTheme.class
		}
)

public class SuiteModelPojo {

}