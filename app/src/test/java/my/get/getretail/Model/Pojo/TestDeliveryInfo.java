package my.get.getretail.model.pojo;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by qlitzler on 20/10/15.
 */
public class TestDeliveryInfo extends AbstractTestPojo<DeliveryInfo> {

	private static final boolean		ACTIVE = true;
	private static final String			SUCCESS_MESSAGE = "success !";

	@Override
	public void		buildPojo() {

		mPojo = new DeliveryInfo(ACTIVE, SUCCESS_MESSAGE);
	}

	@Override
	public void		testPojo() throws Exception {
		assertThat(mPojo.isActive(), is(ACTIVE));
		assertThat(mPojo.getSuccessMessage(), is(SUCCESS_MESSAGE));
	}
}
