package my.get.getretail.model.pojo;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestCustomer extends AbstractTestPojo<Customer> {

	private static final int		ID = -1;
	private static final String		EMAIL = "test@get.my";
	private static final String		FIRST_NAME = "John";
	private static final String		LAST_NAME = "Doe";
	private static final String		PHONE = "0606060606";

	@Override
	public void		buildPojo() {
		mPojo = new Customer(
				EMAIL,
				FIRST_NAME,
				LAST_NAME,
				PHONE
		);
	}

	@Override
	public void		testPojo() throws Exception {
		assertThat(mPojo.getId(), is(ID));
		assertThat(mPojo.getEmail(), is(EMAIL));
		assertThat(mPojo.getFirstName(), is(FIRST_NAME));
		assertThat(mPojo.getLastName(), is(LAST_NAME));
		assertThat(mPojo.getPhone(), is(PHONE));
	}
}
