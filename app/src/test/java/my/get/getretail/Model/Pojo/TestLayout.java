package my.get.getretail.model.pojo;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by qlitzler on 20/10/15.
 */
public class TestLayout extends  AbstractTestPojo<Layout> {

	private static final boolean		FOCUS = true;

	@Override
	public void		buildPojo() {

		mPojo = new Layout(FOCUS);
	}

	@Override
	public void		testPojo() throws Exception {
		assertThat(mPojo.isFocus(), is(FOCUS));
	}
}
