package my.get.getretail.model.pojo;

import org.mockito.Mock;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by qlitzler on 20/10/15.
 */
public class TestTheme extends AbstractTestPojo<Theme> {

	@Mock
	private Theme		theme;

	@Override
	public void		buildPojo() {

		mPojo = theme;
	}

	@Override
	public void		testPojo() throws Exception {
		assertThat(mPojo, is(theme));
	}
}
