package my.get.getretail.model.pojo;

import org.mockito.Mock;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestStore extends AbstractTestPojo<Store> {

	private static final int		ID = -1;
	private static final int		POSITION = 0;
	private static final String		LABEL = "Breakfast";
	private static final String		DESCRIPTION = "This is a breakfast store.";
	private static final String		OPENING_DATE = "1234311123";
	private static final boolean	IS_NEW = true;
	private static final String		PICTURE = "http://breakfast.png";
	private static final boolean	AVAILABLE = true;
	private static final boolean	ACTIVE = true;
	private static final String		TAG = "#breakfast";
	@Mock
	private Theme					mTheme;
	@Mock
	private Layout					mLayout;

	@Override
	public void		buildPojo() {

		mPojo = new Store(
				POSITION,
				LABEL,
				DESCRIPTION,
				OPENING_DATE,
				IS_NEW,
				PICTURE,
				AVAILABLE,
				ACTIVE,
				TAG,
				mTheme,
				mLayout
		);
	}

	@Override
	public void		testPojo() throws Exception {
		assertThat(mPojo.getId(), is(ID));
		assertThat(mPojo.getPosition(), is(POSITION));
		assertThat(mPojo.getLabel(), is(LABEL));
		assertThat(mPojo.getDescription(), is(DESCRIPTION));
		assertThat(mPojo.getOpeningDate(), is(OPENING_DATE));
		assertThat(mPojo.isNew(), is(IS_NEW));
		assertThat(mPojo.getPicture(), is(PICTURE));
		assertThat(mPojo.isAvailable(), is(AVAILABLE));
		assertThat(mPojo.isActive(), is(ACTIVE));
		assertThat(mPojo.getTag(), is(TAG));
	}
}

