package my.get.getretail.request;

import my.get.getretail.model.pojo.GroupBasketCheckout;
import my.get.getretail.model.pojo.Success;
import my.get.getretail.network.request.RequestOrder;
import retrofit.Call;

/**
 * Created by qlitzler on 23/11/15.
 */
public class TestUpdateOrder extends TestRequest<Success> {

	public TestUpdateOrder() {
		super(TestUpdateOrder.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Success> updateCustomer;
		GroupBasketCheckout groupBasketCheckout = new GroupBasketCheckout();

		updateCustomer = RequestOrder.updateOrder(mModuleApi.getService(), groupBasketCheckout.getId());
		mRunnable = new TestRequest.RunnableOnUiThread<>(updateCustomer);
		super.testRequest();
	}
}