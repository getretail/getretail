package my.get.getretail.request;

import my.get.getretail.model.pojo.Success;
import my.get.getretail.network.request.RequestAuth;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestChangePassword extends TestRequest<Success> {

	public TestChangePassword() {
		super(TestChangePassword.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Success> changePassword;

		changePassword = RequestAuth.changePassword(mModuleApi.getService());
		mRunnable = new TestRequest.RunnableOnUiThread<>(changePassword);
		super.testRequest();
	}
}
