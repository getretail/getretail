package my.get.getretail.request;

import my.get.getretail.model.pojo.Success;
import my.get.getretail.network.request.RequestAuth;
import retrofit.Call;

/**
 * Created by qlitzler on 26/10/15.
 */
public class TestForgotPassword extends TestRequest<Success> {

	public TestForgotPassword() {
		super(TestForgotPassword.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Success> forgotPassword;

		forgotPassword = RequestAuth.forgotPassword(mModuleApi.getService());
		mRunnable = new TestRequest.RunnableOnUiThread<>(forgotPassword);
		super.testRequest();
	}
}
