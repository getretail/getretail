package my.get.getretail.request;

import my.get.getretail.model.pojo.Schedule;
import my.get.getretail.network.request.RequestDelivery;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestGetSchedule extends TestRequest<Schedule> {

	public TestGetSchedule() {
		super(TestGetSchedule.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Schedule>	getSchedule;

		getSchedule = RequestDelivery.getSchedule(mModuleApi.getService(), 0, 1);
		mRunnable = new TestRequest.RunnableOnUiThread<>(getSchedule);
		super.testRequest();
	}
}
