package my.get.getretail.request;

import my.get.getretail.model.pojo.Card;
import my.get.getretail.network.request.RequestPaymentCard;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestCreatePaymentCard extends TestRequest<Card> {

	public TestCreatePaymentCard() {
		super(TestCreatePaymentCard.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Card>	createPaymentCard;
		Card card = new Card("Perso", "0543", "09/16", "Quentin LITZLER", Card.PaymentCardType.MASTERCARD);

		createPaymentCard = RequestPaymentCard.createPaymentCard(mModuleApi.getService(), card);
		mRunnable = new TestRequest.RunnableOnUiThread<>(createPaymentCard);
		super.testRequest();
	}
}
