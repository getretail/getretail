package my.get.getretail.request;

import my.get.getretail.model.pojo.Customer;
import my.get.getretail.network.request.RequestCustomer;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestUpdateCustomer extends TestRequest<Customer> {

	public TestUpdateCustomer() {
		super(TestUpdateCustomer.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Customer>	updateCustomer;
		Customer		customer = new Customer("ql@get.my", "Quentin", "LITZLER", "0606060606");

		updateCustomer = RequestCustomer.updateCustomer(mModuleApi.getService(), customer);
		mRunnable = new TestRequest.RunnableOnUiThread<>(updateCustomer);
		super.testRequest();
	}
}
