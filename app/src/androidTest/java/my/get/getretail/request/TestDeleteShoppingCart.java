package my.get.getretail.request;

import my.get.getretail.model.pojo.Success;
import my.get.getretail.network.request.RequestShoppingCart;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestDeleteShoppingCart extends TestRequest<Success> {

	public TestDeleteShoppingCart() {
		super(TestDeleteShoppingCart.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Success>		createShoppingCart;

		createShoppingCart = RequestShoppingCart.deleteShoppingCart(mModuleApi.getService(), 1);
		mRunnable = new TestRequest.RunnableOnUiThread<>(createShoppingCart);
		super.testRequest();
	}
}
