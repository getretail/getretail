package my.get.getretail.request;

import my.get.getretail.model.pojo.Address;
import my.get.getretail.network.request.RequestAddress;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestUpdateAddress extends TestRequest<Address> {

	public TestUpdateAddress() {
		super(TestUpdateAddress.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Address> 	updateAddress;
		Address			address = new Address("Maison", "21, avenue george V", "75008", "Paris", "France");

		updateAddress = RequestAddress.updateAddress(mModuleApi.getService(), address);
		mRunnable = new TestRequest.RunnableOnUiThread<>(updateAddress);
		super.testRequest();
	}
}
