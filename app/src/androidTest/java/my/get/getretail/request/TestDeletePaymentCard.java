package my.get.getretail.request;

import my.get.getretail.model.pojo.Success;
import my.get.getretail.network.request.RequestPaymentCard;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestDeletePaymentCard extends TestRequest<Success> {

	public TestDeletePaymentCard() {
		super(TestDeletePaymentCard.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Success>	deletePaymentCard;

		deletePaymentCard = RequestPaymentCard.deletePaymentCard(mModuleApi.getService(), 1);
		mRunnable = new TestRequest.RunnableOnUiThread<>(deletePaymentCard);
		super.testRequest();
	}
}
