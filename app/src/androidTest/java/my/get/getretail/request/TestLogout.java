package my.get.getretail.request;

/**
 * Created by qlitzler on 26/10/15.
 */

import my.get.getretail.model.pojo.Success;
import my.get.getretail.network.request.RequestAuth;
import retrofit.Call;

public class TestLogout extends TestRequest<Success> {

	public TestLogout() {
		super(TestLogout.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Success> logout;

		logout = RequestAuth.logout(mModuleApi.getService());
		mRunnable = new RunnableOnUiThread<>(logout);
		super.testRequest();
	}
}
