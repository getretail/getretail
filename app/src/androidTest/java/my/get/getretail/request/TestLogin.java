package my.get.getretail.request;

import my.get.getretail.model.pojo.Customer;
import my.get.getretail.network.request.RequestCustomer;
import retrofit.Call;

/**
 * Created by qlitzler on 26/10/15.
 */
public class TestLogin extends TestRequest<Customer> {

	public TestLogin() {
		super(TestLogin.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Customer> login;

		login = RequestCustomer.login(mModuleApi.getService());
		mRunnable = new RunnableOnUiThread<>(login);
		super.testRequest();
	}
}
