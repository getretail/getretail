package my.get.getretail.request;

import java.util.List;

import my.get.getretail.model.pojo.GroupOrderHistory;
import my.get.getretail.network.request.RequestOrder;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestGetOrders extends TestRequest<List<GroupOrderHistory>> {

	public TestGetOrders() {
		super(TestGetOrders.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<List<GroupOrderHistory>>	getOrders;

		getOrders = RequestOrder.getOrders(mModuleApi.getService(), "all");
		mRunnable = new TestRequest.RunnableOnUiThread<>(getOrders);
		super.testRequest();
	}
}
