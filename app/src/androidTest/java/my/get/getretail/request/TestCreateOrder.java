package my.get.getretail.request;

import my.get.getretail.model.pojo.GroupBasketCheckout;
import my.get.getretail.network.request.RequestOrder;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestCreateOrder extends TestRequest<GroupBasketCheckout> {

	public TestCreateOrder() {
		super(TestCreateOrder.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<GroupBasketCheckout>	createOrder;
		GroupBasketCheckout groupBasketCheckout = new GroupBasketCheckout();

		createOrder = RequestOrder.createOrder(mModuleApi.getService(), groupBasketCheckout);
		mRunnable = new TestRequest.RunnableOnUiThread<>(createOrder);
		super.testRequest();
	}
}
