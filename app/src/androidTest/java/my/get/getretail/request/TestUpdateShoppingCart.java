package my.get.getretail.request;

import my.get.getretail.model.pojo.Basket;
import my.get.getretail.network.request.RequestShoppingCart;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestUpdateShoppingCart extends TestRequest<Basket> {

	public TestUpdateShoppingCart() {
		super(TestUpdateShoppingCart.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Basket>		updateShoppingCart;
		Basket basket = new Basket(null);

		updateShoppingCart = RequestShoppingCart.updateShoppingCart(mModuleApi.getService(), basket);
		mRunnable = new TestRequest.RunnableOnUiThread<>(updateShoppingCart);
		super.testRequest();
	}
}