package my.get.getretail.request;

import java.util.List;

import my.get.getretail.model.pojo.Product;
import my.get.getretail.network.request.RequestStore;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestGetProducts extends TestRequest<List<Product>> {

	public TestGetProducts() {
		super(TestGetProducts.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<List<Product>>	getProducts;

		getProducts = RequestStore.getProducts(mModuleApi.getService(), 1);
		mRunnable = new TestRequest.RunnableOnUiThread<>(getProducts);
		super.testRequest();
	}
}
