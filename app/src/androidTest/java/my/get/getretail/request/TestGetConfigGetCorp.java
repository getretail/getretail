package my.get.getretail.request;

import my.get.getretail.model.pojo.ConfigGetCorp;
import my.get.getretail.network.request.RequestConfig;
import retrofit.Call;

public class TestGetConfigGetCorp extends TestRequest<ConfigGetCorp> {

	public TestGetConfigGetCorp() {
		super(TestGetConfigGetCorp.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<ConfigGetCorp>	contentConfig;

		contentConfig = RequestConfig.getConfigGetCorp(mModuleApi.getService());
		mRunnable = new RunnableOnUiThread<>(contentConfig);
		super.testRequest();
	}
}