package my.get.getretail.request;

import java.util.List;

import my.get.getretail.model.pojo.Shop;
import my.get.getretail.network.request.RequestStore;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestGetShops extends TestRequest<List<Shop>> {

	public TestGetShops() {
		super(TestGetShops.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<List<Shop>>	getShops;

		getShops = RequestStore.getShops(mModuleApi.getService(), 1);
		mRunnable = new TestRequest.RunnableOnUiThread<>(getShops);
		super.testRequest();
	}
}
