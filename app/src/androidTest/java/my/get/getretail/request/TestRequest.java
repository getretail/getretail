package my.get.getretail.request;

import android.test.InstrumentationTestCase;
import android.util.Log;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import my.get.getretail.model.pojo.Error;
import my.get.getretail.module.ModuleApi;
import my.get.getretail.suite.SuiteRequest;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by qlitzler on 21/10/15.
 */
public class TestRequest<T> extends InstrumentationTestCase {

	private static final String				mSuffix = SuiteRequest.class.getSimpleName() + " - ";
	protected ModuleApi						mModuleApi;
	protected final CountDownLatch			mSignal = new CountDownLatch(1);
	protected RunnableOnUiThread<T>			mRunnable;
	private final PerformRequest<T> 		mRequest = new PerformRequest<>(this);
	private String							mClassName;

	protected 			TestRequest(String className) {
		mClassName = className;
	}

	@Override
	protected void		setUp() throws Exception {
		super.setUp();
		mModuleApi = new ModuleApi(getInstrumentation().getContext());
	}

	protected void		testRequest() throws Throwable {
		mRunnable.setRequest(mRequest);
		runTestOnUiThread(mRunnable);
		mSignal.await(30, TimeUnit.SECONDS);
	}

	protected static class RunnableOnUiThread<T> implements Runnable {

		private WeakReference<PerformRequest<T>>	mPerformRequest;
		private final WeakReference<Call<T>>		mResponseService;

		public RunnableOnUiThread(Call<T> responseService) {
			mResponseService = new WeakReference<>(responseService);
		}

		public void		setRequest(PerformRequest<T> performRequest) {
			mPerformRequest = new WeakReference<>(performRequest);
		}

		@Override
		public void		run() {
			mResponseService.get().enqueue(mPerformRequest.get());
		}
	}

	static class PerformRequest<T> implements Callback<T> {

		private final WeakReference<TestRequest>	mTestRequest;

		PerformRequest(TestRequest applicationTest) {
			mTestRequest = new WeakReference<>(applicationTest);
		}

		@Override
		public void onResponse(Response<T> response, Retrofit retrofit) {
			Log.d(mSuffix + mTestRequest.get().mClassName, response.raw().toString());
			if (response.body() != null) {
				Log.d(mSuffix + mTestRequest.get().mClassName, response.body().toString());
				assertTrue(true);
			} else {
				try {
					Error error = (Error)retrofit.responseConverter(Error.class, Error.class.getAnnotations()).convert(response.errorBody());
					Log.d(mSuffix + mTestRequest.get().mClassName, error.toString());
				} catch (IOException e) {
					e.printStackTrace();
				}
				assertTrue(false);
			}
			mTestRequest.get().mSignal.countDown();
		}

		@Override
		public void onFailure(Throwable t) {
			t.printStackTrace();
			assertTrue(false);
		}
	}
}
