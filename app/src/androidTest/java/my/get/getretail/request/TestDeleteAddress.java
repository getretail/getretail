package my.get.getretail.request;

import my.get.getretail.model.pojo.Success;
import my.get.getretail.network.request.RequestAddress;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestDeleteAddress extends TestRequest<Success> {

	public TestDeleteAddress() {
		super(TestDeleteAddress.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Success>	deleteAddress;

		deleteAddress = RequestAddress.deleteAddress(mModuleApi.getService(), 1);
		mRunnable = new TestRequest.RunnableOnUiThread<>(deleteAddress);
		super.testRequest();
	}
}
