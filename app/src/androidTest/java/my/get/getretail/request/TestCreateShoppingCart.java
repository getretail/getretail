package my.get.getretail.request;

import my.get.getretail.model.pojo.Basket;
import my.get.getretail.network.request.RequestShoppingCart;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestCreateShoppingCart extends TestRequest<Basket> {

	public TestCreateShoppingCart() {
		super(TestCreateShoppingCart.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Basket>		createShoppingCart;
		Basket basket = new Basket(null);

		createShoppingCart = RequestShoppingCart.createShoppingCart(mModuleApi.getService(), basket);
		mRunnable = new TestRequest.RunnableOnUiThread<>(createShoppingCart);
		super.testRequest();
	}
}
