package my.get.getretail.request;

import my.get.getretail.model.pojo.Customer;
import my.get.getretail.network.request.RequestCustomer;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestCreateCustomer extends TestRequest<Customer> {

	public TestCreateCustomer() {
		super(TestCreateCustomer.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Customer>	createCustomer;
		Customer		customer = new Customer("ql@get.my", "Quentin", "LITZLER", "0606060606");

		createCustomer = RequestCustomer.createCustomer(mModuleApi.getService(), customer, "1234");
		mRunnable = new TestRequest.RunnableOnUiThread<>(createCustomer);
		super.testRequest();
	}
}