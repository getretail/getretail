package my.get.getretail.request;

import my.get.getretail.model.pojo.Address;
import my.get.getretail.network.request.RequestAddress;
import retrofit.Call;

/**
 * Created by qlitzler on 27/10/15.
 */
public class TestCreateAddress extends TestRequest<Address> {

	public TestCreateAddress() {
		super(TestCreateAddress.class.getSimpleName());
	}

	@Override
	public void testRequest() throws Throwable {
		Call<Address>	createAddress;
		Address			address = new Address("Maison", "21, avenue george V", "75008", "Paris", "France");

		createAddress = RequestAddress.createAddress(mModuleApi.getService(), address);
		mRunnable = new TestRequest.RunnableOnUiThread<>(createAddress);
		super.testRequest();
	}
}