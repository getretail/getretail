package my.get.getretail.suite;

import junit.framework.Test;
import junit.framework.TestSuite;

import my.get.getretail.request.TestChangePassword;
import my.get.getretail.request.TestCreateAddress;
import my.get.getretail.request.TestCreateCustomer;
import my.get.getretail.request.TestCreateOrder;
import my.get.getretail.request.TestCreatePaymentCard;
import my.get.getretail.request.TestCreateShoppingCart;
import my.get.getretail.request.TestDeleteAddress;
import my.get.getretail.request.TestDeletePaymentCard;
import my.get.getretail.request.TestDeleteShoppingCart;
import my.get.getretail.request.TestForgotPassword;
import my.get.getretail.request.TestGetConfigGetCorp;
import my.get.getretail.request.TestGetOrders;
import my.get.getretail.request.TestGetProducts;
import my.get.getretail.request.TestGetSchedule;
import my.get.getretail.request.TestGetShops;
import my.get.getretail.request.TestLogin;
import my.get.getretail.request.TestLogout;
import my.get.getretail.request.TestUpdateAddress;
import my.get.getretail.request.TestUpdateCustomer;
import my.get.getretail.request.TestUpdateShoppingCart;

/**
 * Created by qlitzler on 21/10/15.
 */

public class SuiteRequest extends TestSuite {

	public static Test suite() {
		final TestSuite s = new TestSuite();
		s.addTestSuite(TestChangePassword.class);
		s.addTestSuite(TestCreateAddress.class);
		s.addTestSuite(TestCreateCustomer.class);
		s.addTestSuite(TestCreateOrder.class);
		s.addTestSuite(TestCreatePaymentCard.class);
		s.addTestSuite(TestCreateShoppingCart.class);
		s.addTestSuite(TestDeleteAddress.class);
		s.addTestSuite(TestDeletePaymentCard.class);
		s.addTestSuite(TestDeleteShoppingCart.class);
		s.addTestSuite(TestForgotPassword.class);
		s.addTestSuite(TestGetConfigGetCorp.class);
		s.addTestSuite(TestGetOrders.class);
		s.addTestSuite(TestGetSchedule.class);
		s.addTestSuite(TestGetShops.class);
		s.addTestSuite(TestGetProducts.class);
		s.addTestSuite(TestLogin.class);
		s.addTestSuite(TestLogout.class);
		s.addTestSuite(TestUpdateCustomer.class);
		s.addTestSuite(TestUpdateAddress.class);
		s.addTestSuite(TestUpdateShoppingCart.class);
		return s;
	}
}
